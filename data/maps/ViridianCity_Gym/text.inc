ViridianCity_Gym_Text_GiovanniIntro::
    .string "Fwahahaha!\n"
    .string "This is my hideout!\p"
    .string "I planned to resurrect\n"
    .string "TEAM ROCKET here!\p"
    .string "But, you have caught me again!\n"
    .string "So be it!\l"
    .string "This time, I'm not holding back!\p"
    .string "Once more, you shall face\n"
    .string "GIOVANNI, the greatest trainer!{PLAY_BGM}{MUS_ENCOUNTER_ROCKET}$"

ViridianCity_Gym_Text_GiovanniDefeat::
    .string "Ha!\n"
    .string "That was a truly intense fight!\l"
    .string "You have won!\p"
    .string "As proof, here is the EARTHBADGE!\n"
    .string "{PAUSE_MUSIC}{PLAY_BGM}{MUS_OBTAIN_BADGE}{PAUSE 0xFE}{PAUSE 0x56}{RESUME_MUSIC}$"

ViridianCity_Gym_Text_GiovanniPostBattle::
    .string "Having lost, I cannot face\n"
    .string "my underlings!\l"
    .string "TEAM ROCKET is finished forever!\p"
    .string "I will dedicate my life to\n"
    .string "the study of POKéMON!\p"
    .string "Let us meet again some day!\n"
    .string "Farewell!$"

ViridianCity_Gym_Text_ExplainEarthBadgeTakeThis::
    .string "The EARTHBADGE makes POKéMON\n"
    .string "of any level obey!\p"
    .string "It is evidence of your mastery\n"
    .string "as a POKéMON trainer!\p"
    .string "With it, you can enter the\n"
    .string "POKéMON LEAGUE!\p"
    .string "It is my gift for your POKéMON\n"
    .string "LEAGUE challenge!$"

ViridianCity_Gym_Text_ReceivedTM27FromGiovanni::
    .string "{PLAYER} received TM27!$"

ViridianCity_Gym_Text_ExplainTM27::
    .string "TM27 is FISSURE!\p"
    .string "It will take out POKéMON\n"
    .string "with just one hit!\p"
    .string "I made it when I ran the GYM here,\n"
    .string "too long ago...$"

ViridianCity_Gym_Text_YouDoNotHaveSpace::
    .string "You do not have space for this!$"

ViridianCity_Gym_Text_YujiIntro::
    .string "Heh!\n"
    .string "You must be running out of\l"
    .string "steam by now!$"

ViridianCity_Gym_Text_YujiDefeat::
    .string "COOLTRAINER: I ran out of gas!$"

ViridianCity_Gym_Text_YujiPostBattle::
    .string "You'll need power to keep up\n"
    .string "with our GYM LEADER!$"

ViridianCity_Gym_Text_AtsushiIntro::
    .string "Rrrroar!\n"
    .string "I'm working myself into a rage!$"

ViridianCity_Gym_Text_AtsushiDefeat::
    .string "BLACKBELT: Wargh!$"

ViridianCity_Gym_Text_AtsushiPostBattle::
    .string "I'm still not worthy!$"

ViridianCity_Gym_Text_JasonIntro::
    .string "POKéMON and I, we make wonderful\n"
    .string "music together!$"

ViridianCity_Gym_Text_JasonDefeat::
    .string "TAMER: You are in perfect harmony!$"

ViridianCity_Gym_Text_JasonPostBattle::
    .string "Do you know the identity of\n"
    .string "our GYM LEADER?$"

ViridianCity_Gym_Text_KiyoIntro::
    .string "Karate is the ultimate form\n"
    .string "of martial arts!$"

ViridianCity_Gym_Text_KiyoDefeat::
    .string "BLACKBELT: Atcho!$"

ViridianCity_Gym_Text_KiyoPostBattle::
    .string "If my POKéMON were as good\n"
    .string "at Karate as I...$"

ViridianCity_Gym_Text_WarrenIntro::
    .string "The truly talented win with style!$"

ViridianCity_Gym_Text_WarrenDefeat::
    .string "COOLTRAINER: I lost my grip!$"

ViridianCity_Gym_Text_WarrenPostBattle::
    .string "The LEADER will scold me!$"

ViridianCity_Gym_Text_TakashiIntro::
    .string "I'm the KARATE KING!\n"
    .string "Your fate rests with me!$"

ViridianCity_Gym_Text_TakashiDefeat::
    .string "BLACKBELT: Ayah!$"

ViridianCity_Gym_Text_TakashiPostBattle::
    .string "POKéMON LEAGUE?\n"
    .string "You? Don't get cocky!$"

ViridianCity_Gym_Text_ColeIntro::
    .string "Your POKéMON will cower at\n"
    .string "the crack of my whip!$"

ViridianCity_Gym_Text_ColeDefeat::
    .string "TAMER: Yowch!\n"
    .string "Whiplash!$"

ViridianCity_Gym_Text_ColePostBattle::
    .string "Wait!\n"
    .string "I was just careless!$"

ViridianCity_Gym_Text_SamuelIntro::
    .string "VIRIDIAN GYM was closed for\n"
    .string "a long time, but now our\p"
    .string "LEADER is back!$"

ViridianCity_Gym_Text_SamuelDefeat::
    .string "COOLTRAINER♂: I was beaten?$"

ViridianCity_Gym_Text_SamuelPostBattle::
    .string "You can go onto POKéMON\n"
    .string "LEAGUE only by defeating our\l"
    .string "GYM LEADER!$"

ViridianCity_Gym_Text_GymGuyAdvice::
    .string "Yo!\n"
    .string "Champ in the making!\p"
    .string "Even I don't know VIRIDIAN\n"
    .string "LEADER's identity!\p"
    .string "This will be the toughest of\n"
    .string "all the GYM LEADERs!\p"
    .string "I heard that the trainers here\n"
    .string "like ground-type POKéMON!$"

ViridianCity_Gym_Text_GymGuyPostVictory::
    .string "Blow me away! GIOVANNI was\n"
    .string "the GYM LEADER here?$"

ViridianCity_Gym_Text_GymStatue::
    .string "VIRIDIAN CITY POKéMON GYM\n"
    .string "LEADER: GIOVANNI\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

ViridianCity_Gym_Text_GymStatuePlayerWon::
    .string "VIRIDIAN CITY POKéMON GYM\n"
    .string "LEADER: GIOVANNI\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

