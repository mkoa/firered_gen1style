SSAnne_2F_Room2_Text_BrooksIntro::
    .string "Competing against the young keeps\n"
    .string "me youthful.$"

SSAnne_2F_Room2_Text_BrooksDefeat::
    .string "GENTLEMAN: Good match!\n"
    .string "Ah, I feel young again!$"

SSAnne_2F_Room2_Text_BrooksPostBattle::
    .string "15 years ago, I would have won!$"

SSAnne_2F_Room2_Text_DaleIntro::
    .string "Check out what I fished up!$"

SSAnne_2F_Room2_Text_DaleDefeat::
    .string "FISHERMAN: I'm all out!$"

SSAnne_2F_Room2_Text_DalePostBattle::
    .string "Party?\p"
    .string "The cruise ship's party\n"
    .string "should be over by now.$"

