LavenderTown_Mart_Text_SearchingForStatRaiseItems::
    .string "I'm searching for items that raise\n"
    .string "the abilities of POKéMON\l"
    .string "during a single battle.\p"
    .string "of a single battle.\p"
    .string "X ATTACK, X DEFEND, X SPEED\n"
    .string "and X SPECIAL are what I'm after.\p"
    .string "Do you know where I can get them?$"

LavenderTown_Mart_Text_DidYouBuyRevives::
    .string "You know REVIVE?\n"
    .string "It revives any fainted POKéMON!$"

LavenderTown_Mart_Text_TrainerDuosCanChallengeYou::
    .string "Sometimes, a TRAINER duo will\n"
    .string "challenge you with two POKéMON\l"
    .string "at the same time.\p"
    .string "If that happens, you have to send\n"
    .string "out two POKéMON to battle, too.$"

LavenderTown_Mart_Text_SoldNuggetFromMountainsFor5000::
    .string "この　あいだ　やまおくで\n"
    .string "きんのたまを　ひろい　ましてね！\p"
    .string "つかえない　しなもの　ですが\n"
    .string "うったら　なんと　5000¥でした$"
