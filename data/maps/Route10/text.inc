Route10_Text_MarkIntro::
    .string "Wow, are you a POKéMANIAC too?\n"
    .string "Want to see my collection?$"

Route10_Text_MarkDefeat::
    .string "POKéMANIAC: Humph.\n"
    .string "I'm not angry!$"

Route10_Text_MarkPostBattle::
    .string "I have more rare POKéMON at home!$"

Route10_Text_ClarkIntro::
    .string "Ha-hahah-ah-ha!$"

Route10_Text_ClarkDefeat::
    .string "HIKER: Ha-haha!\n"
    .string "Not laughing!\l"
    .string "Ha-hay fever! Haha-ha-choo!$"

Route10_Text_ClarkPostBattle::
    .string "Haha-ha-choo!\n"
    .string "Ha-choo!\l"
    .string "Snort! Snivel!$"

Route10_Text_HermanIntro::
    .string "Hi kid!\n"
    .string "Want to see my POKéMON?$"

Route10_Text_HermanDefeat::
    .string "POKéMANIAC: Oh no!\n"
    .string "My POKéMON!$"

Route10_Text_HermanPostBattle::
    .string "I don't like you for beating me!$\n"

Route10_Text_HeidiIntro::
    .string "I've been to a POKéMON GYM\n"
    .string "a few times.\p"
    .string "But, I lost each time.$"

Route10_Text_HeidiDefeat::
    .string "JR.TRAINER♀: Ohh!\n"
    .string "Blew it again!$"

Route10_Text_HeidiPostBattle::
    .string "I noticed some POKéMANIACs\n"
    .string "prowling around.$"

Route10_Text_TrentIntro::
    .string "Ah!\n"
    .string "This mountain air is delicious!$"

Route10_Text_TrentDefeat::
    .string "HIKER: That cleared my head!$"

Route10_Text_TrentPostBattle::
    .string "I feel bloated on mountain air!$"

Route10_Text_CarolIntro::
    .string "I'm feeling a bit faint\n"
    .string "from this tough hike.$"

Route10_Text_CarolDefeat::
    .string "JR.TRAINER♂: I'm not up for it!$"

Route10_Text_CarolPostBattle::
    .string "The POKéMON here are so chunky!\n"
    .string "There should be a pink one\p"
    .string "with a floral pattern!$"

Route10_Text_RockTunnelDetourToLavender::
    .string "ROCK TUNNEL$"

Route10_Text_RockTunnel::
    .string "ROCK TUNNEL$"

Route10_Text_PowerPlant::
    .string "POWER PLANT$"

