RocketHideout_B4F_Text_GiovanniIntro::
    .string "So! I must say, I am impressed\n"
    .string "you got here.$"

RocketHideout_B4F_Text_GiovanniDefeat::
    .string "WHAT!\n"
    .string "This can't be!$"

RocketHideout_B4F_Text_GiovanniPostBattle::
    .string "I see that you raise POKéMON\n"
    .string "with utmost care.\p"
    .string "A child like you would never\n"
    .string "understand what I hope to achieve.\p"
    .string "I shall step aside this time!\n"
    .string "I hope we meet again...$"

RocketHideout_B4F_Text_Grunt2Intro::
    .string "I know you!\n"
    .string "You ruined our plans at MT.MOON!$"

RocketHideout_B4F_Text_Grunt2Defeat::
    .string "ROCKET: Burned again!$"

RocketHideout_B4F_Text_Grunt2PostBattle::
    .string "Do you have something against\n"
    .string "TEAM ROCKET?$"

RocketHideout_B4F_Text_Grunt3Intro::
    .string "How can you not see the\n"
    .string "beauty of our evil?$"

RocketHideout_B4F_Text_Grunt3Defeat::
    .string "ROCKET: Ayaya!$"

RocketHideout_B4F_Text_Grunt3PostBattle::
    .string "BOSS!\n"
    .string "I'm sorry I failed you!$"

RocketHideout_B4F_Text_Grunt1Intro::
    .string "The elevator doesn't work?\n"
    .string "Who has the LIFT KEY?$"

RocketHideout_B4F_Text_Grunt1Defeat::
    .string "ROCKET: No!$"

RocketHideout_B4F_Text_Grunt1PostBattle::
    .string "Oh, no!\n"
    .string "I dropped the LIFT KEY!$"

