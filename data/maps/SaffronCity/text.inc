SaffronCity_Text_WhatDoYouWantGetLost::
    .string "What do you want?\n"
    .string "Get lost!$"

SaffronCity_Text_BossTakeTownForTeamRocket::
    .string "BOSS said he'll take this town!$"

SaffronCity_Text_DontGetDefiantOrIllHurtYou::
    .string "Get out of the way!$"

SaffronCity_Text_SaffronBelongsToTeamRocket::
    .string "SAFFRON belongs to TEAM ROCKET!$"

SaffronCity_Text_CriminalLifeMakesMeFeelAlive::
    .string "Being evil makes me\n"
    .string "feel so alive!$"

SaffronCity_Text_WatchWhereYoureWalking::
    .string "Ow!\n"
    .string "Watch where you're walking!$"

SaffronCity_Text_WeCanExploitMonsAroundWorld::
    .string "With SILPH under control, we can\n"
    .string "exploit POKéMON around the world!$"

SaffronCity_Text_YouBeatTeamRocket::
    .string "You beat TEAM ROCKET all alone?\n"
    .string "That's amazing!$"

SaffronCity_Text_SafeToGoOutAgain::
    .string "Yeah! TEAM ROCKET is gone!\n"
    .string "It's safe to go out again!$"

SaffronCity_Text_PeopleComingBackToSaffron::
    .string "People should be flocking back\n"
    .string "to SAFFRON now.$"

SaffronCity_Text_FlewHereOnPidgeot::
    .string "I flew here on my PIDGEOT when\n"
    .string "I read about SILPH.\p"
    .string "It's already over?\n"
    .string "I missed the media action.$"

SaffronCity_Text_Pidgeot::
    .string "PIDGEOT: Bi bibii!$"

SaffronCity_Text_SawRocketBossEscaping::
    .string "I saw the ROCKET BOSS escaping\n"
    .string "SILPH's building.$"

SaffronCity_Text_ImASecurityGuard::
    .string "I'm a security guard.\p"
    .string "Suspicious kids I don't allow in!$"

SaffronCity_Text_HesTakingASnooze::
    .string "...\n"
    .string "Snore...\p"
    .string "Hah! He's taking a snooze!$"

SaffronCity_Text_CitySign::
    .string "SAFFRON CITY\n"
    .string "Shining, Golden Land of Commerce$"

SaffronCity_Text_FightingDojo::
    .string "FIGHTING DOJO$"

SaffronCity_Text_GymSign::
    .string "SAFFRON CITY POKéMON GYM\n"
    .string "LEADER: SABRINA\l"
    .string "The Master of Psychic POKéMON!$"

SaffronCity_Text_FullHealCuresStatus::
    .string "TRAINER TIPS\p"
    .string "FULL HEAL cures all ailments like\n"
    .string "burns, paralysis, poisoning,\l"
    .string "freezing, and sleep.\p"
    .string "It costs a bit more, but it's more\n"
    .string "convenient than buying other items.$"

SaffronCity_Text_GreatBallImprovedCatchRate::
    .string "TRAINER TIPS\p"
    .string "New GREAT BALL offers improved\n"
    .string "capture rates.\p"
    .string "Try it on those hard-to-catch\n"
    .string "POKéMON.$"

SaffronCity_Text_SilphCoSign::
    .string "SILPH CO. OFFICE BUILDING$"

SaffronCity_Text_MrPsychicsHouse::
    .string "MR. PSYCHIC'S HOUSE$"

SaffronCity_Text_SilphsLatestProduct::
    .string "SILPH's latest product!\n"
    .string "Release to be determined...$"

SaffronCity_Text_TrainerFanClubSign::
    .string "POKéMON TRAINER FAN CLUB\p"
    .string "Many TRAINERS have scribbled their\n"
    .string "names on this sign.$"

SaffronCity_Text_HowCanClubNotRecognizeLance::
    .string "This FAN CLUB…\n"
    .string "No one here has a clue!\p"
    .string "How could they not recognize\n"
    .string "the brilliance that is LANCE?\p"
    .string "He stands for justice!\n"
    .string "He's cool, and yet passionate!\l"
    .string "He's the greatest, LANCE!$"

