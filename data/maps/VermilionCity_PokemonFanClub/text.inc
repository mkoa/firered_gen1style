VermilionCity_PokemonFanClub_Text_AdmirePikachusTail::
    .string "Won't you admire my PIKACHU's\n"
    .string "adorable tail?$"

VermilionCity_PokemonFanClub_Text_PikachuTwiceAsCute::
    .string "Humph!\p"
    .string "My PIKACHU is twice as cute as\n"
    .string "that one!$"

VermilionCity_PokemonFanClub_Text_AdoreMySeel::
    .string "I just adore my SEEL!\n"
    .string "It's so lovable!\p"
    .string "It squeals, “Kyuuuh,” when I\n"
    .string "hug it!$"

VermilionCity_PokemonFanClub_Text_SeelFarMoreAttractive::
    .string "Oh dear!\p"
    .string "My SEEL is far more attractive!$"

VermilionCity_PokemonFanClub_Text_Pikachu::
    .string "PIKACHU: Chu! Pikachu!$"

VermilionCity_PokemonFanClub_Text_Seel::
    .string "SEEL: Kyuoo!$"

VermilionCity_PokemonFanClub_Text_DidYouComeToHearAboutMyMons::
    .string "I chair the POKéMON Fan Club!\p"
    .string "I have collected over\n"
    .string "over 100 POKéMON!\p"
    .string "I'm very fussy when it\n"
    .string "comes to POKéMON!\p"
    .string "So...\p"
    .string "Did you come visit to hear\n"
    .string "about my POKéMON?$"

VermilionCity_PokemonFanClub_Text_ChairmansStory::
    .string "Good!\n"
    .string "Then listen up!\p"
    .string "My favorite RAPIDASH...\p"
    .string "It's...cute...lovely...smart...\n"
    .string "plus...amazing...you think so?...\l"
    .string "oh yes...it...stunning...\l"
    .string "kindly...love it!\p"
    .string "Hug it...when...sleeping...warm\n"
    .string "and cuddly...spectacular...\l"
    .string "ravishing...\l"
    .string "...Oops! Look at the time!\l"
    .string "I kept you too long!\p"
    .string "Thanks for hearing me out!\n"
    .string "I want you to have this!$"

VermilionCity_PokemonFanClub_Text_ReceivedBikeVoucherFromChairman::
    .string "{PLAYER} received a BIKE VOUCHER!$"

VermilionCity_PokemonFanClub_Text_ExplainBikeVoucher::
    .string "Exchange that for a BICYCLE!\n"
    .string "Don't worry, my FEAROW\p"
    .string "will FLY me anywhere!\l"
    .string "So, I don't need a BICYCLE!\n"
    .string "I hope you like cycling!$"

VermilionCity_PokemonFanClub_Text_ComeBackToHearStory::
    .string "Oh. Come back when you want\n"
    .string "to hear my story!$"

VermilionCity_PokemonFanClub_Text_DidntComeToSeeAboutMonsAgain::
    .string "Hello, {PLAYER}!\p"
    .string "Did you come see me about my\n"
    .string "POKéMON again?\p"
    .string "No?\n"
    .string "Too bad!$"

VermilionCity_PokemonFanClub_Text_MakeRoomForThis::
    .string "Make room for this!$"

VermilionCity_PokemonFanClub_Text_ChairmanVeryVocalAboutPokemon::
    .string "Our Chairman is very vocal\n"
    .string "about POKéMON.$"

VermilionCity_PokemonFanClub_Text_ListenPolitelyToOtherTrainers::
    .string "Let's all listen politely to other\n"
    .string "TRAINERS!$"

VermilionCity_PokemonFanClub_Text_SomeoneBragsBragBack::
    .string "If someone brags, brag right back!$"

VermilionCity_PokemonFanClub_Text_ChairmanReallyAdoresHisMons::
    .string "Our CHAIRMAN really does adore his\n"
    .string "POKéMON.\p"
    .string "But the person who is most liked by\n"
    .string "POKéMON is DAISY, I think.$"
