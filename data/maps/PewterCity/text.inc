ViridianCity_Text_TakeTM42::
    .string "Yawn!\n"
    .string "I must have dozed off in the sun.\p"
    .string "I had this dream about a\n"
    .string "DROWZEE eating my dream.\p"
    .string "What's this?\n"
    .string "Where did this TM come from?\p"
    .string "This is spooky! Here,\n"
    .string "you can have this TM.$"

ViridianCity_Text_PutTM42Away::
    .string "{PLAYER} put TM42 away in\n"
    .string "the BAG's TM CASE.$"

ViridianCity_Text_TM42_Contains::
    .string "TM42 contains DREAM EATER...\n"
    .string "...Snore...$"

PewterCity_Text_ClefairyCameFromMoon::
    .string "It's rumored that CLEFAIRYs.\n"
    .string "came from the moon!\p"
    .string "They appeared after MOON STONE\n"
    .string "fell on MT.MOON.$"

PewterCity_Text_BrockOnlySeriousTrainerHere::
    .string "There aren't many serious POKéMON\n"
    .string "trainers here!\p"
    .string "They're all like BUG CATCHERs,\n"
    .string "but PEWTER GYM's BROCK\l"
    .string "is totally into it!$"

PewterCity_Text_DidYouCheckOutMuseum::
    .string "Did you check out the MUSEUM?$"

PewterCity_Text_WerentThoseFossilsAmazing::
    .string "Weren't those fossils from MT. MOON\n"
    .string "amazing?$"

PewterCity_Text_ReallyYouHaveToGo::
    .string "Really?\n"
    .string "You absolutely have to go!$"

PewterCity_Text_ThisIsTheMuseum::
    .string "It's right here!\n"
    .string "You have to pay to get in, \l"
    .string "but it's worth it!\p"
    .string "See you around!$"

PewterCity_Text_DoYouKnowWhatImDoing::
    .string "Psssst!\n"
    .string "Do you know what I'm doing?$"

PewterCity_Text_ThatsRightItsHardWork::
    .string "That's right!\n"
    .string "It's hard work!$"

PewterCity_Text_SprayingRepelToKeepWildMonsOut::
    .string "I'm spraying REPEL to keep\n"
    .string "POKéMON out of my garden!$"

PewterCity_Text_BrocksLookingForChallengersFollowMe::
    .string "You're a TRAINER right? BROCK's\n"
    .string "looking for new challengers!\p"
    .string "Follow me!$"

PewterCity_Text_GoTakeOnBrock::
    .string "If you have the right stuff,\n"
    .string "go take on BROCK!$"

PewterCity_Text_TrainerTipsEarningEXP::
    .string "TRAINER TIPS\p"
    .string "Any POKéMON that takes part\n"
    .string "in battle, however short,\l"
    .string "earns EXP!$"

PewterCity_Text_CallPoliceIfInfoOnThieves::
    .string "NOTICE!\p"
    .string "Thieves have been stealing POKéMON\n"
    .string "fossils at MT. MOON!\p"
    .string "Please call PEWTER POLICE\n"
    .string "with any info!$"

PewterCity_Text_MuseumOfScience::
    .string "PEWTER MUSEUM OF SCIENCE$"

PewterCity_Text_GymSign::
    .string "PEWTER CITY POKéMON GYM\n"
    .string "LEADER: BROCK\l"
    .string "The Rock Solid POKéMON Trainer!$"

PewterCity_Text_CitySign::
    .string "PEWTER CITY\n"
    .string "A Stone Gray City$"

@ The below 3 JP texts are unused. Possibly a cut NPC meant to give the player the Berry Pouch
PewterCity_Text_DefeatedBrockYouCanHaveTreasure::
    .string "あッれー！\n"
    .string "その　ジムバッジ‥‥\l"
    .string "すげえな　タケシに　かったのかよ！\p"
    .string "かんどう　したから\n"
    .string "おれの　たからもの　やるよ！$"

PewterCity_Text_BerriesInsideUseCarefully::
    .string "なかには　きのみが　はいってるぜ\p"
    .string "やくに　たつ　きのみも\n"
    .string "はいって　いるから\l"
    .string "だいじに　つかって　くれよ！$"

PewterCity_Text_MonsWillUseHeldBerriesOnTheirOwn::
    .string "ポケモンに　きのみを\n"
    .string "もたせて　おけば\l"
    .string "たたかっている　ときに\l"
    .string "かってに　つかって　くれるんだ\p"
    .string "キズぐすり　とか　どくけし　より\n"
    .string "てがるで　べんり　だろ？$"

PewterCity_Text_OhPlayer::
    .string "Oh, {PLAYER}{KUN}!$"

PewterCity_Text_AskedToDeliverThis::
    .string "I'm glad I caught up to you.\n"
    .string "I'm PROF. OAK's AIDE.\p"
    .string "I've been asked to deliver this,\n"
    .string "so here you go.$"

PewterCity_Text_ReceivedRunningShoesFromAide::
    .string "{PLAYER} received the\n"
    .string "RUNNING SHOES from the AIDE.$"

PewterCity_Text_SwitchedShoesWithRunningShoes::
    .string "{PLAYER} switched shoes with the\n"
    .string "RUNNING SHOES.$"

PewterCity_Text_ExplainRunningShoes::
    .string "Press the B Button to run.\n"
    .string "But only where there's room to run!$"

PewterCity_Text_MustBeGoingBackToLab::
    .string "Well, I must be going back to\n"
    .string "the LAB.\p"
    .string "Bye-bye!$"

PewterCity_Text_RunningShoesLetterFromMom::
    .string "There's a letter attached…\p"
    .string "Dear {PLAYER},\p"
    .string "Here is a pair of RUNNING SHOES\n"
    .string "for my beloved challenger.\p"
    .string "Remember, I'll always cheer for\n"
    .string "you! Don't ever give up!\p"
    .string "From Mom$"

