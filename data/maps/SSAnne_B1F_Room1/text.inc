SSAnne_B1F_Room1_Text_PhillipIntro::
    .string "Matey, you're walking the\n"
    .string "plank if you lose!$"

SSAnne_B1F_Room1_Text_PhillipDefeat::
    .string "SAILOR: Argh!\n"
    .string "Beaten by a kid!$"

SSAnne_B1F_Room1_Text_PhillipPostBattle::
    .string "Jellyfish sometimes\n"
    .string "drift into the ship.$"

SSAnne_B1F_Room1_Text_BarnyIntro::
    .string "Hello stranger!\p"
    .string "Stop and chat!\n"
    .string "All my POKéMON are from the sea!$"

SSAnne_B1F_Room1_Text_BarnyDefeat::
    .string "FISHERMAN: Darn!\n"
    .string "I let that one get away!$"

SSAnne_B1F_Room1_Text_BarnyPostBattle::
    .string "I was going to make you my\n"
    .string "assistant, too!$"

