SaffronCity_CopycatsHouse_1F_Text_DaughterIsSelfCentered::
    .string "My daughter is so self-centered.\n"
    .string "She only has a few friends.$"

SaffronCity_CopycatsHouse_1F_Text_DaughterLikesToMimicPeople::
    .string "My daughter likes to mimic people.\n"
    .string "Her mimicry has earned her the\n"
    .string "nickname COPYCAT around here!$"

SaffronCity_CopycatsHouse_1F_Text_Chansey::
    .string "CHANSEY: Chaan! Sii!$"

