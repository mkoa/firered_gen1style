Route4_PokemonCenter_1F_Text_CanHaveSixMonsWithYou::
    .string "I've 6 POKé BALLs\n"
    .string "set in my belt.\p"
    .string "At most, you can\n"
    .string "carry 6 POKéMON.$"

Route4_PokemonCenter_1F_Text_TeamRocketAttacksCerulean::
    .string "TEAM ROCKET attacks CERULEAN\n"
    .string "citizens...\p"
    .string "TEAM ROCKET is always\n"
    .string "in the news!$"

Route4_PokemonCenter_1F_Text_LaddieBuyMagikarpForJust500::
    .string "MAN: Hello there!\n"
    .string "Have I got a deal just for you!\p"
    .string "I'll let you have a swell\n"
    .string "MAGIKARP for just ¥500!\p"
    .string "What do you say?$"

Route4_PokemonCenter_1F_Text_SweetieBuyMagikarpForJust500::
    .string "MAN: Hello there!\n"
    .string "Have I got a deal just for you!\p"
    .string "I'll let you have a swell\n"
    .string "MAGIKARP for just ¥500!\p"
    .string "What do you say?$"

Route4_PokemonCenter_1F_Text_PaidOutrageouslyForMagikarp::
    .string "{PLAYER} paid an outrageous ¥500\n"
    .string "and bought the MAGIKARP...$"

Route4_PokemonCenter_1F_Text_OnlyDoingThisAsFavorToYou::
    .string "No? I'm only doing this\n"
    .string "as a favor to you!$"

Route4_PokemonCenter_1F_Text_NoRoomForMorePokemon::
    .string "There's no more room for any more\n"
    .string "POKéMON, it looks like.$"

Route4_PokemonCenter_1F_Text_YoullNeedMoreMoney::
    .string "You'll need more money than that!$"

Route4_PokemonCenter_1F_Text_IDontGiveRefunds::
    .string "MAN: Well, I don't give refunds!$"

Route4_PokemonCenter_1F_Text_ShouldStoreMonsUsingPC::
    .string "If you have too many POKéMON, you\n"
    .string "should store them via PC!$"

Route4_PokemonCenter_1F_Text_ItsANewspaper::
    .string "It's a newspaper.$"

