VictoryRoad_1F_Text_NaomiIntro::
    .string "I wonder if you are good\n"
    .string "enough for me!$"

VictoryRoad_1F_Text_NaomiDefeat::
    .string "COOLTRAINER♀: I lost out!$"

VictoryRoad_1F_Text_NaomiPostBattle::
    .string "I never wanted to lose to anybody!$"

VictoryRoad_1F_Text_RolandoIntro::
    .string "I can see you're good!\n"
    .string "Let me see exactly how good!$"

VictoryRoad_1F_Text_RolandoDefeat::
    .string "COOLTRAINER♂: I had a chance...$"

VictoryRoad_1F_Text_RolandoPostBattle::
    .string "I concede, you're better than me!$"

