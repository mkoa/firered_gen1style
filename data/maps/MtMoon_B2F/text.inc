MtMoon_B2F_Text_MiguelIntro::
    .string "Hey, stop!\p"
    .string "I found these fossils!\n"
    .string "They're both mine!$"

MtMoon_B2F_Text_MiguelDefeat::
    .string "SUPER NERD: Ok!\n"
    .string "I'll share!$"

MtMoon_B2F_Text_WellEachTakeAFossil::
    .string "We'll each take one!\n"
    .string "No being greedy!$"

MtMoon_B2F_Text_ThenThisFossilIsMine::
    .string "All right.\n"
    .string "Then this is mine!$"

MtMoon_B2F_Text_LabOnCinnabarRegeneratesFossils::
    .string "Far away, on CINNABAR ISLAND,\n"
    .string "there's a POKéMON LAB.\p"
    .string "They do research on regenerating\n"
    .string "fossils.$"

MtMoon_B2F_Text_Grunt1Intro::
    .string "We, TEAM ROCKET, shall find the\n"
    .string "fossils!\p"
    .string "Reviving POKéMON from them will\n"
    .string "earn us huge riches!$"

MtMoon_B2F_Text_Grunt1Defeat::
    .string "ROCKET: Urgh!\n"
    .string "Now I'm mad!$"

MtMoon_B2F_Text_Grunt1PostBattle::
    .string "You made me mad!\n"
    .string "TEAM ROCKET will blacklist you!$"

MtMoon_B2F_Text_Grunt2Intro::
    .string "TEAM ROCKET will find\n"
    .string "the fossils, revive and\l"
    .string "sell them for cash!$"

MtMoon_B2F_Text_Grunt2Defeat::
    .string "ROCKET: I blew it!$"

MtMoon_B2F_Text_Grunt2PostBattle::
    .string "Darn it all!\n"
    .string "My associates won't stand for this!$"

MtMoon_B2F_Text_Grunt3Intro::
    .string "We're pulling a big job here!\n"
    .string "Get lost, kid!$"

MtMoon_B2F_Text_Grunt3Defeat::
    .string "So, you are good...$"

MtMoon_B2F_Text_Grunt3PostBattle::
    .string "If you find a fossil, give it to me\n"
    .string "and scram!$"

MtMoon_B2F_Text_Grunt4Intro::
    .string "Little kids should leave\n"
    .string "grown-ups alone!$"

MtMoon_B2F_Text_Grunt4Defeat::
    .string "ROCKET: I'm steamed!$"

MtMoon_B2F_Text_Grunt4PostBattle::
    .string "POKéMON lived here long before\n"
    .string "people came.$"

MtMoon_B2F_Text_YouWantDomeFossil::
    .string "You want the DOME FOSSIL?$"

MtMoon_B2F_Text_YouWantHelixFossil::
    .string "You want the HELIX FOSSIL?$"

MtMoon_B2F_Text_ObtainedHelixFossil::
    .string "{PLAYER} got the\n" 
    .string "HELIX FOSSIL!$"

MtMoon_B2F_Text_ObtainedDomeFossil::
    .string "{PLAYER} got the\n"
    .string "DOME FOSSIL!$"

