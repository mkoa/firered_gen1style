SilphCo_11F_Text_ThanksForSavingMeDearBoy::
    .string "PRESIDENT: Thank you for\n"
    .string "saving SILPH!\p"
    .string "I will never forget you saved us\n"
    .string "in our moment of peril!\p"
    .string "I have to thank you in some way!\p"
    .string "Because I am rich, I can give\n"
    .string "you anything!\p"
    .string "Here, maybe this will do!$"

SilphCo_11F_Text_ThanksForSavingMeDearGirl::
    .string "PRESIDENT: Oh, dear girl!\n"
    .string "Thank you for saving SILPH.\p"
    .string "I will never forget you saved us in\n"
    .string "our moment of peril.\p"
    .string "I have to thank you in some way.\p"
    .string "Because I am rich, I can give you\n"
    .string "anything.\p"
    .string "Here, maybe this will do.$"

SilphCo_11F_Text_ObtainedMasterBallFromPresident::
    .string "{PLAYER} got a MASTER BALL!$"

SilphCo_11F_Text_ThatsOurSecretPrototype::
    .string "PRESIDENT: You can't buy\n"
    .string "that anywhere!\p"
    .string "It's our secret prototype\n"
    .string "MASTER BALL!\p"
    .string "It will catch any POKéMON\n"
    .string "without fail!\p"
    .string "You should be quiet about using\n"
    .string "it, though.$"

SilphCo_11F_Text_YouHaveNoRoomForThis::
    .string "You have no room for this.$"

SilphCo_11F_Text_ThanksForRescuingUs::
    .string "SECRETARY: Thank you for rescuing\n"
    .string "all of us!\p"
    .string "We admire your courage.$"

SilphCo_11F_Text_GiovanniIntro::
    .string "Ah {PLAYER}!\n"
    .string "So we meet again!\p"
    .string "The PRESIDENT and I are discussing\n"
    .string "a vital business proposition.\p"
    .string "Keep your nose out of grown-up\n"
    .string "matters...\p"
    .string "Or, experience a world of pain!$"

SilphCo_11F_Text_GiovanniDefeat::
    .string "GIOVANNI: Arrgh!!\n"
    .string "I lost again!?$"

SilphCo_11F_Text_GiovanniPostBattle::
    .string "Blast it all!\n"
    .string "You ruined our plans for SILPH!\p"
    .string "But, TEAM ROCKET will never fall!\p"
    .string "{PLAYER}! Never forget that all\n"
    .string "POKéMON exist for TEAM ROCKET!\p"
    .string "I must go, but I shall return!$"

SilphCo_11F_Text_Grunt2Intro::
    .string "Stop right there!\n"
    .string "Don't you move!$"

SilphCo_11F_Text_Grunt2Defeat::
    .string "ROCKET: Don't...\n"
    .string "Please!$"

SilphCo_11F_Text_Grunt2PostBattle::
    .string "So, you want to see my BOSS?$"

SilphCo_11F_Text_Grunt1Intro::
    .string "Halt! Do you have an appointment\n"
    .string "with my BOSS?$"

SilphCo_11F_Text_Grunt1Defeat::
    .string "Gaah!\n"
    .string "Demolished!$"

SilphCo_11F_Text_Grunt1PostBattle::
    .string "Watch your step...\n"
    .string "My BOSS likes his POKéMON tough!$"

SilphCo_11F_Text_MonitorHasMonsOnIt::
    .string "The monitor has POKéMON on it!$"

SilphCo_11F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "11F$"

