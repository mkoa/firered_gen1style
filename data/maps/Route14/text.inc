Route14_Text_CarterIntro::
    .string "You need to use TMs to teach good\n"
    .string "moves to POKéMON!$"

Route14_Text_CarterDefeat::
    .string "BIRD KEEPER: Not good enough!$"

Route14_Text_CarterPostBattle::
    .string "You have some HMs right? POKéMON\n"
    .string "can't ever forget those moves.$"

Route14_Text_MitchIntro::
    .string "My bird POKéMON should be ready\n"
    .string "for battle.$"

Route14_Text_MitchDefeat::
    .string "BIRD KEEPER: Not ready yet!$"

Route14_Text_MitchPostBattle::
    .string "They need to learn better moves.$"

Route14_Text_BeckIntro::
    .string "TMs are on sale in CELEDON!\n"
    .string "But, only a few people\n"
    .string "have HMs!$"

Route14_Text_BeckDefeat::
    .string "BIRD KEEPER: Aww, bummer!$"

Route14_Text_BeckPostBattle::
    .string "Teach POKéMON moves of the same\n"
    .string "element type for more power.$"

Route14_Text_MarlonIntro::
    .string "Have you taught your bird POKéMON\n"
    .string "how to FLY?$"

Route14_Text_MarlonDefeat::
    .string "BIRD KEEPER: Shot down in flames!$"

Route14_Text_MarlonPostBattle::
    .string "Bird POKéMON are my one true love!$"

Route14_Text_DonaldIntro::
    .string "Have you heard of the\n"
    .string "legendary POKéMON?$"

Route14_Text_DonaldDefeat::
    .string "BIRD KEEPER: Why?\n"
    .string "Why'd I lose?$"

Route14_Text_DonaldPostBattle::
    .string "The 3 legendary POKéMON are\n"
    .string "all birds of prey.$"

Route14_Text_BennyIntro::
    .string "I'm not into it, but OK!\n"
    .string "Let's go!$"

Route14_Text_BennyDefeat::
    .string "BIRD KEEPER: I knew it!$"

Route14_Text_BennyPostBattle::
    .string "Winning, losing, it doesn't matter\n"
    .string "in the long run!$"

Route14_Text_LukasIntro::
    .string "C'mon, c'mon.\n"
    .string "Let's go, let's go, let's go!$"

Route14_Text_LukasDefeat::
    .string "BIKER: Arrg!\n"
    .string "Lost! Get lost!$"

Route14_Text_LukasPostBattle::
    .string "What, what, what?\n"
    .string "What do you want?$"

Route14_Text_IsaacIntro::
    .string "Perfect!\n"
    .string "I need to burn some time!$"

Route14_Text_IsaacDefeat::
    .string "BIKER: What?\n"
    .string "You!?$"

Route14_Text_IsaacPostBattle::
    .string "Raising POKéMON is a drag, man.$"

Route14_Text_GeraldIntro::
    .string "We ride out here because there's\n"
    .string "more room!$"

Route14_Text_GeraldDefeat::
    .string "BIKER: Wipe out!$"

Route14_Text_GeraldPostBattle::
    .string "It's cool you made your POKéMON\n"
    .string "so strong!\p"
    .string "Might is right!\n"
    .string "And you know it!$"

Route14_Text_MalikIntro::
    .string "POKéMON fight?\n"
    .string "Cool! Rumble!$"

Route14_Text_MalikDefeat::
    .string "BIKER: Blown away!$"

Route14_Text_MalikPostBattle::
    .string "You know who'd win, you and\n"
    .string "me one on one!$"

Route14_Text_RouteSign::
    .string "ROUTE 14\n"
    .string "West to FUCHSIA CITY$"

Route14_Text_KiriIntro::
    .string "KIRI: JAN, let's try really,\n"
    .string "really hard together.$"

Route14_Text_KiriDefeat::
    .string "KIRI: Whimper…\n"
    .string "We lost, didn't we?$"

Route14_Text_KiriPostBattle::
    .string "KIRI: Did we lose because of me?$"

Route14_Text_KiriNotEnoughMons::
    .string "KIRI: We can battle if you have\n"
    .string "two POKéMON.$"

Route14_Text_JanIntro::
    .string "JAN: KIRI, here we go!\n"
    .string "We have to try hard!$"

Route14_Text_JanDefeat::
    .string "JAN: Eeeeh!\n"
    .string "No fair!$"

Route14_Text_JanPostBattle::
    .string "JAN: KIRI, don't cry!\n"
    .string "We'll just try harder next time.$"

Route14_Text_JanNotEnoughMons::
    .string "JAN: You want to battle?\n"
    .string "You don't have enough POKéMON.$"
