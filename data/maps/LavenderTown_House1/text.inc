LavenderTown_House1_Text_Cubone::
    .string "CUBONE: Kyarugoo!$"

LavenderTown_House1_Text_RocketsKilledCubonesMother::
    .string "I hate those horrible ROCKETs!\n"
    .string "That poor CUBONE's mother...\p"
    .string "It was killed trying to escape\n"
    .string "from TEAM ROCKET!$"

LavenderTown_House1_Text_GhostOfPokemonTowerIsGone::
    .string "The ghost of POKéMON TOWER is\n"
    .string "gone!\p"
    .string "Someone must have soothed its\n"
    .string "restless spirit!$"

