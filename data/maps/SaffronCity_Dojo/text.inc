SaffronCity_Dojo_Text_MasterKoichiIntro::
    .string "Grunt!\p"
    .string "I am the KARATE MASTER!\n"
    .string "I am the LEADER here!\p"
    .string "You wish to challenge us?\n"
    .string "Expect no mercy!\p"
    .string "Fwaaa!$"

SaffronCity_Dojo_Text_MasterKoichiDefeat::
    .string "BLACKBELT: Hwa!\n"
    .string "Arrgh! Beaten!$"

SaffronCity_Dojo_Text_ChoosePrizedFightingMon::
    .string "Indeed, I have lost!\p"
    .string "But, I beseech you, do not take\n"
    .string "our emblem as your trophy!\p"
    .string "In return, I will give you a prized\n"
    .string "fighting POKéMON!\p"
    .string "Choose whichever one you like!$"

SaffronCity_Dojo_Text_StayAndTrainWithUs::
    .string "Ho!\n"
    .string "Stay and train at Karate with us!$"

SaffronCity_Dojo_Text_MikeIntro::
    .string "Hoargh!\n"
    .string "Take your shoes off!$"

SaffronCity_Dojo_Text_MikeDefeat::
    .string "BLACKBELT: I give up!$"

SaffronCity_Dojo_Text_MikePostBattle::
    .string "You wait 'til you see our Master!\n"
    .string "I'm a small fry compared to him!$"

SaffronCity_Dojo_Text_HidekiIntro::
    .string "I hear you're good!\n"
    .string "Show me!$"

SaffronCity_Dojo_Text_HidekiDefeat::
    .string "BLACKBELT: Judge!\n"
    .string "1 point!$"

SaffronCity_Dojo_Text_HidekiPostBattle::
    .string "Our Master is a pro fighter!$"

SaffronCity_Dojo_Text_AaronIntro::
    .string "Nothing frightens me!\p"
    .string "I break boulders for training!$"

SaffronCity_Dojo_Text_AaronDefeat::
    .string "BLACKBELT: Yow!\n"
    .string "Stubbed fingers!$"

SaffronCity_Dojo_Text_AaronPostBattle::
    .string "The only thing that frightens\n"
    .string "us is psychic power!$"

SaffronCity_Dojo_Text_HitoshiIntro::
    .string "Hoohah!\p"
    .string "You're trespassing in our\n"
    .string "FIGHTING DOJO!$"

SaffronCity_Dojo_Text_HitoshiDefeat::
    .string "BLACKBELT: Oof!\n"
    .string "I give up!$"

SaffronCity_Dojo_Text_HitoshiPostBattle::
    .string "The prime fighters across\n"
    .string "the land train here.$"

SaffronCity_Dojo_Text_YouWantHitmonlee::
    .string "You want the hard kicking\n"
    .string "HITMONLEE?$"

SaffronCity_Dojo_Text_ReceivedMonFromKarateMaster::
    .string "{PLAYER} got {STR_VAR_1}!$"

SaffronCity_Dojo_Text_YouWantHitmonchan::
    .string "You want the piston punching\n"
    .string "HITMONCHAN?$"

SaffronCity_Dojo_Text_ReceivedMonFromKarateMaster2::
    .string "{PLAYER}は　カラテ　だいおう　から\n"
    .string "{STR_VAR_1}を　もらった！$"

SaffronCity_Dojo_Text_BetterNotGetGreedy::
    .string "Better not get greedy...$"

SaffronCity_Dojo_Text_EnemiesOnEverySide::
    .string "Enemies on every side!$"

SaffronCity_Dojo_Text_GoesAroundComesAround::
    .string "What goes around comes around!$"

SaffronCity_Dojo_Text_FightingDojo::
    .string "FIGHTING DOJO$"

