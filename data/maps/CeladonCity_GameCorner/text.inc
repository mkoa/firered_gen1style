CeladonCity_GameCorner_Text_CanExchangeCoinsNextDoor::
    .string "Welcome!\p"
    .string "You can exchange your coins for\n"
    .string "fabulous prizes next door.$"

CeladonCity_GameCorner_Text_WelcomeBuySomeCoins::
    .string "Welcome to ROCKET GAME CORNER!\p"
    .string "Do you need some game coins?\n"
    .string "It's ¥1000 for 50 coins.\n"
    .string "Would you like some?$"

CeladonCity_GameCorner_Text_ComePlaySometime::
    .string "No?\n"
    .string "Please come play sometime!$"

CeladonCity_GameCorner_Text_SorryDontHaveCoinCase::
    .string "You don't have a COIN CASE!$"

CeladonCity_GameCorner_Text_CoinCaseIsFull::
    .string "Whoops!\n"
    .string "Your COIN CASE is full.$"

CeladonCity_GameCorner_Text_CantAffordCoins::
    .string "You can't afford the COINS.$"

CeladonCity_GameCorner_Text_HereAreYourCoins::
    .string "Thank you.\n"
    .string "Here are your COINS!$"

CeladonCity_GameCorner_Text_RumoredTeamRocketRunsThisPlace::
    .string "Keep this quiet.\p"
    .string "It's rumored that this place is run\n"
    .string "by TEAM ROCKET.$"

CeladonCity_GameCorner_Text_ThinkMachinesHaveDifferentOdds::
    .string "I think these machines have\n"
    .string "different odds.$"

CeladonCity_GameCorner_Text_DoYouWantToPlay::
    .string "Kid, do you want to play?$"

CeladonCity_GameCorner_Text_Received10CoinsFromMan::
    .string "{PLAYER} received 10 COINS\n"
    .string "from the man.$"

CeladonCity_GameCorner_Text_DontNeedMyCoins::
    .string "You don't need my COINS!$"

CeladonCity_GameCorner_Text_WinsComeAndGo::
    .string "Wins seem to come and go.\n"
    .string "Nothing's a sure thing.$"

CeladonCity_GameCorner_Text_WinOrLoseItsOnlyLuck::
    .string "I'm having a wonderful time!$"

CeladonCity_GameCorner_Text_GymGuyAdvice::
    .string "Hey!\p"
    .string "You have better things to do,\n"
    .string "champ in the making!\p"
    .string "CELADON GYM's LEADER is ERIKA!\n"
    .string "She uses grass-type POKéMON!\p"
    .string "She might appear docile,\n"
    .string "but don't be fooled!$"

CeladonCity_GameCorner_Text_RareMonsForCoins::
    .string "They offer rare POKéMON that can\n"
    .string "be exchanged for your COINS.\p"
    .string "But, I just can't seem to win!$"

CeladonCity_GameCorner_Text_SoEasyToGetHooked::
    .string "Games are scary!\n"
    .string "It's so easy to get hooked!$"

CeladonCity_GameCorner_Text_WantSomeCoins::
    .string "What's up?\n"
    .string "Want some coins?$"

CeladonCity_GameCorner_Text_Received20CoinsFromNiceGuy::
    .string "{PLAYER} received 20 COINS\n"
    .string "from the nice guy.$"

CeladonCity_GameCorner_Text_YouHaveLotsOfCoins::
    .string "You have lots of COINS!$"

CeladonCity_GameCorner_Text_NeedMoreCoinsForMonIWant::
    .string "Darn! I need more COINS for the\n"
    .string "POKéMON I want!$"

CeladonCity_GameCorner_Text_HereAreSomeCoinsShoo::
    .string "Hey, what? You're throwing me off!\n"
    .string "Here are some coins, shoo!$"

CeladonCity_GameCorner_Text_Received20CoinsFromMan::
    .string "{PLAYER} received 20 COINS\n"
    .string "from the man.$"

CeladonCity_GameCorner_Text_YouveGotPlentyCoins::
    .string "You've got plenty of your own\n"
    .string "COINS!$"

CeladonCity_GameCorner_Text_WatchReelsClosely::
    .string "The trick is to watch the reels\n"
    .string "closely.$"

CeladonCity_GameCorner_Text_GruntIntro::
    .string "I'm guarding this poster!\n"
    .string "Go away, or else!$"

CeladonCity_GameCorner_Text_GruntDefeat::
    .string "ROCKET: Dang!$"

CeladonCity_GameCorner_Text_GruntPostBattle::
    .string "Our HIDEOUT might\n"
    .string "be discovered!\p"
    .string "I better tell BOSS!$"

CeladonCity_GameCorner_Text_SwitchBehindPosterPushIt::
    .string "Hey!\p"
    .string "A switch behind the poster!?\n"
    .string "Let's push it!$"

CeladonCity_GameCorner_Text_CoinCaseIsRequired::
    .string "A COIN CASE is required...$"

CeladonCity_GameCorner_Text_DontHaveCoinCase::
    .string "Oops!\n"
    .string "Forgot the COIN CASE!$"

CeladonCity_GameCorner_Text_SlotMachineWantToPlay::
    .string "A slot machine!\n"
    .string "Want to play?$"

CeladonCity_GameCorner_Text_OutOfOrder::
    .string "OUT OF ORDER\n"
    .string "This is broken.$"

CeladonCity_GameCorner_Text_OutToLunch::
    .string "OUT TO LUNCH\n"
    .string "This is reserved.$"

CeladonCity_GameCorner_Text_SomeonesKeys::
    .string "Someone's keys!\n"
    .string "They'll be back.$"

