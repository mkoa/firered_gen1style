Route25_Text_JoeyIntro::
    .string "Local trainers come\n"
    .string "here to practice!$"

Route25_Text_JoeyDefeat::
    .string "YOUNGSTER: You're decent.$"

Route25_Text_JoeyPostBattle::
    .string "All POKéMON have weaknesses.\n"
    .string "It's best to raise different kinds.$"

Route25_Text_DanIntro::
    .string "Dad took me to a great party on\n"
    .string "the S.S.ANNE at VERMILION CITY!$"

Route25_Text_DanDefeat::
    .string "YOUNGSTER: I'm not mad!$"

Route25_Text_DanPostBattle::
    .string "On the S.S. ANNE, I saw TRAINERS\n"
    .string "from around the world.$"

Route25_Text_FlintIntro::
    .string "I'm a cool guy.\n"
    .string "I've got a girl friend!$"

Route25_Text_FlintDefeat::
    .string "JR.TRAINER♂: Aww, darn...$"

Route25_Text_FlintPostBattle::
    .string "Oh well.\n"
    .string "My girl will cheer me up.$"

Route25_Text_KelseyIntro::
    .string "Hi!\n"
    .string "My boy friend is cool!$"

Route25_Text_KelseyDefeat::
    .string "LASS: I was in bad condition!$"

Route25_Text_KelseyPostBattle::
    .string "I wish my guy was\n"
    .string "as good as you!$"

Route25_Text_ChadIntro::
    .string "I knew I had to fight you!$"

Route25_Text_ChadDefeat::
    .string "YOUNGSTER: I knew I'd lose too!$"

Route25_Text_ChadPostBattle::
    .string "If your POKéMON gets confused or\n"
    .string "falls asleep, switch it!$"

Route25_Text_HaleyIntro::
    .string "My friend has a cute POKéMON.\n"
    .string "I'm so jealous!$"

Route25_Text_HaleyDefeat::
    .string "LASS: I'm not so jealous!$"

Route25_Text_HaleyPostBattle::
    .string "You came from MT.MOON?\n"
    .string "May I have a CLEFAIRY?$"

Route25_Text_FranklinIntro::
    .string "I just got down from MT.MOON,\n"
    .string "but I'm ready!$"

Route25_Text_FranklinDefeat::
    .string "HIKER: You worked hard!$"

Route25_Text_FranklinPostBattle::
    .string "Drat!\n"
    .string "A ZUBAT bit me back in there.$"

Route25_Text_NobIntro::
    .string "I'm off to see a POKéMON\n"
    .string "collector at the cape!$"

Route25_Text_NobDefeat::
    .string "HIKER: You got me.$"

Route25_Text_NobPostBattle::
    .string "The collector has many\n"
    .string "rare kinds of POKéMON.$"

Route25_Text_WayneIntro::
    .string "You're going to see BILL?\n"
    .string "First, let's fight!$"

Route25_Text_WayneDefeat::
    .string "HIKER: You're something.$"

Route25_Text_WaynePostBattle::
    .string "The trail below is a shortcut\n"
    .string "to CERULEAN CITY.$"

Route25_Text_SeaCottageSign::
    .string "SEA COTTAGE\n"
    .string "BILL lives here!$"

Route25_Text_MistyHighHopesAboutThisPlace::
    .string "This cape is a famous date spot.\p"
    .string "MISTY, the GYM LEADER, has high\n"
    .string "hopes about this place.$"

Route25_Text_AreYouHereAlone::
    .string "Hello, are you here alone?\p"
    .string "If you're out at CERULEAN's cape…\n"
    .string "Well, it should be as a couple.$"

