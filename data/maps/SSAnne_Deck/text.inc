SSAnne_Deck_Text_ShipDepartingSoon::
    .string "The party's over.\n"
    .string "The ship will be departing soon.$"

SSAnne_Deck_Text_ScrubbingDecksHardWork::
    .string "Scrubbing decks is hard work!$"

SSAnne_Deck_Text_FeelSeasick::
    .string "Urf. I feel ill.\n"
    .string "I stepped out to get some air.$"

SSAnne_Deck_Text_EdmondIntro::
    .string "Hey matey!\p"
    .string "Let's do a little jig!$"

SSAnne_Deck_Text_EdmondDefeat::
    .string "SAILOR: You're impressive!$"

SSAnne_Deck_Text_EdmondPostBattle::
    .string "How many kinds of POKéMON\n"
    .string "do you think there are?$"

SSAnne_Deck_Text_TrevorIntro::
    .string "Ahoy there!\n"
    .string "Are you seasick?$"

SSAnne_Deck_Text_TrevorDefeat::
    .string "SAILOR: I was just careless!$"

SSAnne_Deck_Text_TrevorPostBattle::
    .string "My pa said there are 100 kinds of\n"
    .string "POKéMON. I think there are more.$"

