@ Male and female text identical, differ (presumably) in JP
Route12_NorthEntrance_2F_Text_TakeTMDontNeedAnymoreMale::
    .string "My POKéMON's ashes are stored in\n"
    .string "POKéMON TOWER.\p"
    .string "You can have this TM.\n"
    .string "I don't need it any more...$"

Route12_NorthEntrance_2F_Text_TakeTMDontNeedAnymoreFemale::
    .string "My POKéMON's ashes are stored in\n"
    .string "POKéMON TOWER.\p"
    .string "You can have this TM.\n"
    .string "I don't need it any more...$"

Route12_NorthEntrance_2F_Text_ReceivedTM39FromLittleGirl::
    .string "{PLAYER} received TM39!$"

Route12_NorthEntrance_2F_Text_ExplainTM39::
    .string "TM39 is a move called RETURN...\p"
    .string "If you treat your POKéMON good,\n"
    .string "it will return your love by working\l"
    .string "its hardest in battle.$"

Route12_NorthEntrance_2F_Text_DontHaveRoomForThis::
    .string "You don't have room for this.$"

Route12_NorthEntrance_2F_Text_TheresManFishing::
    .string "Looked into the binoculars.\n"
    .string "A man fishing!$"

Route12_NorthEntrance_2F_Text_ItsPokemonTower::
    .string "Looked into the binoculars.\n"
    .string "It's POKéMON TOWER!$"

