CeladonCity_GameCorner_PrizeRoom_Text_FancyThatPorygon::
    .string "I sure do fancy that PORYGON!\n"
    .string "But, it's hard to win at slots!$"

CeladonCity_GameCorner_PrizeRoom_Text_RakedItInToday::
    .string "I had a major haul today!$"

CeladonCity_GameCorner_PrizeRoom_Text_CoinCaseRequired::
    .string "A COIN CASE is required.$"

CeladonCity_GameCorner_PrizeRoom_Text_WeExchangeCoinsForPrizes::
    .string "We exchange your coins for prizes.$"

CeladonCity_GameCorner_PrizeRoom_Text_WhichPrize::
    .string "Which prize do you want?$"

CeladonCity_GameCorner_PrizeRoom_Text_HereYouGo::
    .string "はい どうぞ$"

CeladonCity_GameCorner_PrizeRoom_Text_YouWantPrize::
    .string "So, you want {STR_VAR_1}?$"

CeladonCity_GameCorner_PrizeRoom_Text_OhFine::
    .string "Oh, fine then.$"

CeladonCity_GameCorner_PrizeRoom_Text_YouWantTM::
    .string "So, you want {STR_VAR_2}?$"

CeladonCity_GameCorner_PrizeRoom_Text_NeedMoreCoins::
    .string "Sorry, you need more coins.$"

CeladonCity_GameCorner_PrizeRoom_Text_OopsNotEnoughRoom::
    .string "おきゃくさん もう もてないよ$"

CeladonCity_GameCorner_PrizeRoom_Text_OhFineThen::
    .string "あっ そう$"

