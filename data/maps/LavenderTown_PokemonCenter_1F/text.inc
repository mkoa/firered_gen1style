LavenderTown_PokemonCenter_1F_Text_RocketsDoAnythingForMoney::
    .string "TEAM ROCKET will do anything\n"
    .string "for the sake of gold!$"

LavenderTown_PokemonCenter_1F_Text_CubonesMotherKilledByRockets::
    .string "I saw CUBONE's mother die trying\n"
    .string "to escape from TEAM ROCKET!$"

LavenderTown_PokemonCenter_1F_Text_PeoplePayForCuboneSkulls::
    .string "CUBONEs wear skulls, right?\n"
    .string "People will pay a lot for one!$"

