SSAnne_B1F_Room4_Text_LeonardIntro::
    .string "You know what they say about\n"
    .string "sailors and fighting!$"

SSAnne_B1F_Room4_Text_LeonardDefeat::
    .string "SAILOR: Right!\n"
    .string "Good fight, mate!$"

SSAnne_B1F_Room4_Text_LeonardPostBattle::
    .string "Haha!\n"
    .string "Want to be a sailor, mate?$"

SSAnne_B1F_Room4_Text_DuncanIntro::
    .string "My sailor's pride is at stake!$"

SSAnne_B1F_Room4_Text_DuncanDefeat::
    .string "SAIRLOR: Your spirit sank me!$"

SSAnne_B1F_Room4_Text_DuncanPostBattle::
    .string "Did you see the FISHING GURU in\n"
    .string "VERMILION CITY?$"

