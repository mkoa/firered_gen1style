Route11_Text_HugoIntro::
    .string "Win, lose or draw!$"

Route11_Text_HugoDefeat::
    .string "GAMBLER: Atcha!\n"
    .string "Didn't go my way!$"

Route11_Text_HugoPostBattle::
    .string "POKéMON is life!\n"
    .string "And to live is to gamble!$"

Route11_Text_JasperIntro::
    .string "Competition!\n"
    .string "I can't get enough!$"

Route11_Text_JasperDefeat::
    .string "GAMBLER: I had a chance!$"

Route11_Text_JasperPostBattle::
    .string "You can't be a coward in the world\n"
    .string "of POKéMON!$"

Route11_Text_EddieIntro::
    .string "Let's go, but don't cheat!$"

Route11_Text_EddieDefeat::
    .string "YOUNGSTER: Huh?\n"
    .string "That's not right!$"

Route11_Text_EddiePostBattle::
    .string "I did my best!\n"
    .string "I have no regrets!$"

Route11_Text_BraxtonIntro::
    .string "Careful!\n"
    .string "I'm laying down some cables!$"

Route11_Text_BraxtonDefeat::
    .string "ENGINEER: That was electric!$"

Route11_Text_BraxtonPostBattle::
    .string "Spread the word to save energy!$"

Route11_Text_DillonIntro::
    .string "I just became a TRAINER!\n"
    .string "But, I think I can win!$"

Route11_Text_DillonDefeat::
    .string "YOUNGSTER: My POKéMON couldn't!$"

Route11_Text_DillonPostBattle::
    .string "What do you want?\n"
    .string "Leave me alone!$"

Route11_Text_DirkIntro::
    .string "Fwahaha!\n"
    .string "I have never lost!$"

Route11_Text_DirkDefeat::
    .string "GAMBLER: My first loss!$"

Route11_Text_DirkPostBattle::
    .string "Luck of the draw!\n"
    .string "Just luck!$"

Route11_Text_DarianIntro::
    .string "I have never won before...$"

Route11_Text_DarianDefeat::
    .string "GAMBLER: I saw this coming...$"

Route11_Text_DarianPostBattle::
    .string "It's just luck.\n"
    .string "Luck of the draw.$"

Route11_Text_YasuIntro::
    .string "I'm the best in my class!$"

Route11_Text_YasuDefeat::
    .string "YOUNGSTER: Darn! I need to\n"
    .string "make my POKéMON stronger!$"

Route11_Text_YasuPostBattle::
    .string "There's a fat POKéMON that comes\n"
    .string "down from the mountains.\p"
    .string "It's strong if you\n"
    .string "can get it.$"

Route11_Text_BernieIntro::
    .string "Watch out for live wires!$"

Route11_Text_BernieDefeat::
    .string "ENGINEER: Whoa!\n"
    .string "You spark plug!$"

Route11_Text_BerniePostBattle::
    .string "Well, better get back to work.$"

Route11_Text_DaveIntro::
    .string "My POKéMON should\n"
    .string "be ready by now!$"

Route11_Text_DaveDefeat::
    .string "YOUNGSTER: Too much, yoo young!$"

Route11_Text_DavePostBattle::
    .string "I better go find stronger ones!$"

Route11_Text_DiglettsCave::
    .string "DIGLETT'S CAVE$"

