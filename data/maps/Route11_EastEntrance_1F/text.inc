@ Unclear where this is originally from
Route11_EastEntrance_1F_Text_BagIsFull::
    .string "{PLAYER}{KUN}の　バッグ\n"
    .string "いっぱい　みたい　だね$"

Route11_EastEntrance_1F_Text_ManInLavenderRatesNames::
    .string "When you catch lots of POKéMON,\n"
    .string "isn't it hard to think up names?\p"
    .string "In LAVENDER TOWN, there's a man\n"
    .string "who rates POKéMON nicknames.\p"
    .string "He'll help you rename\n"
    .string "them too!$"

Route11_EastEntrance_1F_Text_RockTunnelToReachLavender::
    .string "If you're aiming to reach LAVENDER\n"
    .string "TOWN, take ROCK TUNNEL.\p"
    .string "You can get to ROCK TUNNEL from\n"
    .string "CERULEAN CITY.$"

