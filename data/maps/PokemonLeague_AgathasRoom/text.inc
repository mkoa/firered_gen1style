PokemonLeague_AgathasRoom_Text_Intro::
    .string "I am AGATHA of the ELITE FOUR!\p"
    .string "OAK's taken a lot of interest\n"
    .string "in you, child!\p"
    .string "That old duff was once tough\n"
    .string "and handsome!\p"
    .string "That was decades ago!\l"
    .string "Now he just wants to fiddle\n"
    .string "with his POKéDEX!\p"
    .string "He's wrong!\n"
    .string "POKéMON are for fighting!\p"
    .string "{PLAYER}! I'll show you how a real\n"
    .string "trainer fights!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

PokemonLeague_AgathasRoom_Text_RematchIntro::
    .string "I am AGATHA of the ELITE FOUR.\p"
    .string "You're the child that OAK's taken\n"
    .string "under his wing, aren't you?\p"
    .string "That old duff was once tough and\n"
    .string "handsome.\p"
    .string "But that was decades ago.\n"
    .string "He's a shadow of his former self.\p"
    .string "Now he just wants to fiddle with\n"
    .string "his POKéDEX.\p"
    .string "He's wrong.\n"
    .string "POKéMON are for battling!\p"
    .string "{PLAYER}! I'll show you how a real\n"
    .string "TRAINER battles!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

PokemonLeague_AgathasRoom_Text_Defeat::
    .string "Oh ho!\n"
    .string "You're something special, child!$"

PokemonLeague_AgathasRoom_Text_PostBattle::
    .string "You win!\p"
    .string "I see what the old duff sees\n"
    .string "in you now!\p"
    .string "I have nothing else to say!\n"
    .string "Run along now, child!$"

