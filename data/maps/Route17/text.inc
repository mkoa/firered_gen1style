Route17_Text_RaulIntro::
    .string "There's no money in\n"
    .string "fighting kids.$"

Route17_Text_RaulDefeat::
    .string "CUE BALL: Burned out!$"

Route17_Text_RaulPostBattle::
    .string "Good stuff is lying around\n"
    .string "on CYCLING ROAD!$"

Route17_Text_IsaiahIntro::
    .string "What do you want, kiddo?$"

Route17_Text_IsaiahDefeat::
    .string "CUE BALL: Whoo!$"

Route17_Text_IsaiahPostBattle::
    .string "I could belly-bump you\n"
    .string "outta here!$"

Route17_Text_VirgilIntro::
    .string "You heading to FUCHSIA?$"

Route17_Text_VirgilDefeat::
    .string "BIKER: Crash and burn!$"

Route17_Text_VirgilPostBattle::
    .string "I love racing downhill!$"

Route17_Text_BillyIntro::
    .string "We're BIKERs!\n"
    .string "Highway stars!$"

Route17_Text_BillyDefeat::
    .string "BIKER: Smoked!$"

Route17_Text_BillyPostBattle::
    .string "Are you looking for adventure?$"

Route17_Text_NikolasIntro::
    .string "Let VOLTORB electrify you!$"

Route17_Text_NikolasDefeat::
    .string "BIKER: Grounded out!$"

Route17_Text_NikolasPostBattle::
    .string "I got my VOLTORB at the abandoned\n"
    .string "POWER PLANT.$"

Route17_Text_ZeekIntro::
    .string "my POKéMON  won't evolve! Why?$"

Route17_Text_ZeekDefeat::
    .string "Why, you!$"

Route17_Text_ZeekPostBattle::
    .string "Maybe some POKéMON need element\n"
    .string "STONEs to evolve.$"

Route17_Text_JamalIntro::
    .string "I need a little exercise!$"

Route17_Text_JamalDefeat::
    .string "CUE BALL: Whew!\n"
    .string "Good workout!$"

Route17_Text_JamalPostBattle::
    .string "I'm sure I lost weight there!$"

Route17_Text_CoreyIntro::
    .string "Be a rebel!$"

Route17_Text_CoreyDefeat::
    .string "CUE BALL: Aaaargh!$"

Route17_Text_CoreyPostBattle::
    .string "Be ready to fight for your beliefs!$"

Route17_Text_JaxonIntro::
    .string "Nice BIKE!\n"
    .string "How's it handle?$"

Route17_Text_JaxonDefeat::
    .string "BIKER: Shoot!$"

Route17_Text_JaxonPostBattle::
    .string "The slope makes it hard to steer!$"

Route17_Text_WilliamIntro::
    .string "Get lost, kid!\n"
    .string "I'm bushed!$"

Route17_Text_WilliamDefeat::
    .string "BIKER: Are you satisfied?$"

Route17_Text_WilliamPostBattle::
    .string "I need to catch a few Zs!$"

Route17_Text_WatchOutForDiscardedItems::
    .string "It's a notice!\p"
    .string "Watch out for discarded items!$"

Route17_Text_SameSpeciesGrowDifferentRates::
    .string "TRAINER TIPS\p"
    .string "All POKéMON are unique.\p"
    .string "Even POKéMON of the same type and\n"
    .string "level grow at different rates.$"

Route17_Text_PressBToStayInPlace::
    .string "TRAINER TIPS\p"
    .string "Press the A or B Button to stay in\n"
    .string "place while on a slope.$"

Route17_Text_RouteSign::
    .string "ROUTE 17\n"
    .string "CELADON CITY - FUCHSIA CITY$"

Route17_Text_DontThrowGameThrowBalls::
    .string "It's a notice!\p"
    .string "Don't throw the game, throw POKé\n"
    .string "BALLs instead!$"

Route17_Text_CyclingRoadSign::
    .string "CYCLING ROAD\n"
    .string "Slope ends here!$"

