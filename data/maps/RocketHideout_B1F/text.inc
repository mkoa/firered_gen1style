RocketHideout_B1F_Text_Grunt1Intro::
    .string "Who are you?\n"
    .string "How did you get here?$"

RocketHideout_B1F_Text_Grunt1Defeat::
    .string "ROCKET: Oww!\n"
    .string "Beaten!$"

RocketHideout_B1F_Text_Grunt1PostBattle::
    .string "Are you dissing TEAM ROCKET?$"

RocketHideout_B1F_Text_Grunt2Intro::
    .string "You broke into our operation?$"

RocketHideout_B1F_Text_Grunt2Defeat::
    .string "ROCKET: Burnt!$"

RocketHideout_B1F_Text_Grunt2PostBattle::
    .string "You're not going to get\n"
    .string "away with this, brat!$"

RocketHideout_B1F_Text_Grunt3Intro::
    .string "Intruder alert!$"

RocketHideout_B1F_Text_Grunt3Defeat::
    .string "ROCKET: I can't do it!$"

RocketHideout_B1F_Text_Grunt3PostBattle::
    .string "SILPH SCOPE?\n"
    .string "I don't know where it is!$"

RocketHideout_B1F_Text_Grunt4Intro::
    .string "Why did you come here?$"

RocketHideout_B1F_Text_Grunt4Defeat::
    .string "ROCKET: This won't do!$"

RocketHideout_B1F_Text_Grunt4PostBattle::
    .string "Okay, I'll talk!\n"
    .string "Take the elevator to see my BOSS!$"

RocketHideout_B1F_Text_Grunt5Intro::
    .string "Are you lost, you little rat?$"

RocketHideout_B1F_Text_Grunt5Defeat::
    .string "ROCKET: Why...?$"

RocketHideout_B1F_Text_Grunt5PostBattle::
    .string "Uh-oh, that fight somehow\n"
    .string "opened the door!$"

