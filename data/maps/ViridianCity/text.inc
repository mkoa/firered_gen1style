ViridianCity_Text_CanCarryMonsAnywhere::
    .string "Those POKé BALLs at your waist!\n"
    .string "You have POKéMON!\p"
    .string "It's great that you can carry and\n"
    .string "use POKéMON any time, anywhere!$"

ViridianCity_Text_GymClosedWonderWhoLeaderIs::
    .string "This POKéMON GYM is always closed.\p"
    .string "I wonder who the LEADER is?$"

ViridianCity_Text_ViridiansGymLeaderReturned::
    .string "VIRIDIAN GYM's LEADER returned!$"

ViridianCity_Text_WantToKnowAboutCaterpillarMons::
    .string "You want to know about the 2\n"
    .string "kinds of caterpillar POKéMON?$"

ViridianCity_Text_OhOkayThen::
    .string "Oh, okay then!$"

ViridianCity_Text_ExplainCaterpieWeedle::
    .string "CATERPIE has no poison,\n"
    .string "but WEEDLE does.\p"
    .string "Watch out for its\n"
    .string "POISON STING!$"

ViridianCity_Text_GrandpaHasntHadCoffeeYet::
    .string "Oh Grandpa!\n"
    .string "Don't be so mean!\p"
    .string "He hasn't had his\n"
    .string "coffee yet.$"

ViridianCity_Text_GoShoppingInPewterOccasionally::
    .string "When I go shop in PEWTER CITY,\n"
    .string "I have to take the winding\p"
    .string "trail in VIRIDIAN FOREST.$"

ViridianCity_Text_ThisIsPrivateProperty::
    .string "You can't go through here!\n"
    .string "This is private property!$"

ViridianCity_Text_HadMyCoffee::
    .string "Ahh, I've had my coffee now\n"
    .string "and I feel great!\p"
    .string "Sure you can go through!\n"
    .string "Are you in a hurry?$"

ViridianCity_Text_ShowYouHowToCatchMons::
    .string "I see you're using a POKéDEX.\n"
    .string "When you catch a POKéMON,\p"
    .string "POKéDEX is automatically\n"
    .string "updated. What? don't you\p"
    .string "know how to catch POKéMON?\n"
    .string "I'll show you how to then.$"

ViridianCity_Text_DeclineTutorial::
    .string "Time is money...\n"
    .string "Go along then.$"

ViridianCity_Text_FirstWeaken::
    .string "First, you need to weaken\n"
    .string "the target POKéMON.$"

ViridianCity_Text_CitySign::
    .string "VIRIDIAN CITY \n"
    .string "The Eternally Green Paradise$"

ViridianCity_Text_CatchMonsForEasierBattles::
    .string "TRAINER TIPS\p"
    .string "Catch POKéMON and expand\n"
    .string "your collection!\p"
    .string "The more you have, the easier\n"
    .string "it is to fight!$"

ViridianCity_Text_MovesLimitedByPP::
    .string "TRAINER TIPS\p"
    .string "The battle moves of POKéMON are\n"
    .string "limited by their POWER POINTs, PP.\p"
    .string "To replenish PP, rest your tired\n"
    .string "POKéMON at a POKéMON CENTER!$"

ViridianCity_Text_GymSign::
    .string "VIRIDIAN CITY POKéMON GYM$"

ViridianCity_Text_GymDoorsAreLocked::
    .string "The GYM's doors are locked...$"

