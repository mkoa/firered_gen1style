PokemonTower_6F_Text_AngelicaIntro::
    .string "Give...me...\n"
    .string "blood...$"

PokemonTower_6F_Text_AngelicaDefeat::
    .string "CHANNELER: Groan!$"

PokemonTower_6F_Text_AngelicaPostBattle::
    .string "I feel anemic and weak...$"

PokemonTower_6F_Text_EmiliaIntro::
    .string "Urff...\n"
    .string "Kwaah!$"

PokemonTower_6F_Text_EmiliaDefeat::
    .string "CHANNELER: Something fell out!$"

PokemonTower_6F_Text_EmiliaPostBattle::
    .string "Hair didn't fall out!\n"
    .string "It was an evil spirit!$"

PokemonTower_6F_Text_JenniferIntro::
    .string "Ke...ke...ke...\n"
    .string "ke..ke...ke!!$"

PokemonTower_6F_Text_JenniferDefeat::
    .string "CHANNELER: Keee!$"

PokemonTower_6F_Text_JenniferPostBattle::
    .string "What's going on here?$"

PokemonTower_6F_Text_BeGoneIntruders::
    .string "Be gone...\n"
    .string "Intruders...$"

PokemonTower_6F_Text_GhostWasCubonesMother::
    .string "The GHOST was the restless soul\n"
    .string "of CUBONE's mother!$"

PokemonTower_6F_Text_MothersSpiritWasCalmed::
    .string "The mother's soul was calmed.\p"
    .string "It departed to the afterlife!$"

