CeladonCity_Gym_Text_ErikaIntro::
    .string "Hello.\n"
    .string "Lovely weather isn't it?\l"
    .string "It's so pleasant.\p"
    .string "...Oh dear...\n"
    .string "I must have dozed off. Welcome.\p"
    .string "My name is ERIKA.\n"
    .string "I am the LEADER of CELADON GYM.\p"
    .string "I teach the art of\n"
    .string "flower arranging.\p"
    .string "My POKéMON are of the\n"
    .string "grass-type.\p"
    .string "Oh, I'm sorry, I had no idea that\n"
    .string "you wished to challenge me.\p"
    .string "Very well, but I shall not lose.{PLAY_BGM MUS_ENCOUNTER_GYM_LEADER}$"

CeladonCity_Gym_Text_ErikaDefeat::
    .string "ERIKA: Oh!\n"
    .string "I concede defeat.\l"
    .string "You are remarkably strong.\p"
    .string "I must confer you the\n"
    .string "RAINBOWBADGE.$"

CeladonCity_Gym_Text_ErikaPostBattle::
    .string "You are cataloging POKéMON?\n"
    .string "I must say I'm impressed.\p"
    .string "I would never collect POKéMON if\n"
    .string "they were unattractive.$"

CeladonCity_Gym_Text_ExplainRainbowBadgeTakeThis::
    .string "The RAINBOWBADGE will make\n"
    .string "POKéMON up to L50 obey.\p"
    .string "It also allows POKéMON to use\n"
    .string "STRENGTH in and out of battle.\p"
    .string "Please also take this with you.$"

CeladonCity_Gym_Text_ReceivedTM21FromErika::
    .string "{PLAYER} received TM21!$"

CeladonCity_Gym_Text_ExplainTM21::
    .string "TM21 contains MEGA DRAIN.\p"
    .string "Half the damage it inflicts is\n"
    .string "drained to heal your POKéMON!$"

CeladonCity_Gym_Text_ShouldMakeRoomForThis::
    .string "You should make room for this.$"

CeladonCity_Gym_Text_KayIntro::
    .string "Hey!\n"
    .string "You are not allowed in here!$"

CeladonCity_Gym_Text_KayDefeat::
    .string "LASS: You're too rough!$"

CeladonCity_Gym_Text_KayPostBattle::
    .string "Bleaah!\n"
    .string "I hope ERIKA wipes you out!$"

CeladonCity_Gym_Text_BridgetIntro::
    .string "Oh, welcome.\n"
    .string "I was getting bored.$"

CeladonCity_Gym_Text_BridgetDefeat::
    .string "BEAUTY: My makeup!$"

CeladonCity_Gym_Text_BridgetPostBattle::
    .string "Grass-type POKéMON are tough\n"
    .string "against the water-type!\p"
    .string "They also have an edge on rock\n"
    .string "and ground POKéMON!$"

CeladonCity_Gym_Text_TinaIntro::
    .string "Aren't you the peeping Tom?$"

CeladonCity_Gym_Text_TinaDefeat::
    .string "JR.TRAINER♀: I'm in shock!$"

CeladonCity_Gym_Text_TinaPostBattle::
    .string "Oh, you weren't peeping?\n"
    .string "We get a lot of gawkers!$"

CeladonCity_Gym_Text_TamiaIntro::
    .string "Look at my grass POKéMON!\n"
    .string "They're easy to raise!$"

CeladonCity_Gym_Text_TamiaDefeat::
    .string "BEAUTY: No!$"

CeladonCity_Gym_Text_TamiaPostBattle::
    .string "We only use grass-type POKéMON\n"
    .string "at our GYM!\p"
    .string "We also use them for making\n"
    .string "flower arrangements!$"

CeladonCity_Gym_Text_LisaIntro::
    .string "Don't bring any bugs or\n"
    .string "fire POKéMON in here!$"

CeladonCity_Gym_Text_LisaDefeat::
    .string "LASS: Oh!\n"
    .string "You!$"

CeladonCity_Gym_Text_LisaPostBattle::
    .string "Our LEADER, ERIKA, might be quiet,\n"
    .string "but she's also very skilled!$"

CeladonCity_Gym_Text_LoriIntro::
    .string "Pleased to meet you.\n"
    .string "My hobby is POKéMON training.$"

CeladonCity_Gym_Text_LoriDefeat::
    .string "BEAUTY: Oh!\n"
    .string "Splendid!$"

CeladonCity_Gym_Text_LoriPostBattle::
    .string "I have a blind date coming up.\n"
    .string "I have to learn to be polite.$"

CeladonCity_Gym_Text_MaryIntro::
    .string "Welcome to CELADON GYM!\p"
    .string "You better not underestimate\n"
    .string "girl power!$"

CeladonCity_Gym_Text_MaryDefeat::
    .string "COOLTRAINER♀: Oh!\n"
    .string "Beaten!$"

CeladonCity_Gym_Text_MaryPostBattle::
    .string "I didn't bring my best POKéMON!\n"
    .string "Wait 'til next time!$"

CeladonCity_Gym_Text_GymStatue::
    .string "CELADON POKéMON GYM\n"
    .string "LEADER: ERIKA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

CeladonCity_Gym_Text_GymStatuePlayerWon::
    .string "CELADON POKéMON GYM\n"
    .string "LEADER: ERIKA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

