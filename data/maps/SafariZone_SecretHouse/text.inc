SafariZone_SecretHouse_Text_CongratsYouveWon::
    .string "Ah! Finally!\p"
    .string "You're the first person to reach\n"
    .string "the SECRET HOUSE!\p"
    .string "I was getting worried that no one\n"
    .string "would win our campaign prize.\p"
    .string "Congratulations!\n"
    .string "You have won!$"

SafariZone_SecretHouse_Text_ReceivedHM03FromAttendant::
    .string "{PLAYER} received HM03!$"

SafariZone_SecretHouse_Text_ExplainSurf::
    .string "HM03 is SURF!\p"
    .string "POKéMON will be able to ferry\n"
    .string "you across water!\p"
    .string "And, this HM isn't disposable!\n"
    .string "You can use it over and over!\p"
    .string "You're super lucky for winning\n"
    .string "this fabulous prize!$"

SafariZone_SecretHouse_Text_DontHaveRoomForPrize::
    .string "You don't have room for this\n"
    .string "fabulous prize!$"

