Route9_Text_AliciaIntro::
    .string "You have POKéMON with you!\n"
    .string "You're mine!$"

Route9_Text_AliciaDefeat::
    .string "JR.TRAINER♀: You deceived me!$"

Route9_Text_AliciaPostBattle::
    .string "You need light to get through\n"
    .string "that dark tunnel ahead.$"

Route9_Text_ChrisIntro::
    .string "Who's that walking with those\n"
    .string "good looking POKéMON?$"

Route9_Text_ChrisDefeat::
    .string "JR.TRAINER♂: Out like a light!$"

Route9_Text_ChrisPostBattle::
    .string "Keep walking!$"

Route9_Text_DrewIntro::
    .string "I'm taking ROCK TUNNEL\n"
    .string "to go to LAVENDER...$"

Route9_Text_DrewDefeat::
    .string "JR.TRAINER♂: Can't measure up...$"

Route9_Text_DrewPostBattle::
    .string "Are you off to ROCK TUNNEL too?$"

Route9_Text_CaitlinIntro::
    .string "Don't you dare condescend me!$"

Route9_Text_CaitlinDefeat::
    .string "JR.TRAINER♀: No!\n"
    .string "You're too much!$"

Route9_Text_CaitlinPostBattle::
    .string "You're obviously talented!\n"
    .string "Good luck to you!$"

Route9_Text_JeremyIntro::
    .string "Bwahaha!\n"
    .string "Great! I was bored, eh!$"

Route9_Text_JeremyDefeat::
    .string "HIKER: Keep it coming, eh!\p"
    .string "Oh wait.\n"
    .string "I'm out of POKéMON!$"

Route9_Text_JeremyPostBattle::
    .string "You sure had guts standing up to\n"
    .string "me there, eh?$"

Route9_Text_BriceIntro::
    .string "Hahaha!\n"
    .string "Aren't you a little toughie!$"

Route9_Text_BriceDefeat::
    .string "HIKER: What's that?$"

Route9_Text_BricePostBattle::
    .string "Hahaha!\n"
    .string "Kids should be tough!$"

Route9_Text_BrentIntro::
    .string "I got up early every day to raise\n"
    .string "my POKéMON from cocoons!$"

Route9_Text_BrentDefeat::
    .string "BUG CATCHER: WHAT?\p"
    .string "What a total waste of time!$"

Route9_Text_BrentPostBattle::
    .string "I have to collect more than bugs\n"
    .string "to get stronger...$"

Route9_Text_AlanIntro::
    .string "Hahahaha!\n"
    .string "Come on, dude!!$"

Route9_Text_AlanDefeat::
    .string "HIKER: Hahahaha!\n"
    .string "You beat me fair!$"

Route9_Text_AlanPostBattle::
    .string "Hahahaha!\n"
    .string "Us hearty guys always laugh!$"

Route9_Text_ConnerIntro::
    .string "Go, my super bug POKéMON!$"

Route9_Text_ConnerDefeat::
    .string "BUG CATCHER: My bugs...$"

Route9_Text_ConnerPostBattle::
    .string "If you don't like bug POKéMON,\n"
    .string "you bug me!$"

Route9_Text_RouteSign::
    .string "ROUTE 9\n"
    .string "CERULEAN CITY - ROCK TUNNEL$"

