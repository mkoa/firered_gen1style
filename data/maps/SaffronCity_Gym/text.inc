SaffronCity_Gym_Text_SabrinaIntro::
    .string "I had a vision of your arrival!\p"
    .string "I have had psychic powers\n"
    .string "since I was a child.\p"
    .string "I first learned to bend\n"
    .string "spoons with my mind.\p"
    .string "I dislike fighting, but if you\n"
    .string "wish, I will show you my powers!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

SaffronCity_Gym_Text_SabrinaDefeat::
    .string "I'm shocked!\n"
    .string "But, a loss is a loss.\p"
    .string "I admit I didn't work hard\n"
    .string "enough to win!\p"
    .string "You earned the MARSHBADGE!$"

SaffronCity_Gym_Text_SabrinaPostBattle::
    .string "Psychic power isn't something that\n"
    .string "only a few people have.\p"
    .string "Everyone has psychic power.\n"
    .string "People just don't realize it.$"

SaffronCity_Gym_Text_ExplainMarshBadgeTakeThis::
    .string "The MARSHBADGE makes POKéMON\n"
    .string "up to L70 obey you!\p"
    .string "Stronger POKéMON will become wild,\n"
    .string "ignoring your orders in battle!\p"
    .string "Just don't raise your\n"
    .string "POKéMON too much!\p"
    .string "Wait, please take this TM with you!$"

SaffronCity_Gym_Text_ReceivedTM04FromSabrina::
    .string "{PLAYER} got TM46!$"

SaffronCity_Gym_Text_ExplainTM04::
    .string "TM04 is PSYWAVE!\p"
    .string "It uses powerful psychic waves\n"
    .string "to inflict damage!$"

SaffronCity_Gym_Text_BagFullOfOtherItems::
    .string "Your pack is full of other items!$"

SaffronCity_Gym_Text_AmandaIntro::
    .string "SABRINA is younger than I,\n"
    .string "but I respect her!$"

SaffronCity_Gym_Text_AmandaDefeat::
    .string "CHANNELER: Not good enough!$"

SaffronCity_Gym_Text_AmandaPostBattle::
    .string "In a battle of equals, the one\n"
    .string "with the stronger will wins!\p"
    .string "If you wish to beat SABRINA,\n"
    .string "focus on winning!$"

SaffronCity_Gym_Text_JohanIntro::
    .string "Does our unseen power scare you?$"

SaffronCity_Gym_Text_JohanDefeat::
    .string "PSYCHIC: I never foresaw this!$"

SaffronCity_Gym_Text_JohanPostBattle::
    .string "Psychic POKéMON fear only\n"
    .string "ghosts and bugs!$"

SaffronCity_Gym_Text_StacyIntro::
    .string "POKéMON take on the appearance\n"
    .string "of their trainers.\p"
    .string "Your POKéMON must be tough, then!$"

SaffronCity_Gym_Text_StacyDefeat::
    .string "CHANNELER: I knew it!$"

SaffronCity_Gym_Text_StacyPostBattle::
    .string "I must teach better techniques\p"
    .string "to my POKéMON!$"

SaffronCity_Gym_Text_TyronIntro::
    .string "You know that power alone\n"
    .string "isn't enough!$"

SaffronCity_Gym_Text_TyronDefeat::
    .string "PSYCHIC: I don't believe this!$"

SaffronCity_Gym_Text_TyronPostBattle::
    .string "SABRINA just wiped out the\n"
    .string "KARATE MASTER next door!$"

SaffronCity_Gym_Text_TashaIntro::
    .string "You and I, our POKéMON\n"
    .string "shall fight!$"

SaffronCity_Gym_Text_TashaDefeat::
    .string "CHANNELER: I lost after all!$"

SaffronCity_Gym_Text_TashaPostBattle::
    .string "I knew that this was going\n"
    .string "to take place.$"

SaffronCity_Gym_Text_CameronIntro::
    .string "SABRINA is young, but she's\n"
    .string "also our LEADER!\p"
    .string "You won't reach her easily!$"

SaffronCity_Gym_Text_CameronDefeat::
    .string "PSYCHIC: I lost\n"
    .string "my concentration!$"

SaffronCity_Gym_Text_CameronPostBattle::
    .string "There used to be 2 POKéMON\n"
    .string "GYMs in SAFFRON.\p"
    .string "The FIGHTING DOJO next door\n"
    .string "lost its GYM status when we\p"
    .string "went and creamed them!$"

SaffronCity_Gym_Text_PrestonIntro::
    .string "SAFFRON POKéMON GYM is famous\n"
    .string "for its psychics!\p"
    .string "You want to see SABRINA!\n"
    .string "I can tell!$"

SaffronCity_Gym_Text_PrestonDefeat::
    .string "PSYCHIC: Arrrgh!$"

SaffronCity_Gym_Text_PrestonPostBattle::
    .string "That's right! I used telepathy\n"
    .string "to read your mind!$"

SaffronCity_Gym_Text_GymGuyAdvice::
    .string "Yo!\n"
    .string "Champ in the making!\p"
    .string "SABRINA's POKéMON use psychic\n"
    .string "power instead of force!\p"
    .string "Fighting POKéMON are weak\n"
    .string "against psychic POKéMON!\p"
    .string "They get creamed before they\n"
    .string "can even aim a punch!$"

SaffronCity_Gym_Text_GymGuyPostVictory::
    .string "Psychic power, huh?\p"
    .string "If I had that, I'd make a bundle\n"
    .string "at the slots!$"

SaffronCity_Gym_Text_GymStatue::
    .string "SAFFRON CITY POKéMON GYM\n"
    .string "LEADER: SABRINA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

SaffronCity_Gym_Text_GymStatuePlayerWon::
    .string "SAFFRON CITY POKéMON GYM\n"
    .string "LEADER: SABRINA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

