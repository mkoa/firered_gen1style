SilphCo_8F_Text_WonderIfSilphIsFinished::
    .string "I wonder if SILPH is finished...$"

SilphCo_8F_Text_ThanksForSavingUs::
    .string "Thanks for saving us!$"

SilphCo_8F_Text_Grunt1Intro::
    .string "That's as far as you'll go!$"

SilphCo_8F_Text_Grunt1Defeat::
    .string "ROCKET: Not enough grit!$"

SilphCo_8F_Text_Grunt1PostBattle::
    .string "If you don't turn back,\n"
    .string "I'll call for backup!$"

SilphCo_8F_Text_ParkerIntro::
    .string "You're causing us problems!$"

SilphCo_8F_Text_ParkerDefeat::
    .string "SCIENTIST: Huh?\n"
    .string "I lost?$"

SilphCo_8F_Text_ParkerPostBattle::
    .string "So, what do you think of SILPH\n"
    .string "BUILDING's maze?$"

SilphCo_8F_Text_Grunt2Intro::
    .string "I am one of the 4 ROCKET\n"
    .string "BROTHERS!$"

SilphCo_8F_Text_Grunt2Defeat::
    .string "ROCKET: Whoo!\n"
    .string "Oh brothers!$"

SilphCo_8F_Text_Grunt2PostBattle::
    .string "I'll leave you up to my brothers!$"

SilphCo_8F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "8F$"

SilphCo_8F_Text_ToRocketBossMonsAreTools::
    .string "TEAM ROCKET's BOSS is terribly\n"
    .string "cruel!\p"
    .string "To him, POKéMON are just tools to\n"
    .string "be used.\p"
    .string "What will happen if that tyrant\n"
    .string "takes over our company…$"
