SafariZone_West_Text_KogaPatrolsSafariEverySoOften::
    .string "The SAFARI ZONE's huge, wouldn't\n"
    .string "you say?\p"
    .string "FUCHSIA's GYM LEADER, KOGA, \n"
    .string "patrols the grounds every so often.\p"
    .string "Thanks to him, we can play here\n"
    .string "knowing that we're safe.$"

SafariZone_West_Text_RocksMakeMonRunButEasierCatch::
    .string "Tossing ROCKs at POKéMON might\n"
    .string "make them run, but they'll be\l"
    .string "easier to catch.$"

SafariZone_West_Text_BaitMakesMonStickAround::
    .string "Using BAIT will make POKéMON\n"
    .string "easier to catch.$"

SafariZone_West_Text_HikedLotsDidntSeeMonIWanted::
    .string "I hiked a lot, but I didn't see\n"
    .string "and POKéMON I wanted.$"

