CinnabarIsland_PokemonLab_ResearchRoom_Text_EeveeCanEvolveIntroThreeMons::
    .string "EEVEE can evolve into 1 of 3\n"
    .string "kinds of POKéMON.$"

CinnabarIsland_PokemonLab_ResearchRoom_Text_LegendaryBirdEmail::
    .string "There's an e-mail message.\p"
    .string "...\p"
    .string "The 3 legendary bird POKéMON are\n"
    .string "ARTICUNO, ZAPDOS and MOLTRES.\p"
    .string "Their whereabouts are unknown.\n"
    .string "We plan to explore the cavern\n"
    .string "close to CERULEAN.\p"
    .string "From: POKéMON RESEARCH TEAM\n"
    .string "...$"

CinnabarIsland_PokemonLab_ResearchRoom_Text_AnAmberPipe::
    .string "An amber pipe!$"

