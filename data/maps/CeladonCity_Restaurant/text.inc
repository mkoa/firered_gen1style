CeladonCity_Restaurant_Text_TakingBreakRightNow::
    .string "Hi!\p"
    .string "We're taking a break\n"
    .string "right now.$"

CeladonCity_Restaurant_Text_OftenGoToDrugstore::
    .string "My POKéMON are weak, so I often\n"
    .string "have to go to the DRUG STORE.$"

CeladonCity_Restaurant_Text_PsstBasementUnderGameCorner::
    .string "Psst! There's a basement\n"
    .string "under the GAME CORNER!$"

CeladonCity_Restaurant_Text_ManLostItAllAtSlots::
    .string "Munch...\p"
    .string "The man at that table lost\n"
    .string "it all at the slots.$"

CeladonCity_Restaurant_Text_TakeThisImBusted::
    .string "Go ahead! Laugh!\n"
    .string "I'm flat out busted!\p"
    .string "No more slots for me!\n"
    .string "I'm going straight!\p"
    .string "Here!\n"
    .string "I won't be needing this any more!$"

CeladonCity_Restaurant_Text_ReceivedCoinCaseFromMan::
    .string "{PLAYER} received a COIN CASE!$"

CeladonCity_Restaurant_Text_MakeRoomForThis::
    .string "Make room for this!$"

CeladonCity_Restaurant_Text_ThoughtIdWinItBack::
    .string "I always thought I was going\n"
    .string "to win it back...$"

