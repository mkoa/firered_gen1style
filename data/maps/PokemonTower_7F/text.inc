PokemonTower_7F_Text_Grunt1Intro::
    .string "What do you want?\n"
    .string "Why are you here?$"

PokemonTower_7F_Text_Grunt1Defeat::
    .string "ROCKET: I give up!$"

PokemonTower_7F_Text_Grunt1PostBattle::
    .string "I'm not going to forget this!$"

PokemonTower_7F_Text_Grunt2Intro::
    .string "This old guy came and complained\n"
    .string "about us harming useless POKéMON!\p"
    .string "We're talking it over as adults!$"

PokemonTower_7F_Text_Grunt2Defeat::
    .string "ROCKET: Please!\n"
    .string "No more!$"

PokemonTower_7F_Text_Grunt2PostBattle::
    .string "POKéMON are only good for\n"
    .string "making money!\p"
    .string "Stay out of our business!$"

PokemonTower_7F_Text_Grunt3Intro::
    .string "You're not saving anyone, kid!$"

PokemonTower_7F_Text_Grunt3Defeat::
    .string "ROCKET: Don't fight us ROCKETs!$"

PokemonTower_7F_Text_Grunt3PostBattle::
    .string "You're not getting away with this!$"

PokemonTower_7F_Text_MrFujiThankYouFollowMe::
    .string "MR.FUJI: Heh?\n"
    .string "You came to save me?\p"
    .string "Thank you. But, I came here\n"
    .string "of my own free will.\p"
    .string "I came to calm the soul\n"
    .string "of CUBONE's mother.\p"
    .string "I think MAROWAK's spirit has\n"
    .string "gone to the afterlife.\p"
    .string "I must thank you for\n"
    .string "your kind concern!\p"
    .string "Follow me to my home, POKéMON\n"
    .string "HOUSE, at the foot of this tower.$"

