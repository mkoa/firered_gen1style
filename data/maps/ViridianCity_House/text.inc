ViridianCity_House_Text_NicknamingIsFun::
    .string "Coming up with nicknames\n"
    .string "is fun, but hard.\p"
    .string "Simple names are the\n"
    .string "easiest to remember.$"

ViridianCity_House_Text_MyDaddyLovesMonsToo::
    .string "My daddy loves POKéMON too.$"

ViridianCity_House_Text_Speary::
    .string "SPEARY: Tetweet!$"

ViridianCity_House_Text_SpearowNameSpeary::
    .string "SPEAROW\n"
    .string "Name: SPEARY$"

