SilphCo_10F_Text_GruntIntro::
    .string "Welcome to the 10F!\n"
    .string "So good of you to join me!$"

SilphCo_10F_Text_GruntDefeat::
    .string "ROCKET: I'm stunned!$"

SilphCo_10F_Text_GruntPostBattle::
    .string "Nice try, but the boardroom is up\n"
    .string "one more floor!$"

SilphCo_10F_Text_TravisIntro::
    .string "Enough of your silly games!$"

SilphCo_10F_Text_TravisDefeat::
    .string "SCIENTIST: No continues left!$"

SilphCo_10F_Text_TravisPostBattle::
    .string "Are you satisfied with beating me?\n"
    .string "Then go on home!$"

SilphCo_10F_Text_WaaaImScared::
    .string "Waaaaa!\n"
    .string "I'm scared!$"

SilphCo_10F_Text_KeepMeCryingASecret::
    .string "Please keep quiet about\n"
    .string "my crying!$"

SilphCo_10F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "10F$"

