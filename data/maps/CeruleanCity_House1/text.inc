CeruleanCity_House1_Text_BadgesHaveAmazingSecrets::
    .string "POKéMON BADGEs are owned\n"
    .string "only by skilled trainers.\p"
    .string "I see you have at least one.\p"
    .string "Those BADGEs have amazing secrets!$"

CeruleanCity_House1_Text_DescribeWhichBadge::
    .string "Now then...\p"
    .string "Which of the 8 BADGEs\n"
    .string "should I describe?$"

CeruleanCity_House1_Text_ComeVisitAnytime::
    .string "Come visit me any time you wish.$"

CeruleanCity_House1_Text_AttackStatFlash::
    .string "The ATTACK of all POKéMON\n"
    .string "increases a little bit.\p"
    .string "It also lets you use FLASH\n"
    .string "any time you desire.$"

CeruleanCity_House1_Text_ObeyLv30Cut::
    .string "POKéMON up to L30\n"
    .string "will obey you.\p"
    .string "Any higher, they\n"
    .string "become unruly!\p"
    .string "It also lets you use\n"
    .string "CUT outside of battle.$"

CeruleanCity_House1_Text_SpeedStatFly::
    .string "The SPEED of all POKéMON\n"
    .string "increases a little bit.\p"
    .string "It also lets you use\n"
    .string "FLY outside of battle.$"

CeruleanCity_House1_Text_ObeyLv50Strength::
    .string "POKéMON up L50\n"
    .string "will obey you.\p"
    .string "Any higher, they\n"
    .string "become unruly!\p"
    .string "It also lets you\n"
    .string "use STRENGTH out-$"
    .string "side of battle."

CeruleanCity_House1_Text_DefenseStatSurf::
    .string "The DEFENSE all POKéMON\n"
    .string "increases a little bit.\p"
    .string "It also lets you use\n"
    .string "SURF outside of battle.$"

CeruleanCity_House1_Text_ObeyLv70RockSmash::
    .string "POKéMON up to L70\n"
    .string "will obey you.\p"
    .string "Any higher, they\n"
    .string "become unruly!$"

CeruleanCity_House1_Text_SpStatsWaterfall::
    .string "Your POKéMON's SPECIAL\n"
    .string "abilities increase a bit.$"

CeruleanCity_House1_Text_AllMonsWillObeyYou::
    .string "All POKéMON will obey you!$"

