ViridianCity_Mart_Text_YouCameFromPallet::
    .string "Hey!\n"
    .string "You came from PALLET TOWN?$"

ViridianCity_Mart_Text_TakeThisToProfOak::
    .string "You know PROF. OAK, right?\p"
    .string "His order came in.\n"
    .string "Will you take it to him?$"

ViridianCity_Mart_Text_ReceivedOaksParcelFromClerk::
    .string "{PLAYER} got OAK'S PARCEL!$"

ViridianCity_Mart_Text_SayHiToOakForMe::
    .string "Okay! Say hi to\n"
    .string "PROF. OAK for me!$"

ViridianCity_Mart_Text_ShopDoesGoodBusinessInAntidotes::
    .string "This shop sells\n"
    .string "many ANTIDOTEs.$"

ViridianCity_Mart_Text_GotToBuySomePotions::
    .string "No! POTIONs are\n"
    .string "all sold out.$"

