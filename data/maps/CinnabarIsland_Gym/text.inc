CinnabarIsland_Gym_Text_BlaineIntro::
    .string "Hah!\p"
    .string "I am BLAINE! I am the\n"
    .string "LEADER of CINNEBAR GYM!\p"
    .string "My fiery POKéMON will\n"
    .string "incinerate all challengers!\p"
    .string "Hah!\n"
    .string "You better have BURN HEAL!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

CinnabarIsland_Gym_Text_BlaineDefeat::
    .string "BLAINE: I have burnt out!\n"
    .string "You have earned the VOLCANOBADGE!$"

CinnabarIsland_Gym_Text_FireBlastIsUltimateFireMove::
    .string "FIRE BLAST is the ultimate\n"
    .string "fire technique!\p"
    .string "Don't waste it on water POKéMON!$"

CinnabarIsland_Gym_Text_ExplainVolcanoBadge::
    .string "Hah!\p"
    .string "The VOLCANOBADGE heightens the\n"
    .string "SPECIAL abilities of your POKéMON!\p"
    .string "Here, you can have this too!$"

CinnabarIsland_Gym_Text_ReceivedTM38FromBlaine::
    .string "{PLAYER} received TM38!$"

CinnabarIsland_Gym_Text_BlainePostBattle::
    .string "TM38 contains FIRE BLAST!\n"
    .string "Teach it to fire-type POKéMON!\p"
    .string "CHARMELEON or PONYTA would\n"
    .string "be good bets!$"

CinnabarIsland_Gym_Text_MakeSpaceForThis::
    .string "Make space for this, child!$"

CinnabarIsland_Gym_Text_ErikIntro::
    .string "Do you know how hot POKéMON fire\n"
    .string "breath can get?$"

CinnabarIsland_Gym_Text_ErikDefeat::
    .string "SUPER NERD: Yow!\n"
    .string "Hot, hot, hot!$"

CinnabarIsland_Gym_Text_ErikPostBattle::
    .string "Fire, or to be more precise,\n"
    .string "combustion...\p"
    .string "Blah, blah, blah, blah...$"

CinnabarIsland_Gym_Text_QuinnIntro::
    .string "I was a thief, but I became\n"
    .string "straight as a trainer!$"

CinnabarIsland_Gym_Text_QuinnDefeat::
    .string "BURGLAR: I surrender!$"

CinnabarIsland_Gym_Text_QuinnPostBattle::
    .string "I can't help stealing other\n"
    .string "people's POKéMON!$"

CinnabarIsland_Gym_Text_AveryIntro::
    .string "You can't win!\n"
    .string "I have studied POKéMON totally!$"

CinnabarIsland_Gym_Text_AveryDefeat::
    .string "SUPER NERD: Waah!\n"
    .string "My studies!$"

CinnabarIsland_Gym_Text_AveryPostBattle::
    .string "My theories are too complicated\n"
    .string "for you!$"

CinnabarIsland_Gym_Text_RamonIntro::
    .string "I just like using fire POKéMON!$"

CinnabarIsland_Gym_Text_RamonDefeat::
    .string "BURGLAR: Too hot to handle!$"

CinnabarIsland_Gym_Text_RamonPostBattle::
    .string "I wish there was a thief POKéMON!$"

CinnabarIsland_Gym_Text_DerekIntro::
    .string "I know why BLAINE became\n"
    .string "a trainer!$"

CinnabarIsland_Gym_Text_DerekDefeat::
    .string "SUPER NERD: Ow!$"

CinnabarIsland_Gym_Text_DerekPostBattle::
    .string "BLAINE was lost in the\n"
    .string "mountains when a fiery\p"
    .string "bird POKéMON appeared.\l"
    .string "Its light enabled BLAINE to\n"
    .string "find his way down!$"

CinnabarIsland_Gym_Text_DustyIntro::
    .string "I've been to many GYMs, but\n"
    .string "this is my favorite!$"

CinnabarIsland_Gym_Text_DustyDefeat::
    .string "BURGLAR: Yowza!\n"
    .string "Too hot!$"

CinnabarIsland_Gym_Text_DustyPostBattle::
    .string "Us fire POKéMON fans like\n"
    .string "PONYTA and NINETALES!$"

CinnabarIsland_Gym_Text_ZacIntro::
    .string "Fire is weak against H2O!$"

CinnabarIsland_Gym_Text_ZacDefeat::
    .string "SUPER NERD: Oh!\n"
    .string "Snuffed out!$"

CinnabarIsland_Gym_Text_ZacPostBattle::
    .string "Water beats fire!\p"
    .string "But, fire melts ice POKéMON!$"

CinnabarIsland_Gym_Text_GymGuyAdvice::
    .string "Yo!\n"
    .string "Champ in the making!\p"
    .string "The hot-headed BLAINE is a\n"
    .string "fire POKéMON pro!\p"
    .string "Douse his spirits with water!\p"
    .string "You'd better take some\n"
    .string "BURN HEALs!$"

CinnabarIsland_Gym_Text_GymGuyPostVictory::
    .string "{PLAYER}!\n"
    .string "You beat that fire brand!$"

CinnabarIsland_Gym_Text_GymStatue::
    .string "CINNABAR ISLAND POKéMON GYM\n"
    .string "LEADER: BLAINE\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

CinnabarIsland_Gym_Text_GymStatuePlayerWon::
    .string "CINNABAR ISLAND POKéMON GYM\n"
    .string "LEADER: BLAINE\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

CinnabarIsland_Gym_Text_PokemonQuizRules::
    .string "POKéMON Quiz!\p"
    .string "Get it right and the door\n"
    .string "opens to the next room!\p"
    .string "Get it wrong and face a trainer!\l"
    .string "If you want to conserve your\n"
    .string "POKéMON for the GYM LEADER...\p"
    .string "Then get it right!\n"
    .string "Here we go!$"

CinnabarIsland_Gym_Text_QuizQuestion1::
    .string "CATERPIE evolves into BUTTERFREE?$"

CinnabarIsland_Gym_Text_QuizQuestion2::
    .string "There are 9 certified POKéMON\n"
    .string "LEAGUE BADGEs?$"

CinnabarIsland_Gym_Text_QuizQuestion3::
    .string "POLIWAG evolves 3 times?$"

CinnabarIsland_Gym_Text_QuizQuestion4::
    .string "Are thunder moves effective\n"
    .string "against ground element-type POKéMON?$"

CinnabarIsland_Gym_Text_QuizQuestion5::
    .string "POKéMON of the same kind and level\n"
    .string "are not identical?$"

CinnabarIsland_Gym_Text_QuizQuestion6::
    .string "TM28 contains TOMBSTONER?$"

CinnabarIsland_Gym_Text_CorrectGoOnThrough::
    .string "You're absolutely correct!\p"
    .string "Go on through!$"

CinnabarIsland_Gym_Text_SorryBadCall::
    .string "Sorry!\n"
    .string "Bad call!$"

