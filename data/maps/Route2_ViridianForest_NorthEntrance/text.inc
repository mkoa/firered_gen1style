Route2_ViridianForest_NorthEntrance_Text_ManyMonsOnlyInForests::
    .string "Many POKéMON live only in\n"
    .string "forests and caves.\p"
    .string "You need to look everywhere\n"
    .string "to get different kinds!$"

Route2_ViridianForest_NorthEntrance_Text_CanCutSkinnyTrees::
    .string "Have you noticed the bushes\n"
    .string "on the roadside?\p"
    .string "They can be cut down by\n"
    .string "a special POKéMON move.$"

Route2_ViridianForest_NorthEntrance_Text_CanCancelEvolution::
    .string "Do you know the evolution-cancel\n"
    .string "technique?\p"
    .string "When a POKéMON is evolving, you\n"
    .string "can stop the process.\p"
    .string "It's a technique for raising\n"
    .string "POKéMON the way they are.$"

