SSAnne_CaptainsOffice_Text_CaptainIFeelSeasick::
    .string "CAPTAIN: Ooargh...\n"
    .string "I feel hideous...\l"
    .string "Urrp! Seasick...$"

SSAnne_CaptainsOffice_Text_RubbedCaptainsBack::
    .string "{PLAYER} rubbed the\n"
    .string "CAPTAIN's back!\p"
    .string "Rub-rub...\n"
    .string "Rub-rub...$"

SSAnne_CaptainsOffice_Text_ThankYouHaveHMForCut::
    .string "CAPTAIN: Whew! Thank you!\n"
    .string "I feel much better!\p"
    .string "You want to see my\n"
    .string "CUT technique?\p"
    .string "I could show you if\n"
    .string "I wasn't ill...\p"
    .string "I know! You can have this!\n"
    .string "Teach it to your POKéMON and\p"
    .string "you can see it CUT any time!$"

SSAnne_CaptainsOffice_Text_ObtainedHM01FromCaptain::
    .string "{PLAYER} obtained HM01\n"
    .string "from the CAPTAIN!$"

SSAnne_CaptainsOffice_Text_ExplainCut::
    .string "Using CUT, you can chop down\n"
    .string "small trees.\p"
    .string "Why not try it with the trees\n"
    .string "around VERMILION CITY?$"

SSAnne_CaptainsOffice_Text_SSAnneWillSetSailSoon::
    .string "CAPTAIN: Whew!\p"
    .string "Now that I'm not sick any more,\n"
    .string "I guess it's time.$"

SSAnne_CaptainsOffice_Text_YouHaveNoRoomForThis::
    .string "Oh, no!\n"
    .string "You have no room for this!$"

SSAnne_CaptainsOffice_Text_YuckShouldntHaveLooked::
    .string "Yuck!\n"
    .string "Shouldn't have looked!$"

SSAnne_CaptainsOffice_Text_HowToConquerSeasickness::
    .string "How to Conquer Seasickness...\n"
    .string "The CAPTAIN's reading this!$"

