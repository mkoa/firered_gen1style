PewterCity_Museum_1F_Text_Its50YForChildsTicket::
    .string "It's ¥50 for a child's ticket.\n"
    .string "Would you like to come in?$"

PewterCity_Museum_1F_Text_ComeAgain::
    .string "Come again!$"

PewterCity_Museum_1F_Text_Right50YThankYou::
    .string "Right, ¥50!\n"
    .string "Thank you!$"

PewterCity_Museum_1F_Text_DontHaveEnoughMoney::
    .string "You don't have enough money.$"

PewterCity_Museum_1F_Text_PleaseEnjoyYourself::
    .string "Please enjoy yourself.$"

PewterCity_Museum_1F_Text_DoYouKnowWhatAmberIs::
    .string "You can't sneak in the back way!\n"
    .string "Oh, whatever!\p"
    .string "Do you know what AMBER is?$"

PewterCity_Museum_1F_Text_AmberContainsGeneticMatter::
    .string "There's a lab somewhere trying to\n"
    .string "resurrect ancient POKéMON from AMBER.$"

PewterCity_Museum_1F_Text_AmberIsFossilizedSap::
    .string "AMBER is fossilized tree sap.$"

@ Unused
PewterCity_Museum_1F_Text_PleaseGoAround::
    .string "あちらへ　おまわりください$"

PewterCity_Museum_1F_Text_ShouldBeGratefulForLongLife::
    .string "That is one magnificent \n"
    .string "fossil!$"

PewterCity_Museum_1F_Text_WantYouToGetAmberExamined::
    .string "Ssh! I think that this chunk of\n"
    .string "AMBER contains POKéMON DNA!\p"
    .string "It would be great if POKéMON\n"
    .string "could be resurrected from it!\p"
    .string "But, my colleagues just\n"
    .string "ignore me!\p"
    .string "So I have a favor to ask!\n"
    .string "Take this to a POKéMON LAB\n"
    .string "and get it examined!$"

PewterCity_Museum_1F_Text_ReceivedOldAmberFromMan::
    .string "{PLAYER} received OLD AMBER!$"

PewterCity_Museum_1F_Text_GetOldAmberChecked::
    .string "Ssh!\n"
    .string "Get the OLD AMBER checked!$"

PewterCity_Museum_1F_Text_DontHaveSpaceForThis::
    .string "You don't have space for this!$"

PewterCity_Museum_1F_Text_WeHaveTwoFossilsOnExhibit::
    .string "We are proud of 2 fossils of\n"
    .string "very rare, prehistoric POKéMON!$"

PewterCity_Museum_1F_Text_BeautifulPieceOfAmber::
    .string "There is a beautiful piece of\n"
    .string "AMBER in a clear gold color.$"

PewterCity_Museum_1F_Text_AerodactylFossil::
    .string "AERODACTYL Fossil\n"
    .string "A primitive and rare POKéMON.$"

PewterCity_Museum_1F_Text_KabutopsFossil::
    .string "KABUTOPS Fossil\n"
    .string "A primitive and rare POKéMON.$"

