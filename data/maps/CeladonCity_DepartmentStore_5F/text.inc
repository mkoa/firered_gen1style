CeladonCity_DepartmentStore_5F_Text_ExplainStatEnhancers::
    .string "POKéMON ability enhancers\n"
    .string "can be bought only here.\p"
    .string "Use CALCIUM to increase\n"
    .string "SPECIAL abilities.\p"
    .string "Use CARBOS to increase SPEED.$"

CeladonCity_DepartmentStore_5F_Text_HereForStatEnhancers::
    .string "I'm here for POKéMON\n"
    .string "ability enhancers.\p"
    .string "PROTEIN increases ATTACK power.\n"
    .string "IRON increases DEFENSE!$"

CeladonCity_DepartmentStore_5F_Text_Drugstore::
    .string "5F: DRUG STORE$"

