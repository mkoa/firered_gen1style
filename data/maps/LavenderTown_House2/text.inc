LavenderTown_House2_Text_WantMeToRateNicknames::
    .string "Hello, hello!\n"
    .string "I am the official NAME RATER!\p"
    .string "Want me to rate the nicknames of\n"
    .string "your POKéMON?$"

LavenderTown_House2_Text_CritiqueWhichMonsNickname::
    .string "Which POKéMON should\n"
    .string "I look at?$"

LavenderTown_House2_Text_GiveItANicerName::
    .string "{STR_VAR_1}, is it?\n"
    .string "That is a decent nickname!\p"
    .string "But, would you like me to\n"
    .string "give it a nicer name?\p"
    .string "How about it?$"

LavenderTown_House2_Text_WhatShallNewNicknameBe::
    .string "Fine!\n"
    .string "What should we name it?$"

LavenderTown_House2_Text_FromNowOnShallBeKnownAsName::
    .string "OK! This POKéMON has been\n"
    .string "renamed {STR_VAR_1}!\p"
    .string "That's' a better name than before!$"

LavenderTown_House2_Text_ISeeComeVisitAgain::
    .string "Fine!\n"
    .string "Come any time you like!$"

LavenderTown_House2_Text_FromNowOnShallBeKnownAsSameName::
    .string "Done! From now on, this POKéMON\n"
    .string "shall be known as {STR_VAR_1}!\p"
    .string "It looks no different from before,\n"
    .string "and yet, this is vastly superior!\p"
    .string "How fortunate for you!$"

LavenderTown_House2_Text_TrulyImpeccableName::
    .string "{STR_VAR_1}, is it?\n"
    .string "That is a truly impeccable name!\p"
    .string "Take good care of {STR_VAR_1}!$"

LavenderTown_House2_Text_ThatIsMerelyAnEgg::
    .string "Now, now.\n"
    .string "That is merely an EGG!$"

