CeladonCity_DepartmentStore_4F_Text_GettingPokeDollAsPresent::
    .string "I'm getting a POKé DOLL\n"
    .string "for my girl friend!$"

CeladonCity_DepartmentStore_4F_Text_CanRunAwayWithPokeDoll::
    .string "I heard something useful.\p"
    .string "You can run from wild POKéMON by\n"
    .string "distracting them with a POKé DOLL!$"

CeladonCity_DepartmentStore_4F_Text_FloorSign::
    .string "Express yourself with gifts!\n"
    .string "4F: WISEMAN GIFTS\p"
    .string "Evolution Special!\n"
    .string "Element STONEs on sale now!$"
