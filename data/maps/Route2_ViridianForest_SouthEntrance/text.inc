Route2_ViridianForest_SouthEntrance_Text_ForestIsMaze::
    .string "Are you going to VIRIDIAN FOREST?\n"
    .string "Be careful, it's\l"
    .string "a natural maze!$"

Route2_ViridianForest_SouthEntrance_Text_RattataHasWickedBite::
    .string "RATTATA may be small, but its'\n"
    .string "bite is wicked!\p"
    .string "Did you get one?$"

