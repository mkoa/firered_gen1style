Route8_Text_AidanIntro::
    .string "You look good at POKéMON,\n"
    .string "but how's your chem?$"

Route8_Text_AidanDefeat::
    .string "SUPER NERD: Ow!\n"
    .string "Meltdown!$"

Route8_Text_AidanPostBattle::
    .string "I am better at school than this!$"

Route8_Text_StanIntro::
    .string "All right!\n"
    .string "Let's roll the dice!$"

Route8_Text_StanDefeat::
    .string "GAMBLER: Drat!\n"
    .string "Came up short!$"

Route8_Text_StanPostBattle::
    .string "Lady Luck's not with me today!$"

Route8_Text_GlennIntro::
    .string "You need strategy to win at this!$"

Route8_Text_GlennDefeat::
    .string "SUPER NERD: It's not logical!$"

Route8_Text_GlennPostBattle::
    .string "Go with GRIMER first...\n"
    .string "and...and...then...$"

Route8_Text_PaigeIntro::
    .string "I like NIDORAN, so I collect them!$"

Route8_Text_PaigeDefeat::
    .string "LASS: Why?\n"
    .string "Why??$"

Route8_Text_PaigePostBattle::
    .string "When POKéMON grow up they get\n"
    .string "ugly! They shouldn't evolve!$"

Route8_Text_LeslieIntro::
    .string "School is fun, but so are POKéMON.$"

Route8_Text_LeslieDefeat::
    .string "SUPER NERD: I'll stay with school.$"

Route8_Text_LesliePostBattle::
    .string "We're stuck here because of\n"
    .string "the gates at SAFFRON.$"

Route8_Text_AndreaIntro::
    .string "MEOWTH is so cute,\n"
    .string "meow, meow, meow!$"

Route8_Text_AndreaDefeat::
    .string "LASS: Meow!$"

Route8_Text_AndreaPostBattle::
    .string "I think PIDGEY and RATTATA\n"
    .string "are cute too!$"

Route8_Text_MeganIntro::
    .string "We must look silly standing\n"
    .string "here like this!$"

Route8_Text_MeganDefeat::
    .string "LASS: Look what you did!$"

Route8_Text_MeganPostBattle::
    .string "SAFFRON's gate keeper\n"
    .string "won't let us through.\p"
    .string "He's so mean!$"

Route8_Text_RichIntro::
    .string "I'm a rambling, gambling dude!$"

Route8_Text_RichDefeat::
    .string "GAMBLER: Missed the big score!$"

Route8_Text_RichPostBattle::
    .string "Gambling and POKéMON are like\n"
    .string "eating peanuts! Just can't stop!$"

Route8_Text_JuliaIntro::
    .string "What's a cute, round and\n"
    .string "fluffy POKéMON?$"

Route8_Text_JuliaDefeat::
    .string "LASS: Stop!\p"
    .string "Don't be so mean to my CLEFAIRY!$"

Route8_Text_JuliaPostBattle::
    .string "I heard that CLEFAIRY evolves when\n"
    .string "it's exposed to a MOON STONE.$"

Route8_Text_UndergroundPathSign::
    .string "UNDERGROUND PATH\n"
    .string "CELADON CITY - LAVENDER TOWN$"

Route8_Text_EliIntro::
    .string "ELI: Twin power is fantastic.\n"
    .string "Did you know?$"

Route8_Text_EliDefeat::
    .string "ELI: But…\n"
    .string "We used our twin power…$"

Route8_Text_EliPostBattle::
    .string "ELI: I caught my POKéMON with\n"
    .string "ANNE!$"

Route8_Text_EliNotEnoughMons::
    .string "ELI: We can't battle if you don't\n"
    .string "have two POKéMON.$"

Route8_Text_AnneIntro::
    .string "ANNE: We'll shock you with our twin\n"
    .string "power!$"

Route8_Text_AnneDefeat::
    .string "ANNE: Our twin power…$"

Route8_Text_AnnePostBattle::
    .string "ANNE: I'm raising POKéMON with\n"
    .string "ELI.$"

Route8_Text_AnneNotEnoughMons::
    .string "ANNE: Hi, hi! Let's battle!\n"
    .string "But bring two POKéMON.$"

Route8_Text_RicardoIntro::
    .string "My bike's acting up, man.$"

Route8_Text_RicardoDefeat::
    .string "Aww, man.\n"
    .string "I'm not into this.$"

Route8_Text_RicardoPostBattle::
    .string "I got grass caught up in my\n"
    .string "spokes, man.$"

Route8_Text_JarenIntro::
    .string "Clear the way, or I'll run you\n"
    .string "down!$"

Route8_Text_JarenDefeat::
    .string "You for real, kid?$"

Route8_Text_JarenPostBattle::
    .string "Don't think you're all special and\n"
    .string "all just because of this.$"
