CeladonCity_PokemonCenter_1F_Text_PokeFluteAwakensSleepingMons::
    .string "POKé FLUTE awakens POKéMON\n"
    .string "with a sound that only\l"
    .string "they can hear!$"

CeladonCity_PokemonCenter_1F_Text_RodeHereFromFuchsia::
    .string "I rode uphill on CYCLING ROAD\p"
    .string "from FUCHSIA!$"

CeladonCity_PokemonCenter_1F_Text_GoToCyclingRoadIfIHadBike::
    .string "If I had a BIKE, I would\n"
    .string "go to CYCLING ROAD!$"

