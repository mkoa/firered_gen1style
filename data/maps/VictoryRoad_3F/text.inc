Text_DoubleEdgeTeach::
    .string "You should be proud of yourself,\n"
    .string "having battled your way through\l"
    .string "VICTORY ROAD so courageously.\p"
    .string "In recognition of your feat,\n"
    .string "I'll teach you DOUBLE-EDGE.\p"
    .string "Would you like me to teach that\n"
    .string "technique?$"

Text_DoubleEdgeDeclined::
    .string "I'll teach you the technique\n"
    .string "anytime.$"

Text_DoubleEdgeWhichMon::
    .string "Which POKéMON should I teach\n"
    .string "DOUBLE-EDGE?$"

Text_DoubleEdgeTaught::
    .string "Keep that drive going for the\n"
    .string "POKéMON LEAGUE!\p"
    .string "Take a run at them and knock 'em\n"
    .string "out!$"

VictoryRoad_3F_Text_GeorgeIntro::
    .string "I heard rumors of a child prodigy!$"

VictoryRoad_3F_Text_GeorgeDefeat::
    .string "COOLTRAINER♂: The rumors were true!$"

VictoryRoad_3F_Text_GeorgePostBattle::
    .string "You beat GIOVANNI of\n"
    .string "TEAM ROCKET?$"

VictoryRoad_3F_Text_AlexaIntro::
    .string "TRAINERS live to seek stronger\n"
    .string "opponents!$"

VictoryRoad_3F_Text_AlexaDefeat::
    .string "COOLTRAINER♀: Oh!\n"
    .string "So strong!$"

VictoryRoad_3F_Text_AlexaPostBattle::
    .string "By fighting tough battles, you\n"
    .string "get stronger!$"

VictoryRoad_3F_Text_CarolineIntro::
    .string "I'll show you just how good\n"
    .string "you are!$"

VictoryRoad_3F_Text_CarolineDefeat::
    .string "COOLTRAINER♀: I'm furious!$"

VictoryRoad_3F_Text_CarolinePostBattle::
    .string "You showed me just how good\n"
    .string "I was!$"

VictoryRoad_3F_Text_ColbyIntro::
    .string "Only the chosen can pass here!$"

VictoryRoad_3F_Text_ColbyDefeat::
    .string "COOLTRAINER♂: I don't believe it!$"

VictoryRoad_3F_Text_ColbyPostBattle::
    .string "All trainers here are headed to\n"
    .string "the POKéMON LEAGUE!\p"
    .string "Be careful!$"

VictoryRoad_3F_Text_RayIntro::
    .string "RAY: Together, the two of us are\n"
    .string "destined for greatness!$"

VictoryRoad_3F_Text_RayDefeat::
    .string "RAY: Ludicrous!\n"
    .string "This can't be!$"

VictoryRoad_3F_Text_RayPostBattle::
    .string "RAY: You've beaten us.\n"
    .string "Greatness remains elusive…$"

VictoryRoad_3F_Text_RayNotEnoughMons::
    .string "RAY: Together, the two of us are\n"
    .string "striving for the pinnacle.\p"
    .string "We need you to bring two POKéMON\n"
    .string "into battle with us.$"

VictoryRoad_3F_Text_TyraIntro::
    .string "TYRA: We're trying to become\n"
    .string "champions together.$"

VictoryRoad_3F_Text_TyraDefeat::
    .string "TYRA: Oh, but…$"

VictoryRoad_3F_Text_TyraPostBattle::
    .string "TYRA: You've taught me that power\n"
    .string "can be infinite in shape and form.$"

VictoryRoad_3F_Text_TyraNotEnoughMons::
    .string "TYRA: You can't battle with us if\n"
    .string "you have only one POKéMON.$"

