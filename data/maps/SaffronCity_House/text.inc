SaffronCity_House_Text_DontLookAtMyLetter::
    .string "Thank you for writing.\n"
    .string "I hope to see you soon!\p"
    .string "Hey!\n"
    .string "Don't look at my letter!$"

SaffronCity_House_Text_Pidgey::
    .string "PIDGEY: Kurukkoo!$"

SaffronCity_House_Text_GettingCopycatPokeDoll::
    .string "The COPYCAT is cute!\n"
    .string "I'm getting her a POKé DOLL!$"

SaffronCity_House_Text_ExplainPPUp::
    .string "I was given a PP UP as a gift.\p"
    .string "It's used to for increasing the\n"
    .string "PP of techniques!$"

