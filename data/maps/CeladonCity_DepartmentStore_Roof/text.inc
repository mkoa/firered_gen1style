CeladonCity_DepartmentStore_Roof_Text_ImThirstyGiveHerDrink::
    .string "I'm thirsty!\n"
    .string "I want something to drink!\p"
    .string "{FONT_NORMAL}Give her a drink?$"

CeladonCity_DepartmentStore_Roof_Text_GiveWhichDrink::
    .string "Give her which drink?$"

CeladonCity_DepartmentStore_Roof_Text_YayFreshWaterHaveThis::
    .string "Yay!\p"
    .string "FRESH WATER!\p"
    .string "Thank you!\n"
    .string "You can have this from me!$"

Text_ReceivedItemFromLittleGirl::
    .string "{PLAYER} received a {STR_VAR_2}!$"

CeladonCity_DepartmentStore_Roof_Text_ExplainTM13::
    .string "TM13 contains ICE BEAM!$"

CeladonCity_DepartmentStore_Roof_Text_YaySodaPopHaveThis::
    .string "Yay!\p"
    .string "SODA POP!\p"
    .string "Thank you!\n"
    .string "You can have this from me!$"

CeladonCity_DepartmentStore_Roof_Text_ExplainTM48::
    .string "TM48 contains ROCK SLIDE!$"

CeladonCity_DepartmentStore_Roof_Text_YayLemonadeHaveThis::
    .string "Yay!\p"
    .string "LEMONADE!\p"
    .string "Thank you!\n"
    .string "You can have this from me!$"

CeladonCity_DepartmentStore_Roof_Text_ExplainTM49::
    .string "TM49 contains TRI ATTACK!$"

CeladonCity_DepartmentStore_Roof_Text_DontHaveSpaceForThis::
    .string "You don't have space for this!$"

CeladonCity_DepartmentStore_Roof_Text_ImNotThirstyAfterAll::
    .string "No, thank you!\n"
    .string "I'm not thirsty after all!$"

CeladonCity_DepartmentStore_Roof_Text_MySisterIsImmature::
    .string "My sister is a trainer,\n"
    .string "believe it or not.\p"
    .string "But, she's so immature,\n"
    .string "she drives me nuts!$"

CeladonCity_DepartmentStore_Roof_Text_ImThirstyIWantDrink::
    .string "I'm thirsty!\n"
    .string "I want something to drink!$"

CeladonCity_DepartmentStore_Roof_Text_FloorSign::
    .string "ROOFTOP SQUARE:\n"
    .string "VENDING MACHINES$"

CeladonCity_DepartmentStore_Roof_Text_VendingMachineWhatDoesItHave::
    .string "A vending machine!\n"
    .string "Here's the menu!$"

CeladonCity_DepartmentStore_Roof_Text_NotEnoughMoney::
    .string "Oops, not enough money!$"

CeladonCity_DepartmentStore_Roof_Text_DrinkCanPoppedOut::
    .string "{STR_VAR_1} popped out!$"

CeladonCity_DepartmentStore_Roof_Text_NoMoreRoomForStuff::
    .string "There's no more room for stuff!$"

CeladonCity_DepartmentStore_Roof_Text_NotThirsty::
    .string "Not thirsty!$"

