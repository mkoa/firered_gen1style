Route24_Text_JustEarnedFabulousPrize::
    .string "Congratulations! You beat our\n"
    .string "5 contest trainers!\p"
    .string "You just earned a fabulous prize!$"

Route24_Text_ReceivedNuggetFromMysteryTrainer::
    .string "{PLAYER} received a NUGGET!$"

Route24_Text_YouDontHaveAnyRoom::
    .string "You don't have any room!$"

Route24_Text_JoinTeamRocket::
    .string "By the way, would you like to\n"
    .string "join TEAM ROCKET?\p"
    .string "We're a dedicated to\n"
    .string "evil using POKéMON!\p"
    .string "Want to join?\p"
    .string "Are you sure?\p"
    .string "Come on, join us!\p"
    .string "I'm telling you to join!\p"
    .string "OK, you need convincing!\p"
    .string "I'll make you an offer\n"
    .string "you can't refuse!$"

Route24_Text_RocketDefeat::
    .string "ROCKET: Arrgh!\n"
    .string "You are good!$"

Route24_Text_YoudBecomeTopRocketLeader::
    .string "With your ability, you\n"
    .string "could become a top leader\p"
    .string "in TEAM ROCKET!$"

Route24_Text_ShaneIntro::
    .string "I saw your feat from the grass!$"

Route24_Text_ShaneDefeat::
    .string "JR.TRAINER♂: I thought not!$"

Route24_Text_ShanePostBattle::
    .string "I hid because the people on\n"
    .string "the bridge scared me!$"

Route24_Text_EthanIntro::
    .string "OK! I'm No. 5!\n"
    .string "I'll stomp you!$"

Route24_Text_EthanDefeat::
    .string "JR.TRAINER♂: Whoa!\n"
    .string "Too much!$"

Route24_Text_EthanPostBattle::
    .string "I did my best. I have no regrets!$"

Route24_Text_ReliIntro::
    .string "I'm No. 4!\n"
    .string "Getting tired?$"

Route24_Text_ReliDefeat::
    .string "LASS: I lost, too!$"

Route24_Text_ReliPostBattle::
    .string "I did my best. I have no regrets!$"

Route24_Text_TimmyIntro::
    .string "Here's No. 3!\n"
    .string "I won't be easy!$"

Route24_Text_TimmyDefeat::
    .string "YOUNGSTER: Ow!\n"
    .string "Stomped flat!$"

Route24_Text_TimmyPostBattle::
    .string "I did my best. I have no regrets!$"

Route24_Text_AliIntro::
    .string "I'm second!\n"
    .string "Now it's serious!$"

Route24_Text_AliDefeat::
    .string "LASS: How could I lose?$"

Route24_Text_AliPostBattle::
    .string "I did my best. I have no regrets!$"

Route24_Text_CaleIntro::
    .string "This is NUGGET BRIDGE!\n"
    .string "Beat us 5 trainers and\p"
    .string "win a fabulous prize!\l"
    .string "Think you got what it takes?$"

Route24_Text_CaleDefeat::
    .string "BUG CATCHER: Whoo!\n"
    .string "Good stuff!$"

Route24_Text_CalePostBattle::
    .string "I did my best, I have no regrets!$"
