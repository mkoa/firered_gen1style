Route3_Text_TunnelFromCeruleanTiring::
    .string "Whew... I better\n"
    .string "take a rest...\l"
    .string "Groan...\p"
    .string "That tunnel from CERULEAN\n"
    .string "take a lot out of you!$"

Route3_Text_ColtonIntro::
    .string "Hey!\n"
    .string "I met you in VIRIDIAN FOREST!$"

Route3_Text_ColtonDefeat::
    .string "BUG CATCHER: You beat me again!$"

Route3_Text_ColtonPostBattle::
    .string "There are other kinds of POKéMON\n"
    .string "than those found in the forest!$"

Route3_Text_BenIntro::
    .string "Hi! I like shorts!\n"
    .string "They're comfy and easy to wear!$"

Route3_Text_BenDefeat::
    .string "YOUNGSTER: I don't believe it!$"

Route3_Text_BenPostBattle::
    .string "Are you storing your POKéMON on PC?\n"
    .string "Each BOX can hold 20 POKéMON!$"

Route3_Text_JaniceIntro::
    .string "You looked at me, didn't you?$"

Route3_Text_JaniceDefeat::
    .string "LASS: You're mean!$"

Route3_Text_JanicePostBattle::
    .string "Quit staring if you\n"
    .string "don't want to fight!$"

Route3_Text_GregIntro::
    .string "Are you a trainer?\n"
    .string "Let's fight!$"

Route3_Text_GregDefeat::
    .string "BUG CATCHER: If I had new\n"
    .string "POKéMON I would've won!$"

Route3_Text_GregPostBattle::
    .string "If a POKéMON BOX on the PC gets\n"
    .string "full, just switch to another BOX!$"

Route3_Text_SallyIntro::
    .string "That look you gave me,\n"
    .string "it's so intriguing!$"

Route3_Text_SallyDefeat::
    .string "LASS: Be nice!$"

Route3_Text_SallyPostBattle::
    .string "Avoid fights by not letting\n"
    .string "people see you!$"

Route3_Text_CalvinIntro::
    .string "Hey!\n"
    .string "You're not wearing shorts!$"

Route3_Text_CalvinDefeat::
    .string "YOUNGSTER: Lost!\n"
    .string "Lost! Lost!$"

Route3_Text_CalvinPostBattle::
    .string "I always wear shorts,\n"
    .string "even in winter!$"

Route3_Text_JamesIntro::
    .string "You can fight my new POKéMON!$"

Route3_Text_JamesDefeat::
    .string "BUG CATCHER: Done like dinner!$"

Route3_Text_JamesPostBattle::
    .string "Trained POKéMON are stronger than\n"
    .string "the wild ones!$"

Route3_Text_RobinIntro::
    .string "Eek!\n"
    .string "Did you touch me?$"

Route3_Text_RobinDefeat::
    .string "LASS: That's it?$"

Route3_Text_RobinPostBattle::
    .string "ROUTE 4 is at the foot of\n"
    .string "MT.MOON.$"

Route3_Text_RouteSign::
    .string "ROUTE 3\n"
    .string "MT.MOON AHEAD$"

