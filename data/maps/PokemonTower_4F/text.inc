PokemonTower_4F_Text_PaulaIntro::
    .string "Ghost! No!\n"
    .string "Kwaaah!$"

PokemonTower_4F_Text_PaulaDefeat::
    .string "CHANNELER: Where is the ghost?$"

PokemonTower_4F_Text_PaulaPostBattle::
    .string "I must have been dreaming...$"

PokemonTower_4F_Text_LaurelIntro::
    .string "Be cursed with me!\n"
    .string "Kwaaah!$"

PokemonTower_4F_Text_LaurelDefeat::
    .string "CHANNELER: What!$"

PokemonTower_4F_Text_LaurelPostBattle::
    .string "We can't crack the identity\n"
    .string "of the GHOSTs.$"

PokemonTower_4F_Text_JodyIntro::
    .string "Huhuhu...\n"
    .string "Beat me not!$"

PokemonTower_4F_Text_JodyDefeat::
    .string "CHANNELER: Huh?\n"
    .string "Who? What?$"

PokemonTower_4F_Text_JodyPostBattle::
    .string "May the departed sould of \n"
    .string "POKéMON rest in peace...$"

