CinnabarIsland_PokemonCenter_1F_Text_CinnabarGymLocked::
    .string "POKéMON can still learn techniques\n"
    .string "after cancelling evolution.\p"
    .string "Evolution can wait until new\n"
    .string "moves have been learned.$"

CinnabarIsland_PokemonCenter_1F_Text_VisitUnionRoom::
    .string "Do you have any friends?\p"
    .string "POKéMON you get in trades\n"
    .string "grow very quickly.\p"
    .string "I think it's worth a try!$"

CinnabarIsland_PokemonCenter_1F_Text_EvolutionCanWaitForNewMoves::
    .string "You can cancel evolution.\n"
    .string "When a POKéMON is evolving,\p"
    .string "you can stop it and leave\n"
    .string "it the way it is.$"

CinnabarIsland_PokemonCenter_1F_Text_ReadyToSailToOneIsland::
    .string "BILL: Hey, you kept me waiting!\n"
    .string "Ready to set sail to ONE ISLAND?$"

CinnabarIsland_PokemonCenter_1F_Text_OhNotDoneYet::
    .string "Oh, you're still not done yet?$"

CinnabarIsland_PokemonCenter_1F_Text_LetsGo::
    .string "Well, that's it.\n"
    .string "Let's go!$"

