CeruleanCity_BikeShop_Text_WelcomeToBikeShop::
    .string "Hi!\n"
    .string "Welcome to our BIKE SHOP.\p"
    .string "Have we got just the\n"
    .string "BIKE for you!\p"
    .string "It's a cool BIKE!\n"
    .string "Do you want it?$"

CeruleanCity_BikeShop_Text_SorryYouCantAffordIt::
    .string "Sorry!\n"
    .string "You can't afford it!$"

CeruleanCity_BikeShop_Text_OhBikeVoucherHereYouGo::
    .string "Oh, that's...\p"
    .string "A BIKE VOUCHER!\p"
    .string "OK!\n"
    .string "Here you go!$"

CeruleanCity_BikeShop_Text_ExchangedVoucherForBicycle::
    .string "{PLAYER} exchanged the\n"
    .string "BIKE VOUCHER for a BICYCLE.$"

CeruleanCity_BikeShop_Text_ThankYouComeAgain::
    .string "Come back again some time!$"

CeruleanCity_BikeShop_Text_HowDoYouLikeNewBicycle::
    .string "How do you like your new BICYCLE?\n"
    .string "You can take it on CYCLING\n"
    .string "ROAD and in caves!$"

CeruleanCity_BikeShop_Text_MakeRoomForBicycle::
    .string "You better make room for this!$"

CeruleanCity_BikeShop_Text_CityBikeGoodEnoughForMe::
    .string "A plain city BIKE is\n"
    .string "good enough for me!\p"
    .string "You can't put a shopping\n"
    .string "basket on an MTB!$"

CeruleanCity_BikeShop_Text_BikesCoolButExpensive::
    .string "These BIKEs are cool, but\n"
    .string "they're way expensive!$"

CeruleanCity_BikeShop_Text_WowYourBikeIsCool::
    .string "Wow.\n"
    .string "Your BIKE is really cool!$"

@ Unused
CeruleanCity_BikeShop_Text_GermanFoldableBicyleFinallyOnMarket::
    .string "ついに　はつばい！\p"
    .string "ドイツ　せい　さいこうきゅう\n"
    .string "おりたたみ　じてんしゃ！$"

CeruleanCity_BikeShop_Text_ShinyNewBicycle::
    .string "A shiny new BICYCLE!$"

