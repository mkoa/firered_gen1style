CeladonCity_DepartmentStore_3F_Text_OTStandsForOriginalTrainer::
    .string "Captured POKéMON are registered\n"
    .string "with an ID No. and the OT,\p"
    .string "the name of the Original\n"
    .string "Trainer that caught it!$"

CeladonCity_DepartmentStore_3F_Text_BuddyTradingKangaskhanForHaunter::
    .string "All right!\p"
    .string "My buddy's going to trade me his\n"
    .string "KANGASKHAN for my GRAVELER!$"

CeladonCity_DepartmentStore_3F_Text_HaunterEvolvedOnTrade::
    .string "Come on GRAVELER!\p"
    .string "I love GRAVELER!\n"
    .string "I collect them!\p"
    .string "Huh?\p"
    .string "GRAVELER turned into a\n"
    .string "different POKéMON!$"

CeladonCity_DepartmentStore_3F_Text_CanIdentifyTradeMonsByID::
    .string "You can identify POKéMON you got\n"
    .string "in trades by their ID Numbers!$"

CeladonCity_DepartmentStore_3F_Text_ItsSuperNES::
    .string "It's an SNES!$"

CeladonCity_DepartmentStore_3F_Text_AnRPG::
    .string "An RPG!\n"
    .string "There's no time for that!$"

CeladonCity_DepartmentStore_3F_Text_SportsGame::
    .string "A sports game!\n"
    .string "Dad'll like that!$"

CeladonCity_DepartmentStore_3F_Text_PuzzleGame::
    .string "A puzzle game!\n"
    .string "Looks addictive!$"

CeladonCity_DepartmentStore_3F_Text_FightingGame::
    .string "A fighting game!\n"
    .string "Looks tough!$"

CeladonCity_DepartmentStore_3F_Text_TVGameShop::
    .string "3F: TV GAME SHOP$"

CeladonCity_DepartmentStore_3F_Text_RedGreenBothArePokemon::
    .string "Red and Blue!\n"
    .string "Both are POKéMON!$"

Text_CounterTeach::
    .string "Oh, hi!\n"
    .string "I finally finished POKéMON!\p"
    .string "Not done yet?\n"
    .string "This might be useful!$"

Text_CounterDeclined::
    .string "You're not interested? Come see\n"
    .string "me if you change your mind.$"

Text_CounterWhichMon::
    .string "Which POKéMON should I teach\n"
    .string "COUNTER to?$"

Text_CounterTaught::
    .string "Are you using that COUNTER move\n"
    .string "I taught your POKéMON?$"
