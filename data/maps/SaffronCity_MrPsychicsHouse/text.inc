SaffronCity_MrPsychicsHouse_Text_YouWantedThis::
    .string "...Wait! Don't say a word!\p"
    .string "You wanted this!$"

SaffronCity_MrPsychicsHouse_Text_ReceivedTM29FromMrPsychic::
    .string "{PLAYER} received TM29!$"

SaffronCity_MrPsychicsHouse_Text_ExplainTM29::
    .string "TM29 is PSYCHIC!\n"
    .string "It's can lower the target's\n"
    .string "SPECIAL abilities.$"

SaffronCity_MrPsychicsHouse_Text_YouveNoRoom::
    .string "Where do you plan to put this?\n"
    .string "You've no room.$"

