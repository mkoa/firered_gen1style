CeladonCity_Condominiums_RoofRoom_Text_TheresNothingIDontKnow::
    .string "I know everything about the world\n"
    .string "of POKéMON in your GAME BOY!\p"
    .string "Get together with your friends\n"
    .string "and trade POKéMON!$"

CeladonCity_Condominiums_RoofRoom_Text_ObtainedAnEevee::
    .string "{PLAYER} got EEVEE!$"

CeladonCity_Condominiums_RoofRoom_Text_BoxIsFull::
    .string "ポケモンが　いっぱいだ\n"
    .string "ボックスを　かえて　きなさい$"

CeladonCity_Condominiums_RoofRoom_Text_WirelessAdapterLecture::
    .string "TRAINER TIPS\p"
    .string "Using a Game Link Cable$"

CeladonCity_Condominiums_RoofRoom_Text_ReadWhichHeading::
    .string "Which heading do you want to read?$"

CeladonCity_Condominiums_RoofRoom_Text_ExplainWirelessClub::
    .string "When you have linked your GAME\n"
    .string "BOY with another GAME BOY,\p"
    .string "talk to the attendant on the\n"
    .string "right in any POKéMON CENTER.$"

CeladonCity_Condominiums_RoofRoom_Text_ExplainDirectCorner::
    .string "COLOSSEUM lets you play\n"
    .string "against a friend.$"

CeladonCity_Condominiums_RoofRoom_Text_ExplainUnionRoom::
    .string "TRADE CENTER is used for\n"
    .string "trading POKéMON.$"

CeladonCity_Condominiums_RoofRoom_Text_PamphletOnTMs::
    .string "It's a pamphlet on TMs.\p"
    .string "...\p"
    .string "There are 50 TMs in all.\p"
    .string "There are also 5 HMs that\n"
    .string "can be used repeatedly.\p"
    .string "SILPH CO.$"

