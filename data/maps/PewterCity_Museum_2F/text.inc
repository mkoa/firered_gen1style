Text_SeismicTossTeach::
    .string "The secrets of space…\n"
    .string "The mysteries of earth…\p"
    .string "There are so many things about\n"
    .string "which we know so little.\p"
    .string "But that should spur us to study\n"
    .string "harder, not toss in the towel.\p"
    .string "The only thing you should toss…\p"
    .string "Well, how about SEISMIC TOSS?\n"
    .string "Should I teach that to a POKéMON?$"

Text_SeismicTossDeclined::
    .string "Is that so?\n"
    .string "I'm sure you'll be back for it.$"

Text_SeismicTossWhichMon::
    .string "Which POKéMON wants to learn\n"
    .string "SEISMIC TOSS?$"

Text_SeismicTossTaught::
    .string "I hope you won't toss in the towel.\n"
    .string "Keep it up.$"

PewterCity_Museum_1F_Text_WhatsSpecialAboutMoonStone::
    .string "MOON STONE?\p"
    .string "What's so special about it?$"

PewterCity_Museum_1F_Text_BoughtColorTVForMoonLanding::
    .string "July 20, 1969!\p"
    .string "The 1st lunar landing!\n"
    .string "I bought a color TV\l"
    .string "just to watch it!$"

PewterCity_Museum_1F_Text_RunningSpaceExhibitThisMonth::
    .string "We have a space\n"
    .string "exhibit now.$"

PewterCity_Museum_1F_Text_AskedDaddyToCatchPikachu::
    .string "I want a PIKACHU!\n"
    .string "It's so cute!\p"
    .string "I asked my Daddy to catch me one!$"

PewterCity_Museum_1F_Text_PikachuSoonIPromise::
    .string "Yeah, a PIKACHU soon, I promise!$"

PewterCity_Museum_1F_Text_SpaceShuttle::
    .string "SPACE SHUTTLE\n"
    .string "COLOMBIA$"

PewterCity_Museum_1F_Text_MeteoriteThatFellOnMtMoon::
    .string "Meteorite that fell on MT.MOON.\n"
    .string "(MOON STONE?).$"

