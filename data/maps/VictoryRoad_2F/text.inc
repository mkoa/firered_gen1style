VictoryRoad_2F_Text_DawsonIntro::
    .string "If you can get through here,\n"
    .string "you can go meet the ELITE FOUR.$"

VictoryRoad_2F_Text_DawsonDefeat::
    .string "No!\n"
    .string "Unbelievable!$"

VictoryRoad_2F_Text_DawsonPostBattle::
    .string "I can beat you when it comes to\n"
    .string "knowledge about POKéMON!$"

VictoryRoad_2F_Text_DaisukeIntro::
    .string "VICTORY ROAD is the final\n"
    .string "test for trainers!$"

VictoryRoad_2F_Text_DaisukeDefeat::
    .string "BLACKBELT: Aiyah!$"

VictoryRoad_2F_Text_DaisukePostBattle::
    .string "If you get stuck, try moving\n"
    .string "some boulders around!$"

VictoryRoad_2F_Text_NelsonIntro::
    .string "Ah, so you wish to challenge\n"
    .string "the ELITE FOUR?$"

VictoryRoad_2F_Text_NelsonDefeat::
    .string "JUGGLER: You got me!$"

VictoryRoad_2F_Text_NelsonPostBattle::
    .string "{RIVAL} also came through here!$"

VictoryRoad_2F_Text_VincentIntro::
    .string "Come on!\n"
    .string "I'll whip you!$"

VictoryRoad_2F_Text_VincentDefeat::
    .string "TAMER: I got whipped!$"

VictoryRoad_2F_Text_VincentPostBattle::
    .string "You earned the right to be\n"
    .string "on VICTORY ROAD!$"

VictoryRoad_2F_Text_GregoryIntro::
    .string "Is VICTORY ROAD too tough?$"

VictoryRoad_2F_Text_GregoryDefeat::
    .string "JUGGLER: Well done!$"

VictoryRoad_2F_Text_GregoryPostBattle::
    .string "Many trainers give up the\n"
    .string "challenge here.$"

@ Unused, old text for Moltres
VictoryRoad_2F_Text_Gyaoo::
    .string "ギヤーオ！$"

