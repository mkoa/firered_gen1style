Route12_Text_MonSprawledOutInSlumber::
    .string "A sleeping POKéMON\n"
    .string "blocks the way!$"

Text_SnorlaxWokeUp::
    .string "SNORLAX woke up!\p"
    .string "It attacked in a grumpy rage!$"

Text_SnorlaxReturnedToMountains::
    .string "SNORLAX calmed down!\n"
    .string "With a big yawn, it retuned\l"
    .string "to the mountains!$"

Text_WantToUsePokeFlute::
    .string "Want to use the POKé FLUTE?$"

Text_PlayedPokeFlute::
    .string "{PLAYER} played the POKé FLUTE.$"

Route12_Text_NedIntro::
    .string "Yeah!\n"
    .string "I got a bite, here!$"

Route12_Text_NedDefeat::
    .string "FISHERMAN: Tch!\n"
    .string "Just a small fry!$"

Route12_Text_NedPostBattle::
    .string "Hang on!\n"
    .string "My line's snagged!$"

Route12_Text_ChipIntro::
    .string "Be patient!\n"
    .string "Fishing is a waiting game!$"

Route12_Text_ChipDefeat::
    .string "FISHERMAN: That one got away!$"

Route12_Text_ChipPostBattle::
    .string "With a better ROD, I could\n"
    .string "catch better POKéMON!$"

Route12_Text_JustinIntro::
    .string "Have you found a MOON STONE?$"

Route12_Text_JustinDefeat::
    .string "JR.TRAINER♂: Oww!$"

Route12_Text_JustinPostBattle::
    .string "I could have made my POKéMON\n"
    .string "evolve with MOON STONE!$"

Route12_Text_LucaIntro::
    .string "Electricity is my specialty!$"

Route12_Text_LucaDefeat::
    .string "ROCKER: Unplugged!$"

Route12_Text_LucaPostBattle::
    .string "Water conducts electricity, so\n"
    .string "you should zap sea POKéMON!$"

Route12_Text_HankIntro::
    .string "The FISHING FOOL vs.\n"
    .string "POKéMON KID!$"

Route12_Text_HankDefeat::
    .string "FISHERMAN: Too much!$"

Route12_Text_HankPostBattle::
    .string "You beat me at POKéMON,\n"
    .string "but I'm good at fishing!$"

Route12_Text_ElliotIntro::
    .string "I'd rather be working!$"

Route12_Text_ElliotDefeat::
    .string "FISHERMAN: It's not easy...$"

Route12_Text_ElliotPostBattle::
    .string "It's all right.\n"
    .string "Losing doesn't bug me any more.$"

Route12_Text_AndrewIntro::
    .string "You never know what you\n"
    .string "could catch!$"

Route12_Text_AndrewDefeat::
    .string "FISHERMAN: Lost it!$"

Route12_Text_AndrewPostBattle::
    .string "I catch MAGIKARP all the time,\n"
    .string "but they're so weak!$"

Route12_Text_RouteSign::
    .string "ROUTE 12 \n"
    .string "North to LAVENDER$"

Route12_Text_SportfishingArea::
    .string "SPORT FISHING AREA$"

Route12_Text_JesIntro::
    .string "JES: If I win, I'm going to\n"
    .string "propose to GIA.$"

Route12_Text_JesDefeat::
    .string "JES: Oh, please, why couldn't you\n"
    .string "let us win?$"

Route12_Text_JesPostBattle::
    .string "JES: Oh, GIA, forgive me,\n"
    .string "my love!$"

Route12_Text_JesNotEnoughMons::
    .string "JES: GIA and I, we'll be\n"
    .string "together forever.\p"
    .string "We won't battle unless you have\n"
    .string "two POKéMON of your own.$"

Route12_Text_GiaIntro::
    .string "GIA: Hey, JES…\p"
    .string "If we win, I'll marry you!$"

Route12_Text_GiaDefeat::
    .string "GIA: Oh, but why?$"

Route12_Text_GiaPostBattle::
    .string "GIA: JES, you silly!\n"
    .string "You ruined this!$"

Route12_Text_GiaNotEnoughMons::
    .string "GIA: I can't bear to battle\n"
    .string "without my JES!\p"
    .string "Don't you have one more POKéMON?$"
