Route2_EastBuilding_Text_GiveHM05IfSeen10Mons::
    .string "Hi! Remember me?\n"
    .string "I'm PROF.OAK's AIDE!\p"
    .string "If you caught 10 kinds of\n"
    .string "POKéMON, I'm supposed to\p"
    .string "give you an HM05!\l"
    .string "So, {PLAYER}!\n"
    .string "Have you caught at least\p"
    .string "10 kinds of POKéMON?$"

Route2_EastBuilding_Text_GreatHereYouGo::
    .string "Great! You have caught\n"
    .string "{STR_VAR_3} kinds of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route2_EastBuilding_Text_ReceivedHM05FromAide::
    .string "{PLAYER} got the HM05!$"

Route2_EastBuilding_Text_ExplainHM05::
    .string "The HM FLASH lights even\n"
    .string "the darkest dungeons.$"

Route2_EastBuilding_Text_CanGetThroughRockTunnel::
    .string "Once a POKéMON learns FLASH, you\n"
    .string "can get through ROCK TUNNEL.$"

