CeruleanCity_Gym_Text_MistyIntro::
    .string "Hi, you're a new face!\p"
    .string "Trainers who want to turn\n"
    .string "pro have to have a policy\p"
    .string "about POKéMON!\p"
    .string "What is your approach when\n"
    .string "you catch POKéMON?\p"
    .string "My policy is an all-out offensive\n"
    .string "with WATER-type POKéMON!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

CeruleanCity_Gym_Text_ExplainTM11::
    .string "TM11 teaches BUBBLEBEAM!\p"
    .string "Use it on an aquatic POKéMON!$"

CeruleanCity_Gym_Text_ExplainCascadeBadge::
    .string "The CASCADEBADGE makes all\n"
    .string "POKéMON up to L30 obey!\p"
    .string "That includes even outsiders!\n"
    .string "There's more, you can now\p"
    .string "use CUT any time!\l"
    .string "You can CUT down small bushes\n"
    .string "to open new paths!\p"
    .string "You can also have my favorite TM!$"

CeruleanCity_Gym_Text_ReceivedTM11FromMisty::
    .string "{PLAYER} received TM11!$"

CeruleanCity_Gym_Text_BetterMakeRoomForThis::
    .string "You better make room for this!$"

CeruleanCity_Gym_Text_MistyDefeat::
    .string "MISTY: Wow!\n"
    .string "You're too much!\p"
    .string "All right!\p"
    .string "You can have the CASCADEBADGE\n"
    .string "to show you beat me!$"

CeruleanCity_Gym_Text_DianaIntro::
    .string "I'm more than good enough for you!\n"
    .string "MISTY can wait!$"

CeruleanCity_Gym_Text_DianaDefeat::
    .string "JR.TRAINER♀: You overwhelmed me!$"

CeruleanCity_Gym_Text_DianaPostBattle::
    .string "You have to face other trainers to\n"
    .string "find out how good you really are.$"

CeruleanCity_Gym_Text_LuisIntro::
    .string "Splash!\p"
    .string "I'm first up!\n"
    .string "Let's do it!$"

CeruleanCity_Gym_Text_LuisDefeat::
    .string "That can't be!$"

CeruleanCity_Gym_Text_LuisPostBattle::
    .string "MISTY is a TRAINER who's going to\n"
    .string "keep improving.\p"
    .string "She won't lose to someone like you!$"

CeruleanCity_Gym_Text_GymGuyAdvice::
    .string "Yo!\n"
    .string "Champ in the making!\p"
    .string "Here's my advice!\p"
    .string "The LEADER, MISTY, is a pro\n"
    .string "who uses water POKéMON!\p"
    .string "You can drain all their water\n"
    .string "with plant POKéMON!\p"
    .string "Or, zap them with electricity!$"

CeruleanCity_Gym_Text_WeMakePrettyGoodTeam::
    .string "You beat MISTY!\n"
    .string "See, what'd I tell ya?\p"
    .string "You and me, kid, we make a pretty\n"
    .string "darn-good team!$"

CeruleanCity_Gym_Text_GymStatue::
    .string "CERULEAN CITY POKéMON GYM\n"
    .string "LEADER: MISTY\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

CeruleanCity_Gym_Text_GymStatuePlayerWon::
    .string "CERULEAN CITY POKéMON GYM\n"
    .string "LEADER: MISTY\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

