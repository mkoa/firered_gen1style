ViridianCity_School_Text_TryingToMemorizeNotes::
    .string "Whew! I'm trying to memorize all my\n"
    .string "notes.$"

ViridianCity_School_Text_ReadBlackboardCarefully::
    .string "Okay!\p"
    .string "Be sure to read the\n"
    .string "blackboard carefully!$"

ViridianCity_School_Text_NotebookFirstPage::
    .string "Looked at the notebook!\p"
    .string "First page...\p"
    .string "POKé BALLs are used to catch\n"
    .string "POKéMON.\p"
    .string "Up to 6 POKéMON can be carried.\n"
    .string "People who raise and\n"
    .string "make POKéMON fight are\p"
    .string "called POKéMON TRAINERS.$"

ViridianCity_School_Text_NotebookSecondPage::
    .string "Second page...\p"
    .string "A healthy POKéMON may be hard to\n"
    .string "catch, so weaken it first!\p"
    .string "Poison, burns and other\n"
    .string "damage are effective!$"

ViridianCity_School_Text_NotebookThirdPage::
    .string "Third page...\p"
    .string "POKéMON trainers seek others to\n"
    .string "engage in POKéMON fights.\p"
    .string "Battles are constantly fought\n"
    .string "at POKéMON GYMs.$"

ViridianCity_School_Text_NotebookFourthPage::
    .string "Fourth page...\p"
    .string "The goal for POKéMON trainers\n"
    .string "is to beat the top 8\n"
    .string "POKéMON GYM LEADERs.\p"
    .string "Do so to earn the right to face...\p"
    .string "The ELITE FOUR of POKéMON LEAGUE!$"

ViridianCity_School_Text_TurnThePage::
    .string "Turn the page?$"

ViridianCity_School_Text_HeyDontLookAtMyNotes::
    .string "GIRL: Hey!\n"
    .string "Don't look at my notes!$"

ViridianCity_School_Text_BlackboardListsStatusProblems::
    .string "The blackboard describes POKéMON\n"
    .string "STATUS changes during battles.$"

ViridianCity_School_Text_ReadWhichTopic::
    .string "Which heading do you want to read?$"

ViridianCity_School_Text_ExplainSleep::
    .string "A POKéMON can't attack if it's\n"
    .string "asleep!\p"
    .string "POKéMON will stay asleep even\n"
    .string "after battles.\p"
    .string "Use AWAKENING to wake them up!$"

ViridianCity_School_Text_ExplainBurn::
    .string "A burn reduces power and speed.\n"
    .string "It also causes ongoing damage.\p"
    .string "Burns remain after battles.\n"
    .string "Use BURN HEAL to cure a burn!$"

ViridianCity_School_Text_ExplainPoison::
    .string "When poisoned, a POKéMON's health\n"
    .string "steadily drops.\p"
    .string "Poison lingers after battles.\n"
    .string "Use an ANTIDOTE to cure poison!$"

ViridianCity_School_Text_ExplainFreeze::
    .string "If frozen, a POKéMON becomes\n"
    .string "totally immobile!\p"
    .string "It stays frozen even after the\n"
    .string "battle ends.\p"
    .string "Use ICE HEAL to\n"
    .string "thaw out POKéMON!$"

ViridianCity_School_Text_ExplainParalysis::
    .string "Paralysis could make POKéMON\n"
    .string "moves misfire!\p"
    .string "Paralysis remains after battles.\n"
    .string "Use PARLYZ HEAL for treatment!$"

