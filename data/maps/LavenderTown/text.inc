LavenderTown_Text_DoYouBelieveInGhosts::
    .string "Do you believe in GHOSTs?$"

LavenderTown_Text_SoThereAreBelievers::
    .string "Really?\n"
    .string "So there are believers...$"

LavenderTown_Text_JustImaginingWhiteHand::
    .string "Hahaha, I guess not.\p"
    .string "That white hand on your shoulder,\n"
    .string "it's not real.$"

LavenderTown_Text_TownKnownAsMonGraveSite::
    .string "This town is known as the grave\n"
    .string "site of POKéMON.\p"
    .string "Memorial services are held in\n"
    .string "POKéMON TOWER.$"

LavenderTown_Text_GhostsAppearedInTower::
    .string "GHOSTs appeared in POKéMON TOWER.\p"
    .string "I think they're the spirits of\n"
    .string "POKéMON that the ROCKETs killed.$"

LavenderTown_Text_TownSign::
    .string "LAVENDER TOWN\n"
    .string "The Noble Purple Town$"

LavenderTown_Text_SilphScopeNotice::
    .string "New SILPH SCOPE!\n"
    .string "Make the Invisible Plain to See!\p"
    .string "SILPH CO.$"

LavenderTown_Text_VolunteerPokemonHouse::
    .string "LAVENDER VOLUNTEER\n"
    .string "POKéMON HOUSE$"

LavenderTown_Text_PokemonTowerSign::
    .string "May the Souls of POKéMON\n"
    .string "Rest Easy\p"
    .string "POKéMON TOWER$"

