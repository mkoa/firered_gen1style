FuchsiaCity_WardensHouse_Text_HifFuffHefifoo::
    .string "WARDEN: Hif fuff hefifoo!\p"
    .string "Ha lof ha feef ee hafahi ho.\n"
    .string "Heff hee fwee!$"

FuchsiaCity_WardensHouse_Text_AhHowheeHoHoo::
    .string "Ah howhee ho hoo!\n"
    .string "Eef ee hafahi ho!$"

FuchsiaCity_WardensHouse_Text_HeOhayHeHaHoo::
    .string "Ha?\n"
    .string "He ohay heh ha hoo ee haheh!$"

FuchsiaCity_WardensHouse_Text_GaveGoldTeethToWarden::
    .string "{PLAYER} gave the GOLD TEETH\n"
    .string "to the WARDEN!$"

FuchsiaCity_WardensHouse_Text_WardenPoppedInHisTeeth::
    .string "The WARDEN popped in his teeth!$"

FuchsiaCity_WardensHouse_Text_ThanksSonGiveYouSomething::
    .string "WARDEN: Thanks, kid!\n"
    .string "No one could understand a word\p"
    .string "that I said.\l"
    .string "I couldn't work that way.\n"
    .string "Let me give you something for\p"
    .string "your trouble.$"

FuchsiaCity_WardensHouse_Text_ThanksLassieGiveYouSomething::
    .string "WARDEN: Thanks, kid!\n"
    .string "No one could understand a word\p"
    .string "that I said.\l"
    .string "I couldn't work that way.\n"
    .string "Let me give you something for\p"
    .string "your trouble.$"

FuchsiaCity_WardensHouse_Text_ReceivedHM04FromWarden::
    .string "{PLAYER} received HM04!$"

FuchsiaCity_WardensHouse_Text_ExplainStrength::
    .string "WARDEN: HM04 teaches STRENGTH!\n"
    .string "It lets POKéMON move boulders when\p"
    .string "you're outside of battle.\l"
    .string "Oh yes, did you find the SECRET\n"
    .string "HOUSE in the SAFARI ZONE?\p"
    .string "If you do, you win an HM!\n"
    .string "I hear it's the rare SURF HM.$"

@ Unused
FuchsiaCity_WardensHouse_Text_YouHaveTooMuchStuff::
    .string "なんや　にもつ\n"
    .string "いっぱいやんけ！$"

FuchsiaCity_WardensHouse_Text_MonPhotosFossilsOnDisplay::
    .string "POKéMON photos and fossils.$"

FuchsiaCity_WardensHouse_Text_OldMonMerchandiseOnDisplay::
    .string "Old POKéMON merchandise.$"

