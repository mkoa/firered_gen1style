Route18_Text_WiltonIntro::
    .string "I always check every grassy area\n"
    .string "for new POKéMON.$"

Route18_Text_WiltonDefeat::
    .string "BIRD KEEPER: Tch!$"

Route18_Text_WiltonPostBattle::
    .string "I wish I had a BIKE!$"

Route18_Text_RamiroIntro::
    .string "Kurukkoo!\n"
    .string "How do you like my bird call?$"

Route18_Text_RamiroDefeat::
    .string "BIRD KEEPER: I had to bug you!$"

Route18_Text_RamiroPostBattle::
    .string "I also collect sea POKéMON\n"
    .string "on weekends!$"

Route18_Text_JacobIntro::
    .string "This is my turf!\n"
    .string "Get out of here!$"

Route18_Text_JacobDefeat::
    .string "BIRD KEEPER: Darn!$"

Route18_Text_JacobPostBattle::
    .string "This is my fave POKéMON\n"
    .string "hunting area!$"

Route18_Text_RouteSign::
    .string "ROUTE 18\n"
    .string "CELADON CITY - FUCHSIA CITY$"

Route18_Text_CyclingRoadSign::
    .string "CYCLING ROAD\n"
    .string "No pedestrians permitted!$"

