CeruleanCity_Mart_Text_RepelWorksOnWeakMons::
    .string "Use REPEL to keep bugs\n"
    .string "and weak POKéMON away.\p"
    .string "Put your strongest POKéMON\n"
    .string "at the top of the list\l"
    .string "for best results!$"

CeruleanCity_Mart_Text_DoYouKnowAboutRareCandy::
    .string "Have you seen any RARE CANDY?\n"
    .string "It's supposed to make\l"
    .string "POKéMON go up one level!$"

