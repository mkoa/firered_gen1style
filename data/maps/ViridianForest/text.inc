ViridianForest_Text_FriendsItchingToBattle::
    .string "I came here with some friends!\n"
    .string "They're out for POKéMON fights!$"

ViridianForest_Text_RickIntro::
    .string "Hey! You have POKéMON!\n"
    .string "Come on!\l"
    .string "Let's battle' em!$"

ViridianForest_Text_RickDefeat::
    .string "No!\nCATERPIE can't cut it!$"

ViridianForest_Text_RickPostBattle::
    .string "Ssh! You'll scare the bugs away.\n"
    .string "Another time, okay?$"

ViridianForest_Text_DougIntro::
    .string "Yo! You can't jam out if\n"
    .string "you're a POKéMON trainer!$"

ViridianForest_Text_DougDefeat::
    .string "Huh?\n"
    .string "I ran out of POKéMON!$"

ViridianForest_Text_DougPostBattle::
    .string "That totally stinks! I'm going to\n"
    .string "catch some stronger ones!$"

ViridianForest_Text_SammyIntro::
    .string "Hey, wait up!\n"
    .string "What's the hurry?$"

ViridianForest_Text_SammyDefeat::
    .string "I give!\n"
    .string "You're good at this!$"

ViridianForest_Text_SammyPostBattle::
    .string "Sometimes, you can find stuff on\n"
    .string "the ground.\p"
    .string "I'm looking for the stuff I\n"
    .string "dropped. Can you help me?$"

ViridianForest_Text_AnthonyIntro::
    .string "I might be little, but I won't like\n"
    .string "it if you go easy on me!$"

ViridianForest_Text_AnthonyDefeat::
    .string "Oh, boo.\n"
    .string "Nothing went right.$"

ViridianForest_Text_AnthonyPostBattle::
    .string "I lost some of my allowance…$"

ViridianForest_Text_CharlieIntro::
    .string "Did you know that POKéMON evolve?$"

ViridianForest_Text_CharlieDefeat::
    .string "Oh!\n"
    .string "I lost!$"

ViridianForest_Text_CharliePostBattle::
    .string "BUG POKéMON evolve quickly.\n"
    .string "They're a lot of fun!$"

ViridianForest_Text_RanOutOfPokeBalls::
    .string "I ran out of POKé BALLs to\n"
    .string "catch POKéMON with!\p"
    .string "You should carry extras!$"

ViridianForest_Text_AvoidGrassyAreasWhenWeak::
    .string "TRAINER TIPS\p"
    .string "If you want to avoid battles,\n"
    .string "stay away from grassy areas!$"

ViridianForest_Text_UseAntidoteForPoison::
    .string "For poison, use ANTIDOTE!\n"
    .string "Get it at POKéMON MARTs!$"

ViridianForest_Text_ContactOakViaPCToRatePokedex::
    .string "TRAINER TIPS\p"
    .string "Contact PROF.OAK via a PC to\n"
    .string "get your POKéDEX evaluated!$"

ViridianForest_Text_CantCatchOwnedMons::
    .string "TRAINER TIPS\p"
    .string "No stealing of POKéMON\n"
    .string "from other trainers!\p"
    .string "Catch only wild POKéMON!$"

ViridianForest_Text_WeakenMonsBeforeCapture::
    .string "TRAINER TIPS\p"
    .string "Weaken POKéMON before attempting\n"
    .string "capture!\p"
    .string "When healthy, they may escape!$"

ViridianForest_Text_LeavingViridianForest::
    .string "LEAVING VIRIDIAN FOREST\n"
    .string "PEWTER CITY AHEAD$"

