PokemonTower_1F_Text_ErectedInMemoryOfDeadMons::
    .string "POKéMON TOWER was erected in the\n"
    .string "memory of POKéMON that had died.$"

PokemonTower_1F_Text_ComeToPayRespectsSon::
    .string "Did you come to pay respects?\p"
    .string "Bless you!$"

PokemonTower_1F_Text_ComeToPayRespectsGirl::
    .string "Did you come to pay respects?\p"
    .string "Bless your POKéMON-loving heart,\n"
    .string "girl.$"

PokemonTower_1F_Text_CameToPrayForDepartedClefairy::
    .string "I came to pray for\n"
    .string "my CLEFAIRY.\p"
    .string "Sniff!\n"
    .string "I can't stop crying...$"

PokemonTower_1F_Text_GrowlitheWhyDidYouDie::
    .string "My GROWLITHE...\n"
    .string "Why did you die?$"

PokemonTower_1F_Text_SenseSpiritsUpToMischief::
    .string "I am a CHANNELER!\p"
    .string "There are spirits up to mischief!$"

