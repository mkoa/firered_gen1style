Route12_FishingHouse_Text_DoYouLikeToFish::
    .string "I'm the FISHING GURU's brother!\n"
    .string "I simply Looove fishing!\p"
    .string "Do you like to fish?$"

Route12_FishingHouse_Text_TakeThisAndFish::
    .string "Grand! I like your style!\n"
    .string "Take this and fish, young one!$"

Route12_FishingHouse_Text_ReceivedSuperRod::
    .string "{PLAYER} received a SUPER ROD!$"

Route12_FishingHouse_Text_IfYouCatchBigMagikarpShowMe::
    .string "Fishing is a way of life!\n"
    .string "From the seas to rivers, go out\p"
    .string "and land the big one!$"

Route12_FishingHouse_Text_OhThatsDisappointing::
    .string "Oh...\n"
    .string "That's so disappointing...$"

Route12_FishingHouse_Text_TryFishingBringMeMagikarp::
    .string "Hello there, {PLAYER}!\n"
    .string "Use the SUPER ROD in any water!\n"
    .string "You can find different kinds\n"
    .string "of POKéMON.\p"
    .string "Try fishing wherever you can!$"

Route12_FishingHouse_Text_OhMagikarpAllowMeToSee::
    .string "Oh? {PLAYER}?\n"
    .string "Why, if it isn't a MAGIKARP!\p"
    .string "Allow me to see it, quick!$"

Route12_FishingHouse_Text_WhoaXInchesTakeThis::
    .string "… … …Whoa!\n"
    .string "{STR_VAR_2} inches!\p"
    .string "You have a rare appreciation for\n"
    .string "the fine, poetic aspects of fishing!\p"
    .string "You must take this.\n"
    .string "I insist!$"

Route12_FishingHouse_Text_LookForwardToGreaterRecords::
    .string "I'll look forward to seeing greater\n"
    .string "records from you!$"

Route12_FishingHouse_Text_HuhXInchesSameSizeAsLast::
    .string "Huh?\n"
    .string "{STR_VAR_2} inches?\p"
    .string "This is the same size as the one\n"
    .string "I saw before.$"

Route12_FishingHouse_Text_HmmXInchesDoesntMeasureUp::
    .string "Hmm…\n"
    .string "This one is {STR_VAR_2} inches long.\p"
    .string "It doesn't measure up to the\n"
    .string "{STR_VAR_3}-inch one you brought before.$"

Route12_FishingHouse_Text_DoesntLookLikeMagikarp::
    .string "Uh… That doesn't look much like\n"
    .string "a MAGIKARP.$"

Route12_FishingHouse_Text_NoRoomForGift::
    .string "Oh, no!\p"
    .string "I had a gift for you, but you have\n"
    .string "no room for it.$"

Route12_FishingHouse_Text_MostGiganticMagikarpXInches::
    .string "The most gigantic MAGIKARP\n"
    .string "I have ever witnessed…\p"
    .string "{STR_VAR_3} inches!$"

Route12_FishingHouse_Text_BlankChartOfSomeSort::
    .string "It's a blank chart of some sort.\p"
    .string "It has spaces for writing in\n"
    .string "records of some kind.$"

