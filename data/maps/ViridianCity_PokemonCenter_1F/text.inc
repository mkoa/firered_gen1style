ViridianCity_PokemonCenter_1F_Text_FeelFreeToUsePC::
    .string "You can use that\n"
    .string "PC in the corner.\p"
    .string "The receptionist told me.\n"
    .string "So kind!$"

ViridianCity_PokemonCenter_1F_Text_PokeCenterInEveryTown::
    .string "There's a POKéMON CENTER in every\n"
    .string "town ahead.\p"
    .string "They don't charge\n"
    .string "any money either!$"

ViridianCity_PokemonCenter_1F_Text_PokeCentersHealMons::
    .string "POKéMON CENTERs heal your tired,\n"
    .string "hurt or fainted POKéMON!$"

