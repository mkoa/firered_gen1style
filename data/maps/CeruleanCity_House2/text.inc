CeruleanCity_House2_Text_RocketsStoleTMForDig::
    .string "Those miserable ROCKETs!\n"
    .string "Look what did here!\p"
    .string "They stole a TM for teaching\n"
    .string "POKéMON how to DIG holes!\p"
    .string "That cost me a bundle, it did!$"

CeruleanCity_House2_Text_TeachDiglettDigWithoutTM::
    .string "I figure what's lost is lost.\n"
    .string "I decided to teach DIGLETT how to\p"
    .string "DIG without a TM.$"

CeruleanCity_House2_Text_TeamRocketTryingToDigIntoNoGood::
    .string "TEAM ROCKET must be trying to\n"
    .string "DIG their way into no good!$"

CeruleanCity_House2_Text_TeamRocketLeftWayOut::
    .string "TEAM ROCKET left a way out!$"

