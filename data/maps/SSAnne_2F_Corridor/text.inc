SSAnne_2F_Corridor_Text_ThisShipIsLuxuryLiner::
    .string "This ship, she is a luxury\n"
    .string "liner for trainerss!\p"
    .string "At every port, we hold parties\n"
    .string "with invited trainers!$"

SSAnne_2F_Corridor_Text_RivalIntro::
    .string "{RIVAL}: Bonjour!\n"
    .string "{PLAYER}!\p"
    .string "Imagine seeing you here!\n"
    .string "{PLAYER}, were you really invited?\p"
    .string "So how's your POKéDEX coming?\p"
    .string "I already caught 40 kinds, pal!\n"
    .string "Different kinds are everywhere!\p"
    .string "Crawl around in grassy areas!$"

SSAnne_2F_Corridor_Text_RivalDefeat::
    .string "{RIVAL}: Humph!\p"
    .string "At least you're raising\n"
    .string "your POKéMON!$"

SSAnne_2F_Corridor_Text_RivalVictory::
    .string "{PLAYER}‥！\n"
    .string "ふなよい　してるのか！\p"
    .string "もっと　からだ\n"
    .string "きたえた　ほうが　いいぜ！$"

SSAnne_2F_Corridor_Text_RivalPostBattle::
    .string "{RIVAL}: I heard there was a CUT\n"
    .string "master on board.\p"
    .string "But, he was just a seasick,\n"
    .string "old man!\p"
    .string "But, CUT itself is really useful!\n"
    .string "You should go see him!\p"
    .string "Smell ya!$"

