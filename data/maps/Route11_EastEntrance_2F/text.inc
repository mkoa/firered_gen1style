Route11_EastEntrance_2F_Text_GiveItemfinderIfCaught30::
    .string "Hi! Remember me?\n"
    .string "I'm PROF.OAK's AIDE!\p"
    .string "If you caught {STR_VAR_1} kinds\n"
    .string "of POKéMON, I'm supposed to\p"
    .string "give you an ITEMFINDER!\l"
    .string "So, {PLAYER}!\n"
    .string "Have you caught at least\p"
    .string "{STR_VAR_1} kinds of POKéMON?$"

Route11_EastEntrance_2F_Text_GreatHereYouGo::
    .string "Great! You have caught\n"
    .string "{STR_VAR_3} kinds of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route11_EastEntrance_2F_Text_ReceivedItemfinderFromAide::
    .string "{PLAYER} got the {STR_VAR_2}!$"

Route11_EastEntrance_2F_Text_ExplainItemfinder::
    .string "There are items on the ground\n"
    .string "that can't be seen.\p"
    .string "ITEMFINDER will detect an\n"
    .string "item close to you.\p"
    .string "It can't pinpoint it, so\n"
    .string "you have to look yourself!$"

Route11_EastEntrance_2F_Text_BigMonAsleepOnRoad::
    .string "Looked into the binoculars.\n"
    .string "A big POKéMON is asleep on a road!$"

Route11_EastEntrance_2F_Text_WhatABreathtakingView::
    .string "Looked into the binoculars.\n"
    .string "What a breathtaking view!$"

Route11_EastEntrance_2F_Text_RockTunnelGoodRouteToLavender::
    .string "Looked into the binoculars.\n"
    .string "The only way to get from\n"
    .string "CERULEAN CITY to LAVENDER is\p"
    .string "by way of the ROCK TUNNEL.$"

