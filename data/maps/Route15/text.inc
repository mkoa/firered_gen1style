Route15_Text_KindraIntro::
    .string "Let me try out the POKéMON\n"
    .string "I just got in a trade!$"

Route15_Text_KindraDefeat::
    .string "JR.TRAINER♀: Not good enough!$"

Route15_Text_KindraPostBattle::
    .string "You can't change the nickname of\n"
    .string "any POKéMON you get in a trade.\p"
    .string "Only the Original Trainer can.$"

Route15_Text_BeckyIntro::
    .string "You look gentle, so I think I\n"
    .string "can beat you!$"

Route15_Text_BeckyDefeat::
    .string "JR.TRAINER♀: No, wrong!$"

Route15_Text_BeckyPostBattle::
    .string "I'm afraid of BIKERs, they look\n"
    .string "so ugly and mean!$"

Route15_Text_EdwinIntro::
    .string "When I whistle, I can summon\n"
    .string "bird POKéMON.$"

Route15_Text_EdwinDefeat::
    .string "BIRD KEEPER: Ow!\n"
    .string "That's tragic!$"

Route15_Text_EdwinPostBattle::
    .string "Maybe I'm not cut out for battles.$"

Route15_Text_ChesterIntro::
    .string "Hmm? My birds are shivering!\n"
    .string "You're good, aren't you?$"

Route15_Text_ChesterDefeat::
    .string "BIRD KEEPER: Just as I thought!$"

Route15_Text_ChesterPostBattle::
    .string "Did you know that moves like\n"
    .string "EARTHQUAKE don't have any\l"
    .string "effect on birds?$"

Route15_Text_GraceIntro::
    .string "Oh, you're a little cutie!$"

Route15_Text_GraceDefeat::
    .string "BEAUTY: You looked so cute too!$"

Route15_Text_GracePostBattle::
    .string "I forgive you!\n"
    .string "I can take it!$"

Route15_Text_OliviaIntro::
    .string "I raise POKéMON because I\n"
    .string "live alone!$"

Route15_Text_OliviaDefeat::
    .string "BEAUTY: I didn't ask for this!$"

Route15_Text_OliviaPostBattle::
    .string "I just like going home to be\n"
    .string "with my POKéMON!$"

Route15_Text_ErnestIntro::
    .string "Hey, kid! C'mon!\n"
    .string "I just got these!$"

Route15_Text_ErnestDefeat::
    .string "BIKER: Why not?$"

Route15_Text_ErnestPostBattle::
    .string "You only live once, so I live\n"
    .string "as an outlaw!\l"
    .string "TEAM ROCKET RULES!$"

Route15_Text_AlexIntro::
    .string "Fork over all your cash when you\n"
    .string "lose to me, kid!$"

Route15_Text_AlexDefeat::
    .string "biker: That can't be true!$"

Route15_Text_AlexPostBattle::
    .string "I was just joking about the money!$"

Route15_Text_CeliaIntro::
    .string "What's cool?\n"
    .string "Trading POKéMON!$"

Route15_Text_CeliaDefeat::
    .string "JR.TRAINER♀: I said trade!$"

Route15_Text_CeliaPostBattle::
    .string "I trade POKéMON with my friends!$"

Route15_Text_YazminIntro::
    .string "Want to play with my POKéMON?$"

Route15_Text_YazminDefeat::
    .string "JR.TRAINER♀: I was too impatient!$"

Route15_Text_YazminPostBattle::
    .string "I'll go train with weaker people.$"

Route15_Text_RouteSign::
    .string "ROUTE 15\n"
    .string "West to FUCHSIA CITY$"

Route15_Text_MyaIntro::
    .string "MYA: You're perfect.\n"
    .string "Help me train my little brother?$"

Route15_Text_MyaDefeat::
    .string "MYA: RON, you have to focus!\n"
    .string "Concentrate on what you're doing!$"

Route15_Text_MyaPostBattle::
    .string "MYA: Okay, we'll turn it up.\n"
    .string "I'll add to our training menu!$"

Route15_Text_MyaNotEnoughMons::
    .string "MYA: Do you want to challenge us?\n"
    .string "You'll need two POKéMON, though.$"

Route15_Text_RonIntro::
    .string "RON: My sister gets scary when we\n"
    .string "lose.$"

Route15_Text_RonDefeat::
    .string "RON: Oh, no, no…\n"
    .string "Sis, I'm sorry!$"

Route15_Text_RonPostBattle::
    .string "RON: Oh, bleah…\n"
    .string "I wish I had a nice sister…$"

Route15_Text_RonNotEnoughMons::
    .string "RON: Did you want to battle with\n"
    .string "my sister and me?\p"
    .string "You need two POKéMON, then.$"
