Route16_NorthEntrance_2F_Text_OnBikeRideWithGirlfriend::
    .string "I'm going for a ride with my\n"
    .string "girl friend!$"

Route16_NorthEntrance_2F_Text_RidingTogetherOnNewBikes::
    .string "We're going riding together!$"

Route16_NorthEntrance_2F_Text_ItsCeladonDeptStore::
    .string "Looked into the binoculars.\n"
    .string "It's the CELADON DEPT. STORE!$"

Route16_NorthEntrance_2F_Text_LongPathOverWater::
    .string "Looked into the binoculars.\n"
    .string "There's a long path over water!$"

Route16_NorthEntrance_2F_Text_GiveAmuletCoinIfCaught40::
    .string "Hi! Remember me?\n"
    .string "I'm PROF.OAK's AIDE!\p"
    .string "If your POKéDEX has complete data\n"
    .string "on 40 species, I'm supposed to\p"
    .string "give you a reward.\l"
    .string "So, {PLAYER}, let me ask you.\n"
    .string "Have you gathered data on at least\p"
    .string "40 kinds of POKéMON?$"

Route16_NorthEntrance_2F_Text_GreatHereYouGo::
    .string "Great! You have caught\n"
    .string "{STR_VAR_3} kinds of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route16_NorthEntrance_2F_Text_ReceivedAmuletCoinFromAide::
    .string "{PLAYER} got the AMULET COIN!$"

Route16_NorthEntrance_2F_Text_ExplainAmuletCoin::
    .string "An AMULET COIN is an item to be\n"
    .string "held by a POKéMON.\p"
    .string "If the POKéMON appears in a winning\n"
    .string "battle, you will earn more money.$"

