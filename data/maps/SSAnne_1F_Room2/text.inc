SSAnne_1F_Room2_Text_TylerIntro::
    .string "I love POKéMON!\n"
    .string "Do you?$"

SSAnne_1F_Room2_Text_TylerDefeat::
    .string "YOUNGSTER: Wow!\n"
    .string "You're great!$"

SSAnne_1F_Room2_Text_TylerPostBattle::
    .string "Let me be your friend, OK?\n"
    .string "Then we can trade POKéMON!$"

SSAnne_1F_Room2_Text_AnnIntro::
    .string "I collected these POKéMON\n"
    .string "from all around the world!$"

SSAnne_1F_Room2_Text_AnnDefeat::
    .string "LASS: Oh no!\n"
    .string "I went around the world for these!$"

SSAnne_1F_Room2_Text_AnnPostBattle::
    .string "You hurt my poor worldly POKéMON!\p"
    .string "I demand that you heal them\n"
    .string "at a POKéMON CENTER!$"

SSAnne_1F_Room2_Text_CruisingAroundWorld::
    .string "We are cruising around the world.$"

