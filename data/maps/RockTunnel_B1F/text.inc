RockTunnel_B1F_Text_SofiaIntro::
    .string "Hikers leave twigs\n"
    .string "as trail markers.$"

RockTunnel_B1F_Text_SofiaDefeat::
    .string "JR.TRAINER♂: Ohhh!\n"
    .string "I did my best!$"

RockTunnel_B1F_Text_SofiaPostBattle::
    .string "I want to go home!$"

RockTunnel_B1F_Text_DudleyIntro::
    .string "Hahaha!\n"
    .string "Can you beat my power?$"

RockTunnel_B1F_Text_DudleyDefeat::
    .string "HIKER: Oops!\n"
    .string "Out-muscled!$"

RockTunnel_B1F_Text_DudleyPostBattle::
    .string "I go for power because\n"
    .string "I hate thinking!$"

RockTunnel_B1F_Text_CooperIntro::
    .string "You have a POKéDEX?\n"
    .string "I want one too!$"

RockTunnel_B1F_Text_CooperDefeat::
    .string "POKéMANIAC: Shoot!\n"
    .string "I'm so jealous!$"

RockTunnel_B1F_Text_CooperPostBattle::
    .string "When you finish your POKéDEX,\n"
    .string "can I have it?$"

RockTunnel_B1F_Text_SteveIntro::
    .string "Do you know about\n"
    .string "costume players?$"

RockTunnel_B1F_Text_SteveDefeat::
    .string "POKéMANIAC: Well, that's that.$"

RockTunnel_B1F_Text_StevePostBattle::
    .string "Costume players dress up\n"
    .string "as POKéMON for fun.$"

RockTunnel_B1F_Text_AllenIntro::
    .string "My POKéMON techniques will leave\n"
    .string "you crying!$"

RockTunnel_B1F_Text_AllenDefeat::
    .string "HIKER: I give!\n"
    .string "You're a better technician!$"

RockTunnel_B1F_Text_AllenPostBattle::
    .string "In mountains, you'll often find\n"
    .string "rock-type POKéMON.$"

RockTunnel_B1F_Text_MarthaIntro::
    .string "I don't often come here,\n"
    .string "but I will fight you.$"

RockTunnel_B1F_Text_MarthaDefeat::
    .string "JR.TRAINER♂: Oh!\n"
    .string "I lost!$"

RockTunnel_B1F_Text_MarthaPostBattle::
    .string "I like tiny POKéMON,\n"
    .string "big ones are too scary!$"

RockTunnel_B1F_Text_EricIntro::
    .string "Hit me with your best shot!$"

RockTunnel_B1F_Text_EricDefeat::
    .string "HIKER: Fired away!$"

RockTunnel_B1F_Text_EricPostBattle::
    .string "I'll raise my POKéMON to\n"
    .string "beat yours, kid!$"

RockTunnel_B1F_Text_WinstonIntro::
    .string "I draw POKéMON\n"
    .string "when I'm home.$"

RockTunnel_B1F_Text_WinstonDefeat::
    .string "POKéMANIAC: Whew!\n"
    .string "I'm exhausted!$"

RockTunnel_B1F_Text_WinstonPostBattle::
    .string "I'm an artist, not a fighter.$"

