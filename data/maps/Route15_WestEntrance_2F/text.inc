Route15_WestEntrance_2F_Text_GiveItemIfCaughtEnough::
    .string "Hi! Remember me?\n"
    .string "I'm PROF.OAK's AIDE!\p"
    .string "If you caught {STR_VAR_1} kinds\n"
    .string "of POKéMON, I'm supposed to\p"
    .string "give you an {STR_VAR_2}!\l"
    .string "So, {PLAYER}, have you caught at\n"
    .string "least {STR_VAR_1} kinds of POKéMON?$"

Route15_WestEntrance_2F_Text_GreatHereYouGo::
    .string "Great! You have caught\n"
    .string "{STR_VAR_3} kinds of POKéMON!\p"
    .string "Congratulations!\n"
    .string "Here you go!$"

Route15_WestEntrance_2F_Text_ReceivedItemFromAide::
    .string "{PLAYER} got the {STR_VAR_2}!$"

Route15_WestEntrance_2F_Text_ExplainExpShare::
    .string "EXP.ALL gives EXP points to all\n"
    .string "the POKéMON with you, even if\p"
    .string "they don't fight.$"

Route15_WestEntrance_2F_Text_LargeShiningBird::
    .string "Looked into the binoculars...\n"
    .string "A large, shining bird is flying\p"
    .string "toward the sea.$"

Route15_WestEntrance_2F_Text_SmallIslandOnHorizon::
    .string "Looked into the binoculars.\n"
    .string "It looks like a small island!$"

