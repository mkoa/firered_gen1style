SSAnne_2F_Room4_Text_LamarIntro::
    .string "Which do you like,\n"
    .string "a strong or a rare POKéMON?$"

SSAnne_2F_Room4_Text_LamarDefeat::
    .string "GENTLEMAN: I must salute you!$"

SSAnne_2F_Room4_Text_LamarPostBattle::
    .string "I prefer strong and\n"
    .string "rare POKéMON.$"

SSAnne_2F_Room4_Text_DawnIntro::
    .string "I never saw you at the party.$"

SSAnne_2F_Room4_Text_DawnDefeat::
    .string "LASS: Take it easy!$"

SSAnne_2F_Room4_Text_DawnPostBattle::
    .string "Oh, I adore your strong POKéMON!$"

