Route6_Text_RickyIntro::
    .string "Who's there?\n"
    .string "Quit listening in on us!$"

Route6_Text_RickyDefeat::
    .string "JR.TRAINER♂: I just can't win!$"

Route6_Text_RickyPostBattle::
    .string "Whisper...\n"
    .string "Whisper...$"

Route6_Text_NancyIntro::
    .string "Excuse me!\n"
    .string "This is a private conversation!$"

Route6_Text_NancyDefeat::
    .string "JR.TRAINER♀: Ugh!\n"
    .string "I hate losing!$"

Route6_Text_NancyPostBattle::
    .string "Whisper...\n"
    .string "Whisper...$"

Route6_Text_KeigoIntro::
    .string "There aren't many bugs out here.$"

Route6_Text_KeigoDefeat::
    .string "BUG CATCHER: No!\n"
    .string "You're kidding!$"

Route6_Text_KeigoPostBattle::
    .string "I like bugs, so I'm going back to\n"
    .string "VIRIDIAN FOREST.$"

Route6_Text_JeffIntro::
    .string "Huh?\n"
    .string "You want to talk to me?$"

Route6_Text_JeffDefeat::
    .string "JR.TRAINER♂: I didn't start it!$"

Route6_Text_JeffPostBattle::
    .string "I should carry more POKéMON\n"
    .string "with me for safety.$"

Route6_Text_IsabelleIntro::
    .string "Me? Well, OK.\n"
    .string "I'll play!$"

Route6_Text_IsabelleDefeat::
    .string "JR.TRAINER♀: Just didn't work!$"

Route6_Text_IsabellePostBattle::
    .string "I want to get stronger!\n"
    .string "What's your secret?$"

Route6_Text_ElijahIntro::
    .string "I've never seen you around!\n"
    .string "Are you good?$"

Route6_Text_ElijahDefeat::
    .string "BUG CATCHER: You are too good!$"

Route6_Text_ElijahPostBattle::
    .string "Are my POKéMON weak?\n"
    .string "Or, am I just bad?$"

Route6_Text_UndergroundPathSign::
    .string "UNDERGROUND PATH\n"
    .string "CERULEAN CITY - VERMILION CITY$"

