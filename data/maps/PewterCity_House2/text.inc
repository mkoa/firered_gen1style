PewterCity_House2_Text_MonsLearnTechniquesAsTheyGrow::
    .string "POKéMON learn new techniques\n"
    .string "as they grow!\p"
    .string "But, some moves must be\n"
    .string "taught by the trainer!$"

PewterCity_House2_Text_MonsEasierCatchIfStatused::
    .string "POKéMON become easier to catch\n"
    .string "when they are hurt or asleep!\p"
    .string "But, it's not a sure thing!$"
