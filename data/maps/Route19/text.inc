Route19_Text_RichardIntro::
    .string "Have to warm up before\n"
    .string "my swim!$"

Route19_Text_RichardDefeat::
    .string "SWIMMER: All warmed up!$"

Route19_Text_RichardPostBattle::
    .string "Thanks, kid!\n"
    .string "I'm ready for a swim!$"

Route19_Text_ReeceIntro::
    .string "Wait!\n"
    .string "You'll have a heart attack!$"

Route19_Text_ReeceDefeat::
    .string "SWIMMER: Ooh!\n"
    .string "That's chilly!$"

Route19_Text_ReecePostBattle::
    .string "Watch out for TENTACOOL!$"

Route19_Text_MatthewIntro::
    .string "I love swimming!\n"
    .string "What about you?$"

Route19_Text_MatthewDefeat::
    .string "SWIMMER: Belly flop!$"

Route19_Text_MatthewPostBattle::
    .string "I can beat POKéMON\n"
    .string "at swimming!$"

Route19_Text_DouglasIntro::
    .string "What's beyond the horizon?$"

Route19_Text_DouglasDefeat::
    .string "SWIMMER: Glub!$"

Route19_Text_DouglasPostBattle::
    .string "I see a couple of islands!$"

Route19_Text_DavidIntro::
    .string "I tried diving for POKéMON, but it\n"
    .string "was a no go!$"

Route19_Text_DavidDefeat::
    .string "SWIMMER: Help!$"

Route19_Text_DavidPostBattle::
    .string "You have to fish for\n"
    .string "sea POKéMON!$"

Route19_Text_TonyIntro::
    .string "I look at the sea to forget all\n"
    .string "the bad things that happened.$"

Route19_Text_TonyDefeat::
    .string "Ooh!\n"
    .string "Traumatic!$"

Route19_Text_TonyPostBattle::
    .string "I'm looking at the sea to forget\n"
    .string "the bad thing that happened!$"

Route19_Text_AnyaIntro::
    .string "Oh, I just love your ride!\n"
    .string "Can I have it if I win?$"

Route19_Text_AnyaDefeat::
    .string "BEAUTY: Oh! I lost!$"

Route19_Text_AnyaPostBattle::
    .string "It's still a long way to go\n"
    .string "to SEAFOAM ISLANDS.$"

Route19_Text_AliceIntro::
    .string "Swimming's great!\n"
    .string "Sunburns aren't!$"

Route19_Text_AliceDefeat::
    .string "BEAUTY: Shocker!$"

Route19_Text_AlicePostBattle::
    .string "My boy friend wanted to swim\n"
    .string "TO SEAFOAM ISLANDS.$"

Route19_Text_AxleIntro::
    .string "These waters are treacherous!$"

Route19_Text_AxleDefeat::
    .string "SWIMMER: Ooh!\n"
    .string "Dangerous!$"

Route19_Text_AxlePostBattle::
    .string "I got a cramp!\n"
    .string "Glub, glub...$"

Route19_Text_ConnieIntro::
    .string "I swam here , but I'm tired.$"

Route19_Text_ConnieDefeat::
    .string "BEAUTY: I'm exhausted...$"

Route19_Text_ConniePostBattle::
    .string "LAPRAS is so big, it must\n"
    .string "keep you dry on water.$"

Route19_Text_RouteSign::
    .string "SEA ROUTE 19\n"
    .string "FUCHSIA CITY - SEAFOAM ISLANDS$"

Route19_Text_LiaIntro::
    .string "LIA: I'm looking after my brother.\n"
    .string "He just became a TRAINER.$"

Route19_Text_LiaDefeat::
    .string "LIA: That's no way to treat my\n"
    .string "little brother!$"

Route19_Text_LiaPostBattle::
    .string "LIA: Do you have a younger\n"
    .string "brother?\p"
    .string "I hope you're teaching him all\n"
    .string "sorts of things.$"

Route19_Text_LiaNotEnoughMons::
    .string "LIA: I want to battle together\n"
    .string "with my little brother.\p"
    .string "Don't you have two POKéMON?$"

Route19_Text_LucIntro::
    .string "LUC: My big sis taught me how\n"
    .string "to swim and train POKéMON.$"

Route19_Text_LucDefeat::
    .string "LUC: Oh, wow!\n"
    .string "Someone tougher than my big sis!$"

Route19_Text_LucPostBattle::
    .string "LUC: My big sis is strong and nice.\n"
    .string "I think she's awesome!$"

Route19_Text_LucNotEnoughMons::
    .string "LUC: I don't want to if I can't\n"
    .string "battle you with my big sis.\p"
    .string "Don't you have two POKéMON?$"
