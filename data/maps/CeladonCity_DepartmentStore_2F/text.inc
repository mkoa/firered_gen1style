CeladonCity_DepartmentStore_2F_Text_SuperRepelMorePowerfulRepel::
    .string "SUPER REPEL keeps weak POKéMON\n"
    .string "at bay...\p"
    .string "Hmm, it's a more powerful REPEL!$"

CeladonCity_DepartmentStore_2F_Text_BuyReviveForLongOutings::
    .string "For long outings, you should buy\n"
    .string "REVIVE.$"

CeladonCity_DepartmentStore_3F_Text_TakeTM18::
    .string "Oh, hi!\n"
    .string "I finally finished POKéMON!\p"
    .string "Not done yet?\n"
    .string "This might be useful!$"

CeladonCity_DepartmentStore_3F_Text_PutTM18Away::
    .string "{PLAYER} put TM18 away in\n"
    .string "the BAG's TM CASE.$"

CeladonCity_DepartmentStore_3F_Text_TM18_Contains::
    .string "TM18 is COUNTER!\n"
    .string "Not like the one I'm\l"
    .string "leaning on, mind you!$"

CeladonCity_DepartmentStore_2F_Text_FloorSign::
    .string "Top Grade Items for TRAINERS!\p"
    .string "2F: TRAINER'S MARKET$"

CeladonCity_DepartmentStore_2F_Text_LanceComesToBuyCapes::
    .string "We have a customer, LANCE, who\n"
    .string "occasionally comes.\p"
    .string "He always buys capes.\l"
    .string "I wonder... Does he have many\n"
    .string "identical capes at home?$"
