PokemonTower_3F_Text_HopeIntro::
    .string "Urrg...Awaa...\n"
    .string "Huhu...graa..$"

PokemonTower_3F_Text_HopeDefeat::
    .string "CHANNELER: Hwa!\n"
    .string "I'm saved!$"

PokemonTower_3F_Text_HopePostBattle::
    .string "The GHOSSTs can be identified\n"
    .string "by the SILPH SCOPE.$"

PokemonTower_3F_Text_CarlyIntro::
    .string "Kekeke...\n"
    .string "Kwaaah!$"

PokemonTower_3F_Text_CarlyDefeat::
    .string "CHANNELER: Hmm?\n"
    .string "What am I doing?$"

PokemonTower_3F_Text_CarlyPostBattle::
    .string "Sorry!\n"
    .string "I was possessed!$"

PokemonTower_3F_Text_PatriciaIntro::
    .string "Be gone!\n"
    .string "Evil spirit!$"

PokemonTower_3F_Text_PatriciaDefeat::
    .string "CHANNELER: Whew!\n"
    .string "The spirit left!$"

PokemonTower_3F_Text_PatriciaPostBattle::
    .string "My friends were possessed too!$"

