CeladonCity_Text_GotMyKoffingInCinnabar::
    .string "I got my KOFFING in CINNABAR!\p"
    .string "It's nice, but it breathes\n"
    .string "poison when it's angry!$"

CeladonCity_Text_GymIsGreatFullOfWomen::
    .string "Heheh! This GYM is great!\n"
    .string "It's full of women!$"

CeladonCity_Text_GameCornerIsBadForCitysImage::
    .string "The GAME CORNER is bad\n"
    .string "for our city's image.$"

CeladonCity_Text_BlewItAllAtSlots::
    .string "Moan!\n"
    .string "I blew it all at the slots!\p"
    .string "I knew I should have cashed\n"
    .string "in my coins for prizes!$"

CeladonCity_Text_MyTrustedPalPoliwrath::
    .string "This is my trusted pal, POLIWRATH!\p"
    .string "It evolved from POLIWHIRL when\n"
    .string "I used WATER STONE!$"

CeladonCity_Text_Poliwrath::
    .string "POLIWRATH: Ribi ribit!$"

CeladonCity_Text_GetLostOrIllPunchYou::
    .string "What are you staring at?$"

CeladonCity_Text_KeepOutOfTeamRocketsWay::
    .string "Keep out of TEAM ROCKET's way!$"

CeladonCity_Text_ExplainXAccuracyDireHit::
    .string "TRAINER TIPS\p"
    .string "X ACCURACY boosts the accuracy of\n"
    .string "techniques.\p"
    .string "DIRE HIT jacks up the likelihood\n"
    .string "of critical hits.\p"
    .string "Get your items at the CELADON\n"
    .string "DEPT. STORE!$"

CeladonCity_Text_CitySign::
    .string "CELADON CITY\n"
    .string "The City of Rainbow Dreams$"

CeladonCity_Text_GymSign::
    .string "CELADON CITY POKéMON GYM\n"
    .string "LEADER: ERIKA\l"
    .string "The Nature Loving Princess!$"

CeladonCity_Text_MansionSign::
    .string "CELADON MANSION$"

CeladonCity_Text_DeptStoreSign::
    .string "Find what you need at the\n"
    .string "CELADON DEPT. STORE!$"

CeladonCity_Text_GuardSpecProtectsFromStatus::
    .string "TRAINER TIPS\p"
    .string "GUARD SPEC. protects POKéMON\n"
    .string "against SPECIAL attacks such\l"
    .string "as fire and water!\p"
    .string "Get your items at\n"
    .string "CELADON DEPT. STORE!$"

CeladonCity_Text_PrizeExchangeSign::
    .string "COINS exchanged for prizes!\n"
    .string "PRIZE EXCHANGE$"

CeladonCity_Text_GameCornerSign::
    .string "ROCKET GAME CORNER\n"
    .string "The playground for grown-ups!$"

CeladonCity_Text_ScaldedTongueOnTea::
    .string "Aaaagh, ow...\n"
    .string "I scalded my tongue!\p"
    .string "This nice old lady in the MANSION\n"
    .string "gave me some TEA.\p"
    .string "But it was boiling hot!\n"
    .string "Gotta cool it to drink it.$"

CeladonCity_Text_TakeTM41::
    .string "Hello, there!\p"
    .string "I've seen you, but I never\n"
    .string "had a chance to talk!\p"
    .string "Here's a gift for\n"
    .string "dropping by!$"

CeladonCity_Text_PutTM41Away::
    .string "{PLAYER} put TM41 away in\n"
    .string "the BAG's TM CASE.$"

CeladonCity_Text_TM41_Contains::
    .string "TM41 teaches SOFTBOILED!\n"
    .string "Only one POKéMON can use it!$"

CeladonCity_Text_SomeoneStoleSilphScope::
    .string "Oh, what am I to do…\p"
    .string "Someone stole our SILPH SCOPE.\p"
    .string "The thief came running this way,\n"
    .string "I'm sure of it.\p"
    .string "But I lost sight of him!\n"
    .string "Where'd he go?$"

