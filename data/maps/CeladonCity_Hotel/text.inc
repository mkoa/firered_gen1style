CeladonCity_Hotel_Text_ThisHotelIsForPeople::
    .string "POKéMON?\n"
    .string "No, this is a hotel for people.\p"
    .string "We're full up.$"

CeladonCity_Hotel_Text_OnVacationWithBrotherAndBoyfriend::
    .string "I'm on vacation with my\n"
    .string "brother and boy friend.\p"
    .string "CELADON is such a pretty city!$"

CeladonCity_Hotel_Text_WhyDidSheBringBrother::
    .string "Why did she bring her brother?$"

CeladonCity_Hotel_Text_SisBroughtMeOnVacation::
    .string "My sis brought me on this vacation!$"

