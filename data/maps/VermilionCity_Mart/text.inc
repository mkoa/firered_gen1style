VermilionCity_Mart_Text_TeamRocketAreWickedPeople::
    .string "There are evil people who will\n"
    .string "use POKéMON for criminal acts.\p"
    .string "TEAM ROCKET traffics in\n"
    .string "rare POKéMON.\p"
    .string "They also abandon POKéMON that\n"
    .string "they consider not to be\p"
    .string "popular or useful.$"

VermilionCity_Mart_Text_MonsGoodOrBadDependingOnTrainer::
    .string "I think POKéMON can be good or\n"
    .string "evil. It depends on the trainer.$"

