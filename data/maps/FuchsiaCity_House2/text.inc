FuchsiaCity_House2_Text_DoYouLikeToFish::
    .string "I'm the FISHING GURU's\n"
    .string "older brother!\p"
    .string "I simply Looove fishing!\n"
    .string "Do you like to fish?$"

FuchsiaCity_House2_Text_LikeYourStyleTakeThis::
    .string "Grand! I like your style!\n"
    .string "Take this and fish, young one!$"

FuchsiaCity_House2_Text_ReceivedGoodRod::
    .string "{PLAYER} received a GOOD ROD!$"

FuchsiaCity_House2_Text_GoodRodCanCatchBetterMons::
    .string "Fishing is a way of life!\n"
    .string "It is like the finest poetry.\p"
    .string "A crummy OLD ROD could only catch\n"
    .string "MAGIKARP, yes?\p"
    .string "But with a GOOD ROD, you can\n"
    .string "catch much better POKéMON.$"

FuchsiaCity_House2_Text_OhThatsDisappointing::
    .string "Oh...\n"
    .string "That's so disappointing...$"

FuchsiaCity_House2_Text_HowAreTheFishBiting::
    .string "Hello there, {PLAYER}!\p"
    .string "How are the fish biting?$"

FuchsiaCity_House2_Text_YouHaveNoRoomForGift::
    .string "Oh no!\p"
    .string "You have no room for my gift!$"

