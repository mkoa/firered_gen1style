FuchsiaCity_Gym_Text_KogaIntro::
    .string "KOGA: Fwahahaha!\p"
    .string "A mere child like you dares to\n"
    .string "challenge me?\p"
    .string "Very well, I shall show you true\n"
    .string "terror as a ninja master!\p"
    .string "You shall feel the despair of\n"
    .string "poison and sleep techniques!{PLAY_BGM}{MUS_ENCOUNTER_GYM_LEADER}$"

FuchsiaCity_Gym_Text_KogaDefeat::
    .string "KOGA: Humph!\n"
    .string "You have proven your worth!\p"
    .string "Here!\n"
    .string "Take the SOULBADGE!$"

FuchsiaCity_Gym_Text_KogaPostBattle::
    .string "When afflicted by TOXIC, POKéMON\n"
    .string "suffer more and more as\l"
    .string "battle progresses!\p"
    .string "It will surely terrorize foes!$"

FuchsiaCity_Gym_Text_KogaExplainSoulBadge::
    .string "Now that you have the SOULBADGE,\n"
    .string "the DEFENSE of your\l"
    .string "POKéMON increases!\p"
    .string "It also lets you SURF outside\n"
    .string "of battle!\p"
    .string "Ah!\n"
    .string "Take this, too!$"

FuchsiaCity_Gym_Text_ReceivedTM06FromKoga::
    .string "{PLAYER} received TM06!$"

FuchsiaCity_Gym_Text_KogaExplainTM06::
    .string "TM06 contains TOXIC!\n"
    .string "It is a secret technique over\p"
    .string "400 years old!$"

FuchsiaCity_Gym_Text_MakeSpaceForThis::
    .string "Make space for this, child!$"

FuchsiaCity_Gym_Text_KaydenIntro::
    .string "Strength isn't the key for POKéMON!\n"
    .string "It's strategy!\p"
    .string "I'll show you how strategy can\n"
    .string "beat brute strength!$"

FuchsiaCity_Gym_Text_KaydenDefeat::
    .string "JUGGLER: What?\n"
    .string "Extraordinary!$"

FuchsiaCity_Gym_Text_KaydenPostBattle::
    .string "So, you mix brawn with brains?\n"
    .string "Good strategy!$"

FuchsiaCity_Gym_Text_KirkIntro::
    .string "I wanted to become a ninja,\n"
    .string "so I joined this GYM!$"

FuchsiaCity_Gym_Text_KirkDefeat::
    .string "JUGGLER: I'm done for!$"

FuchsiaCity_Gym_Text_KirkPostBattle::
    .string "I will keep training under\n"
    .string "KOGA, my ninja master!$"

FuchsiaCity_Gym_Text_NateIntro::
    .string "Let's see you beat my special\n"
    .string "techniques!$"

FuchsiaCity_Gym_Text_NateDefeat::
    .string "JUGGLER: You had me fooled!$"

FuchsiaCity_Gym_Text_NatePostBattle::
    .string "I like poison and sleep techniques,\n"
    .string "as they linger after battle!$"

FuchsiaCity_Gym_Text_PhilIntro::
    .string "Stop right there!\p"
    .string "Our invisible walls have\n"
    .string "you frustrated?$"

FuchsiaCity_Gym_Text_PhilDefeat::
    .string "TAMER: Whoa!\n"
    .string "He's got it!$"

FuchsiaCity_Gym_Text_PhilPostBattle::
    .string "You impressed me!\n"
    .string "Here's a hint!\p"
    .string "Look very closely for gaps in the\n"
    .string "invisible walls!$"

FuchsiaCity_Gym_Text_EdgarIntro::
    .string "I also study the way of the ninja\n"
    .string "with master KOGA!\p"
    .string "Ninja have a long history of\n"
    .string "using animals!$"

FuchsiaCity_Gym_Text_EdgarDefeat::
    .string "TAMER: Awoo!$"

FuchsiaCity_Gym_Text_EdgarPostBattle::
    .string "I still have much to learn!$"

FuchsiaCity_Gym_Text_ShawnIntro::
    .string "Master KOGA comes from a long line\n"
    .string "of ninjas!\p"
    .string "What did you descend from?$"

FuchsiaCity_Gym_Text_ShawnDefeat::
    .string "JUGGLER: Dropped my balls!$"

FuchsiaCity_Gym_Text_ShawnPostBattle::
    .string "Where there is light, there is\n"
    .string "shadow!\p"
    .string "Light and shadow!\n"
    .string "Which do you choose?$"

FuchsiaCity_Gym_Text_GymGuyAdvice::
    .string "Yo!\n"
    .string "Champ in the making!\p"
    .string "FUCHSIA GYM is riddled with\n"
    .string "invisible walls!\p"
    .string "KOGA might appear close, but he's\n"
    .string "blocked off!\p"
    .string "You have to find gaps in the walls\n"
    .string "to reach him!$"

FuchsiaCity_Gym_Text_GymGuyPostVictory::
    .string "It's amazing how ninja can terrify,\n"
    .string "even now!$"

FuchsiaCity_Gym_Text_GymStatue::
    .string "FUCHSIA CITY POKéMON GYM\n"
    .string "LEADER: KOGA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}$"

FuchsiaCity_Gym_Text_GymStatuePlayerWon::
    .string "FUCHSIA CITY POKéMON GYM\n"
    .string "LEADER: KOGA\p"
    .string "WINNING TRAINERS:\n"
    .string "{RIVAL}, {PLAYER}$"

