SilphCo_2F_Text_ConnorIntro::
    .string "Help!\n"
    .string "I'm a SILPH employee.$"

SilphCo_2F_Text_ConnorDefeat::
    .string "SCIENTIST: How did you know\n"
    .string "I was a ROCKET?$"

SilphCo_2F_Text_ConnorPostBattle::
    .string "I work for both SILPH and\n"
    .string "TEAM ROCKET!$"

SilphCo_2F_Text_JerryIntro::
    .string "It's off limits here!\n"
    .string "Go home!$"

SilphCo_2F_Text_JerryDefeat::
    .string "SCIENTIST: You're good.$"

SilphCo_2F_Text_JerryPostBattle::
    .string "Can you solve the maze in here?$"

SilphCo_2F_Text_Grunt1Intro::
    .string "No kids are allowed in here!$"

SilphCo_2F_Text_Grunt1Defeat::
    .string "ROCKET: Tough!$"

SilphCo_2F_Text_Grunt1PostBattle::
    .string "Diamond shaped tiles are\n"
    .string "teleport blocks!\p"
    .string "They're hi-tech transporters!$"
SilphCo_2F_Text_Grunt2Intro::
    .string "Hey kid!\n"
    .string "What are you doing here?$"

SilphCo_2F_Text_Grunt2Defeat::
    .string "ROCKET: I goofed!$"

SilphCo_2F_Text_Grunt2PostBattle::
    .string "SILPH CO. will be merged with\n"
    .string "TEAM ROCKET!$"

SilphCo_2F_Text_TakeTM36::
    .string "Eeek!\n"
    .string "No! Stop! Help!\p"
    .string "Oh, you're not with TEAM ROCKET.\n"
    .string "I thought...\p"
    .string "I'm sorry.\n"
    .string "Here, please take this!$"

SilphCo_2F_Text_PutTM36Away::
    .string "{PLAYER} put TM36 away in\n"
    .string "the BAG's TM CASE.$"

SilphCo_2F_Text_TM36_Contains::
    .string "TM36 is SELFDESTRUCT!\n"
    .string "It's powerful, but the POKéMON\p"
    .string "that uses it faints!\l"
    .string "Be careful.$"

SilphCo_2F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "2F$"

