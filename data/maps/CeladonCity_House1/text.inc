CeladonCity_House1_Text_SlotsReelInTheDough::
    .string "Hehehe!\p"
    .string "The slots just reel in\n"
    .string "the dough, big time!$"

CeladonCity_House1_Text_ShippedMonsAsSlotPrizes::
    .string "CHIEF!\p"
    .string "We just shipped 2000\n"
    .string "POKéMON as slot prizes!$"

CeladonCity_House1_Text_DontTouchGameCornerPoster::
    .string "Don't touch the poster\n"
    .string "at the GAME CORNER!\p"
    .string "There's no secret switch behind it!$"

@ Text for the replaced altars in the rocket chiefs house / celadon mansion
@ In English RB, this is westernized as "It's a sculpture of DIGLETT.", and is removed altogether in FRLG
Text_ItsABuddhistAltar::
    .string "ぶつだん　だ‥$"
