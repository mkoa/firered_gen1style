RocketHideout_B3F_Text_Grunt1Intro::
    .string "Stop meddling in TEAM\n"
    .string "ROCKET's affairs!$"

RocketHideout_B3F_Text_Grunt1Defeat::
    .string "ROCKET: Oof!\n"
    .string "Taken down!$"

RocketHideout_B3F_Text_Grunt1PostBattle::
    .string "SILPH SCOPE?\n"
    .string "The machine the BOSS stole.\p"
    .string "It's here somewhere.$"

RocketHideout_B3F_Text_Grunt2Intro::
    .string "We got word from upstairs\n"
    .string "that you were coming!$"

RocketHideout_B3F_Text_Grunt2Defeat::
    .string "ROCKET: What?\n"
    .string "I lost? No!$"

RocketHideout_B3F_Text_Grunt2PostBattle::
    .string "Go ahead and go!\p"
    .string "But, you need the LIFT KEY\n"
    .string "to run the elevator!$"

