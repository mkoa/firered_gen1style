LavenderTown_PokemonCenter_1F_Text_HearMrFujiNotFromAroundHere::
    .string "I recently moved to this town.\p"
    .string "I hear that MR. FUJI's not from\n"
    .string "these parts originally, either.$"

LavenderTown_VolunteerPokemonHouse_Text_WhereDidMrFujiGo::
    .string "That's odd, MR.FUJI isn't here.\n"
    .string "Where'd he go?$"

LavenderTown_VolunteerPokemonHouse_Text_MrFujiWasPrayingForCubonesMother::
    .string "MR.FUJI had been praying alone\n"
    .string "for CUBONE's mother.$"

LavenderTown_VolunteerPokemonHouse_Text_MrFujiLooksAfterOrphanedMons::
    .string "This is really MR.FUJI's house.\p"
    .string "He's really kind!\p"
    .string "He looks after abandoned and\n"
    .string "orphaned POKéMON!$"

LavenderTown_VolunteerPokemonHouse_Text_MonsNiceToHug::
    .string "It's so warm!\n"
    .string "POKéMON are so nice to hug.$"

LavenderTown_VolunteerPokemonHouse_Text_Nidorino::
    .string "NIDORINO: Gaoo!$"

LavenderTown_VolunteerPokemonHouse_Text_Psyduck::
    .string "PSYDUCK: Gwappa!$"

LavenderTown_VolunteerPokemonHouse_Text_IdLikeYouToHaveThis::
    .string "MR.FUJI: {PLAYER}.\p"
    .string "Your POKéDEX quest may fail\n"
    .string "without love for your POKéMON.\p"
    .string "I think this may help\n"
    .string "your quest.$"

LavenderTown_VolunteerPokemonHouse_Text_ReceivedPokeFluteFromMrFuji::
    .string "{PLAYER} received a POKé FLUTE!$"

LavenderTown_VolunteerPokemonHouse_Text_ExplainPokeFlute::
    .string "Upon hearing POKé FLUTE,\n"
    .string "sleeping POKéMON will spring awake.\p"
    .string "It works on all sleeping POKéMON.$"

LavenderTown_VolunteerPokemonHouse_Text_MustMakeRoomForThis::
    .string "You must make room for this!$"

LavenderTown_VolunteerPokemonHouse_Text_HasPokeFluteHelpedYou::
    .string "MR.FUJI: Has my FLUTE\n"
    .string "helped you?$"

LavenderTown_VolunteerPokemonHouse_Text_GrandPrizeDrawingClipped::
    .string "POKéMON Monthly\n"
    .string "Grand Prize Drawing!\p"
    .string "The application form is...\p"
    .string "Gone! It's been clipped out!$"

LavenderTown_VolunteerPokemonHouse_Text_PokemonMagazinesLineShelf::
    .string "POKéMON magazines!\p"
    .string "POKéMON notebooks!\p"
    .string "POKéMON graphs!$"

