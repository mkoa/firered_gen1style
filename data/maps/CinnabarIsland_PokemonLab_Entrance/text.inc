CinnabarIsland_Gym_Text_PhotoOfBlaineAndFuji::
    .string "It's a photo of BLAINE and\n"
    .string "MR. FUJI.\p"
    .string "They're standing shoulder to\n"
    .string "shoulder with big grins.$"

CinnabarIsland_PokemonLab_Entrance_Text_StudyMonsExtensively::
    .string "We study POKéMON extensively\n"
    .string "here.\p"
    .string "People often bring us rare\n"
    .string "POKéMON for examination.$"

CinnabarIsland_PokemonLab_Entrance_Text_PhotoOfLabFounderDrFuji::
    .string "A photo of the LAB's\n"
    .string "founder, DR.FUJI!$"

CinnabarIsland_PokemonLab_Entrance_Text_MeetingRoomSign::
    .string "POKéMON LAB\n"
    .string "Meeting Room$"

CinnabarIsland_PokemonLab_Entrance_Text_RAndDRoomSign::
    .string "POKéMON LAB\n"
    .string "R-and-D Room$"

CinnabarIsland_PokemonLab_Entrance_Text_TestingRoomSign::
    .string "POKéMON LAB\n"
    .string "Testing Room$"

