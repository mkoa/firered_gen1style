FuchsiaCity_SafariZone_Entrance_Text_WelcomeToSafariZone::
    .string "Welcome to the SAFARI ZONE!$"

FuchsiaCity_SafariZone_Entrance_Text_PlaySafariGameFor500::
    .string "For just ¥500, you can catch all\n"
    .string "the POKéMON you want in the park!\p"
    .string "Would you like to join the hunt?$"

FuchsiaCity_SafariZone_Entrance_Text_ThatllBe500WeOnlyUseSpecialBalls::
    .string "That'll be ¥500, please!\p"
    .string "We only use a special POKé\n"
    .string "BALL here.$"

FuchsiaCity_SafariZone_Entrance_Text_PlayerReceived30SafariBalls::
    .string "{PLAYER} received 30 SAFARI BALLS!$"

FuchsiaCity_SafariZone_Entrance_Text_CallYouOnPAWhenYouRunOut::
    .string "We'll call you on the PA when you\n"
    .string "run out of time or SAFARI BALLS!$"

FuchsiaCity_SafariZone_Entrance_Text_OkayPleaseComeAgain::
    .string "OK!\n"
    .string "Please come again!$"

FuchsiaCity_SafariZone_Entrance_Text_OopsNotEnoughMoney::
    .string "Oops!\n"
    .string "Not enough money!$"

FuchsiaCity_SafariZone_Entrance_Text_GoingToLeaveSafariZoneEarly::
    .string "Are you going to leave the\n"
    .string "SAFARI ZONE early?$"

FuchsiaCity_SafariZone_Entrance_Text_PleaseReturnSafariBalls::
    .string "Please return any SAFARI BALLS\n"
    .string "you may have left.$"

FuchsiaCity_SafariZone_Entrance_Text_GoodLuck::
    .string "Good luck!$"

FuchsiaCity_SafariZone_Entrance_Text_CatchFairShareComeAgain::
    .string "Did you get a good haul?\n"
    .string "Come again!$"

FuchsiaCity_SafariZone_Entrance_Text_FirstTimeAtSafariZone::
    .string "Hi! Is it your first time here?$"

FuchsiaCity_SafariZone_Entrance_Text_ExplainSafariZone::
    .string "SAFARI ZONE actually has 4\n"
    .string "zones in it.\p"
    .string "Each zone has different kinds\n"
    .string "of POKéMON.\p"
    .string "Use SAFARI BALLS to\n"
    .string "catch them!\p"
    .string "When you run out of time or SAFARI\n"
    .string "BALLS, it's game over for you!\p"
    .string "Before you go, open an unused\n"
    .string "POKéMON BOX so there's room for\l"
    .string "new POKéMON!$"

FuchsiaCity_SafariZone_Entrance_Text_SorryYoureARegularHere::
    .string "Sorry, you're a regular here!$"

