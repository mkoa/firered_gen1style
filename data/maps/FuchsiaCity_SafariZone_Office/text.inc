FuchsiaCity_SafariZone_Office_Text_NicknamedWardenSlowpoke::
    .string "We nicknamed the WARDEN SLOWPOKE.\n"
    .string "He and SLOWPOKE both look vacant!$"

FuchsiaCity_SafariZone_Office_Text_WardenIsVeryKnowledgeable::
    .string "SLOWPOKE is very\n"
    .string "knowledgeable about POKéMON!\p"
    .string "He even has some fossils of\n"
    .string "rare, extinct POKéMON!$"

FuchsiaCity_SafariZone_Office_Text_CouldntUnderstandWarden::
    .string "SLOWPOKE came in, but I\n"
    .string "couldn't understand him.\p"
    .string "I think he's got a speech problem!$"

FuchsiaCity_SafariZone_Office_Text_PrizeInSafariZone::
    .string "WARDEN SLOWPOKE is running a\n"
    .string "promotion campaign right now.\p"
    .string "Try to get to the farthest corner\n"
    .string "of the SAFARI ZONE.\p"
    .string "If you can make it, you'll win a\n"
    .string "very convenient prize.$"

