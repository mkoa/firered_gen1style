PokemonTower_2F_Text_RivalIntro::
    .string "{RIVAL}: Hey, {PLAYER}!\n"
    .string "What brings you here?\p"
    .string "Your POKéMON don't look dead!\n"
    .string "I can at least make them faint!\p"
    .string "Let's go, pal!$"

PokemonTower_2F_Text_RivalDefeat::
    .string "{RIVAL}: What?\n"
    .string "You stinker!\p"
    .string "I took it easy on you too!$"

@ Unused. Translated below
@ Aw, man! They really kicked the bucket! Weak! Do them a favor and raise them more properly.
PokemonTower_2F_Text_RivalVictory::
    .string "{RIVAL}“あーあ‥！\n"
    .string "ほんとに　くたばっちまったぞ！\l"
    .string "よわいなー！\l"
    .string "もっと　ちゃんと　そだてて　やれよ$"

PokemonTower_2F_Text_RivalPostBattle::
    .string "How's your POKéDEX coming, pal?\n"
    .string "I just caught a CUBONE!\p"
    .string "I can't find the grown-up\n"
    .string "MAROWAK yet!\p"
    .string "I doubt there are any left!\n"
    .string "Well, I better get going!\p"
    .string "I've got a lot to accomplish, pal!\n"
    .string "Smell ya later!$"

PokemonTower_2F_Text_SilphScopeCouldUnmaskGhosts::
    .string "Even we could not identify\n"
    .string "the wayward GHOSTs!\p"
    .string "A SILPH SCOPE might be able to\n"
    .string "unmask them.$"

