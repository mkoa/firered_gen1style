SilphCo_7F_Text_HaveMonForSavingUs::
    .string "Oh! Hi! You're not a ROCKET!\n"
    .string "You came to save us?\l"
    .string "Why, thank you!\p"
    .string "I want you to have this POKéMON\n"
    .string "for saving us.$"

SilphCo_7F_Text_ObtainedLaprasFromEmployee::
    .string "{PLAYER} got LAPRAS.$"

SilphCo_7F_Text_ExplainLapras::
    .string "It's LAPRAS.\n"
    .string "It's very intelligent.\p"
    .string "We kept it in our lab, but it will\n"
    .string "be much better off with you!\p"
    .string "I think you will be a good trainer\n"
    .string "for LAPRAS!\p"
    .string "It's a good swimmer.\n"
    .string "It'll give you a lift!$"

SilphCo_7F_Text_RocketBossWentToBoardroom::
    .string "TEAM ROCKET's BOSS went to\n"
    .string "the boardroom!\p"
    .string "Is our PRESIDENT OK?$"

SilphCo_7F_Text_OhNo::
    .string "あ‥　もう　もてないぞ$"

SilphCo_7F_Text_SavedAtLast::
    .string "たすかったよ　ありがと！$"

SilphCo_7F_Text_RocketsAfterMasterBall::
    .string "TEAM ROCKET was after the\n"
    .string "MASTER BALL which will catch\p"
    .string "any POKéMON!$"

SilphCo_7F_Text_CanceledMasterBallProject::
    .string "We canceled the MASTER BALL\n"
    .string "project because of TEAM ROCKET.$"

SilphCo_7F_Text_BadIfTeamRocketTookOver::
    .string "It would be bad if TEAM ROCKET\n"
    .string "took over SILPH or our POKéMON!$"

SilphCo_7F_Text_WowYouChasedOffTeamRocket::
    .string "Wow!\p"
    .string "You chased off TEAM ROCKET all by\n"
    .string "yourself?$"

SilphCo_7F_Text_ReallyDangerousHere::
    .string "You!\n"
    .string "It's really dangerous here!\p"
    .string "You came to save me?\n"
    .string "You can't!$"

SilphCo_7F_Text_ThankYouSoMuch::
    .string "Safe at last!\n"
    .string "Oh thank you!$"

SilphCo_7F_Text_Grunt3Intro::
    .string "Oh ho!\n"
    .string "I smell a little rat!$"

SilphCo_7F_Text_Grunt3Defeat::
    .string "ROCKET: Lights out!$"

SilphCo_7F_Text_Grunt3PostBattle::
    .string "You won't find my BOSS by just\n"
    .string "scurrying around!$"

SilphCo_7F_Text_JoshuaIntro::
    .string "Heheh!\p"
    .string "You mistook me for a SILPH worker?$"

SilphCo_7F_Text_JoshuaDefeat::
    .string "SCIENTIST: I'm done!$"

SilphCo_7F_Text_JoshuaPostBattle::
    .string "Despite your age, you are a\n"
    .string "skilled trainer!$"

SilphCo_7F_Text_Grunt1Intro::
    .string "I am one of the 4 ROCKET\n"
    .string "BROTHERS!$"

SilphCo_7F_Text_Grunt1Defeat::
    .string "ROCKET: Aack!\n"
    .string "Brothers, I lost!$"

SilphCo_7F_Text_Grunt1PostBattle::
    .string "Doesn't matter.\n"
    .string "My brothers will repay the favor!$"

SilphCo_7F_Text_Grunt2Intro::
    .string "A child intruder?\n"
    .string "That must be you!$"

SilphCo_7F_Text_Grunt2Defeat::
    .string "ROCKET: Fine!\n"
    .string "I lost!$"

SilphCo_7F_Text_Grunt2PostBattle::
    .string "Go on home before my BOSS\n"
    .string "gets ticked off!$"

SilphCo_7F_Text_RivalWhatKeptYou::
    .string "{RIVAL}: What kept you {PLAYER}?$"

SilphCo_7F_Text_RivalIntro::
    .string "{RIVAL}: Hahaha! I thought you'd\n"
    .string "turn up if I waited here!\p"
    .string "I guess TEAM ROCKET slowed you\n"
    .string "down! Not that I care!\p"
    .string "I saw you in SAFFRON, so I decided\n"
    .string "to see if you got better!$"

SilphCo_7F_Text_RivalDefeat::
    .string "Oh ho!\n"
    .string "So, you are ready for BOSS ROCKET!$"

SilphCo_7F_Text_RivalVictory::
    .string "{RIVAL}“おまえなあ‥\p"
    .string "こんな　うでまえじゃ\n"
    .string "まだまだ‥\l"
    .string "いちにんまえ　とは　いえないぜ$"

SilphCo_7F_Text_RivalPostBattle::
    .string "Well, {PLAYER}!\n"
    .string "I'm moving on up and ahead!\p"
    .string "By checking my POKéDEX, I'm\n"
    .string "starting to see what's strong\p"
    .string "and how they evolve!\l"
    .string "I'm going to the POKéMON LEAGUE to\n"
    .string "boot out the ELITE FOUR!\p"
    .string "I'll become the world's most\n"
    .string "powerful trainer!\p"
    .string "{PLAYER}, well good luck to you!\n"
    .string "Don't sweat it!\p"
    .string "Smell ya!$"

SilphCo_7F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "7F$"

