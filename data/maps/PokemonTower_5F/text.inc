PokemonTower_5F_Text_RestHereInPurifiedSpace::
    .string "Come, child!\n"
    .string "I sealed this space\l"
    .string "with white magic!$"
    .string "You can rest here!$"

PokemonTower_5F_Text_TammyIntro::
    .string "Give...me...\n"
    .string "your...soul...$"

PokemonTower_5F_Text_TammyDefeat::
    .string "Gasp!$"

PokemonTower_5F_Text_TammyPostBattle::
    .string "I was under possession!$"

PokemonTower_5F_Text_RuthIntro::
    .string "You...shall...\n"
    .string "join...us...$"

PokemonTower_5F_Text_RuthDefeat::
    .string "CHANNELER: What a nightmare!$"

PokemonTower_5F_Text_RuthPostBattle::
    .string "I was possessed!$"

PokemonTower_5F_Text_KarinaIntro::
    .string "Zombies!$"

PokemonTower_5F_Text_KarinaDefeat::
    .string "CHANNELER: Ha?$"

PokemonTower_5F_Text_KarinaPostBattle::
    .string "I regained my senses!$"

PokemonTower_5F_Text_JanaeIntro::
    .string "Urgah...\n"
    .string "Urff....$"

PokemonTower_5F_Text_JanaeDefeat::
    .string "CHANNELER: Whoo!$"

PokemonTower_5F_Text_JanaePostBattle::
    .string "I fell to evil spirits\n"
    .string "despite my training!$"

PokemonTower_5F_Text_PurifiedZoneMonsFullyHealed::
    .string "Entered purified, protected zone!\n"
    .string "{PLAYER}'s POKéMON are fully healed!$"

