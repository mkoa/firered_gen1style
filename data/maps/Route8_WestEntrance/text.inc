Route8_WestEntrance_Text_ThirstyOnGuardDuty::
    .string "I'm on guard duty.\n"
    .string "Gee, I'm thirsty, though!\p"
    .string "Oh wait there, the road's closed.$"

Route8_WestEntrance_Text_ThatTeaLooksTasty::
    .string "Whoa, boy!\n"
    .string "I'm parched!$"

Route8_WestEntrance_Text_ThanksIllShareTeaWithGuards::
    .string "...\n"
    .string "Huh? I can have this drink?\p"
    .string "Gee, thanks!\l"
    .string "...\n"
    .string "Glug, glug...\p"
    .string "...\l"
    .string "Gulp...\n"
    .string "If you want to go to\p"
    .string "SAFFRON CITY...\l"
    .string "...\n"
    .string "You can go on through.\p"
    .string "I'll share this with the\n"
    .string "other guards!$"

Route8_WestEntrance_Text_HiHowsItGoing::
    .string "Hi, thanks for the cool drinks!$"

