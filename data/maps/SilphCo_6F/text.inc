SilphCo_6F_Text_RocketsTookOverBuilding::
    .string "The ROCKETs came and took over\n"
    .string "the building!$"

SilphCo_6F_Text_BetterGetBackToWork::
    .string "Well, better get back to work!$"

SilphCo_6F_Text_HelpMePlease::
    .string "Oh dear, oh dear.\n"
    .string "Help me please!$"

SilphCo_6F_Text_WeGotEngaged::
    .string "We got engaged!\n"
    .string "Heheh!$"

SilphCo_6F_Text_ThatManIsSuchACoward::
    .string "Look at him!\n"
    .string "He's such a coward!$"

SilphCo_6F_Text_NeedsMeToLookAfterHim::
    .string "I feel so sorry for him,\n"
    .string "I have to marry him!$"

SilphCo_6F_Text_RocketsTryingToConquerWorld::
    .string "TEAM ROCKET is trying to conquer\n"
    .string "the world with POKéMON!$"

SilphCo_6F_Text_RocketsRanAwayBecauseOfYou::
    .string "TEAM ROCKET ran because\n"
    .string "of you!$"

SilphCo_6F_Text_TargetedSilphForOurMonProducts::
    .string "They must have targeted SILPH for\n"
    .string "our POKéMON products.$"

SilphCo_6F_Text_ComeWorkForSilphWhenYoureOlder::
    .string "Come work for SILPH when you\n"
    .string "get older!$"

SilphCo_6F_Text_Grunt1Intro::
    .string "I am one of the 4 ROCKET\n"
    .string "BROTHERS!$"

SilphCo_6F_Text_Grunt1Defeat::
    .string "ROCKET: Flame out!$"

SilphCo_6F_Text_Grunt1PostBattle::
    .string "No matter!\n"
    .string "My brothers will avenge me!$"

SilphCo_6F_Text_TaylorIntro::
    .string "That rotten PRESIDENT!\p"
    .string "He shouldn't have sent me to\n"
    .string "the TIKSI BRANCH!$"

SilphCo_6F_Text_TaylorDefeat::
    .string "SCIENTIST: Shoot!$"

SilphCo_6F_Text_TaylorPostBattle::
    .string "TIKSI BRANCH?\n"
    .string "It's in Russian no man's-land!$"

SilphCo_6F_Text_Grunt2Intro::
    .string "You dare betray TEAM ROCKET?$"

SilphCo_6F_Text_Grunt2Defeat::
    .string "ROCKET: You traitor!$"

SilphCo_6F_Text_Grunt2PostBattle::
    .string "If you stand for justice, you\n"
    .string "betray evil!$"

SilphCo_6F_Text_FloorSign::
    .string "SILPH CO. HEAD OFFICE\n"
    .string "6F$"

