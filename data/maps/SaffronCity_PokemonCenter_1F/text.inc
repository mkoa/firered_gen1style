SaffronCity_PokemonCenter_1F_Text_GrowthRatesDifferBySpecies::
    .string "POKéMON growth rates differ\n"
    .string "from species to specie.$"

SaffronCity_PokemonCenter_1F_Text_SilphCoVictimOfFame::
    .string "SILPH CO. is very famous.\p"
    .string "That's why it attracted\n"
    .string "TEAM ROCKET!$"

SaffronCity_PokemonCenter_1F_Text_GreatIfEliteFourCameBeatRockets::
    .string "It would be great if the ELITE FOUR\n"
    .string "came and stomped TEAM ROCKET!$"

SaffronCity_PokemonCenter_1F_Text_TeamRocketTookOff::
    .string "TEAM ROCKET took off!\n"
    .string "We can go out safely again!\l"
    .string "That's great!$"

