Route16_Text_LaoIntro::
    .string "What do you want?$"

Route16_Text_LaoDefeat::
    .string "BIKER: Don't you dare laugh!$"

Route16_Text_LaoPostBattle::
    .string "We like just hanging here,\n"
    .string "what's it to you?$"

Route16_Text_KojiIntro::
    .string "Nice BIKE!\n"
    .string "Hand it over!$"

Route16_Text_KojiDefeat::
    .string "CUE BALL: Knock out!$"

Route16_Text_KojiPostBattle::
    .string "Forget it, who needs your BIKE!$"

Route16_Text_LukeIntro::
    .string "Come out and play, little mouse!$"

Route16_Text_LukeDefeat::
    .string "CUE BALL: You little rat!$"

Route16_Text_LukePostBattle::
    .string "I hate losing!\n"
    .string "Get away form me!$"

Route16_Text_HideoIntro::
    .string "Hey, you just bumped me!$"

Route16_Text_HideoDefeat::
    .string "BIKER: Kaboom!$"

Route16_Text_HideoPostBattle::
    .string "You can also get to FUCHSIA from\n"
    .string "VERMILION using a coastal road.$"

Route16_Text_CamronIntro::
    .string "I'm feeling hungry and mean!$"

Route16_Text_CamronDefeat::
    .string "CUE BALL: Bad, bad, bad!$"

Route16_Text_CamronPostBattle::
    .string "I like my POKéMON ferocious!\n"
    .string "They tear up enemies!$"

Route16_Text_RubenIntro::
    .string "Sure, I'll go!$"

Route16_Text_RubenDefeat::
    .string "BIKER: Don't make me mad!$"

Route16_Text_RubenPostBattle::
    .string "I like harassing people with\n"
    .string "my vicious POKéMON!$"

Route16_Text_MonSprawledOutInSlumber::
    .string "A sleeping POKéMON\n"
    .string "blocks the way!$"

Route16_Text_CyclingRoadSign::
    .string "Enjoy the slope!\n"
    .string "CYCLING ROAD$"

Route16_Text_RouteSign::
    .string "ROUTE 16\n"
    .string "CELADON CITY - FUCHSIA CITY$"

Route16_Text_JedIntro::
    .string "JED: Our love knows no bounds.\n"
    .string "We're in love and we show it!$"

Route16_Text_JedDefeat::
    .string "JED: Oh, no!\n"
    .string "My love has seen me as a loser!$"

Route16_Text_JedPostBattle::
    .string "JED: Listen, LEA.\n"
    .string "You need to focus less on me.$"

Route16_Text_JedNotEnoughMons::
    .string "JED: You have just one POKéMON?\n"
    .string "Is there no love in your heart?$"

Route16_Text_LeaIntro::
    .string "LEA: Sometimes, the intensity of\n"
    .string "our love scares me.$"

Route16_Text_LeaDefeat::
    .string "LEA: Ohh! But JED looks cool\n"
    .string "even in a loss!$"

Route16_Text_LeaPostBattle::
    .string "LEA: Ehehe, I'm sorry.\n"
    .string "JED is so cool.$"

Route16_Text_LeaNotEnoughMons::
    .string "LEA: Oh, you don't have two\n"
    .string "POKéMON with you?\p"
    .string "Doesn't it feel lonely for you or\n"
    .string "your POKéMON?$"
