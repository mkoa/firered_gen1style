Route13_Text_SebastianIntro::
    .string "My bird POKéMON want to scrap!$"

Route13_Text_SebastianDefeat::
    .string "BIRD KEEPER: My bird combo lost?$"

Route13_Text_SebastianPostBattle::
    .string "My POKéMON look happy even though\n"
    .string "they lost.$"

Route13_Text_SusieIntro::
    .string "I'm told I'm good for a kid!$"

Route13_Text_SusieDefeat::
    .string "JR.TRAINER♀: Ohh!\n"
    .string "I lost!$"

Route13_Text_SusiePostBattle::
    .string "I want to become a good trainer.\n"
    .string "I'll train hard.$"

Route13_Text_ValerieIntro::
    .string "Wow!\n"
    .string "Your BADGEs are too cool!$"

Route13_Text_ValerieDefeat::
    .string "JR.TRAINER♀: Not enough!$"

Route13_Text_ValeriePostBattle::
    .string "You got those BADGEs from GYM\n"
    .string "LEADERs. I know!$"

Route13_Text_GwenIntro::
    .string "My cute POKéMON wish to make your\n"
    .string "acquaintance.$"

Route13_Text_GwenDefeat::
    .string "JR.TRAINER♀: Wow!\n"
    .string "You totally won!$"

Route13_Text_GwenPostBattle::
    .string "You have to make POKéMON fight\n"
    .string "to toughen them up.$"

Route13_Text_AlmaIntro::
    .string "I found CARBOS in a cave once.$"

Route13_Text_AlmaDefeat::
    .string "JR.TRAINER♀: Just messed up!$"

Route13_Text_AlmaPostBattle::
    .string "CARBOS boosted the SPEED of my\n"
    .string "POKéMON.$"

Route13_Text_PerryIntro::
    .string "The wind's blowing my way!$"

Route13_Text_PerryDefeat::
    .string "BIRD KEEPER: The wind turned!$"

Route13_Text_PerryPostBattle::
    .string "I'm beat.\n"
    .string "I guess I'll FLY home.$"

Route13_Text_LolaIntro::
    .string "Sure, I'll play with you!$"

Route13_Text_LolaDefeat::
    .string "BEAUTY: Oh!\n"
    .string "You little brute!$"

Route13_Text_LolaPostBattle::
    .string "I wonder which is stronger, male\n"
    .string "or female POKéMON?$"

Route13_Text_SheilaIntro::
    .string "Do you want to POKéMON with me?$"

Route13_Text_SheilaDefeat::
    .string "BEAUTY: It's over already?$"

Route13_Text_SheilaPostBattle::
    .string "I don't know anything about POKéMON.\n"
    .string "I just like cool ones!$"

Route13_Text_JaredIntro::
    .string "What're you lookin' at?$"

Route13_Text_JaredDefeat::
    .string "BIKER: Dang!\n"
    .string "Stripped gears!$"

Route13_Text_JaredPostBattle::
    .string "Get lost!$"

Route13_Text_RobertIntro::
    .string "I always go with bird POKéMON!$"

Route13_Text_RobertDefeat::
    .string "BIRD KEEPER: Out of power!$"

Route13_Text_RobertPostBattle::
    .string "I wish I could fly like PIDGEY\n"
    .string "and PIDGEOTTO...$"

Route13_Text_LookToLeftOfThatPost::
    .string "TRAINER TIPS\p"
    .string "Look, look!\n"
    .string "Look to the left of that post!$"

Route13_Text_SelectToSwitchItems::
    .string "TRAINER TIPS\p"
    .string "Use SELECT to switch items in\n"
    .string "the ITEM window.$"

Route13_Text_RouteSign::
    .string "ROUTE 13\n"
    .string "North to SILENCE BRIDGE$"

