PewterCity_Mart_Text_BoughtWeirdFishFromShadyGuy::
    .string "A shady, old man got me to\n"
    .string "buy this really weird fish POKéMON!\p"
    .string "It's totally weak and it cost ¥500!$"

PewterCity_Mart_Text_GoodThingsIfRaiseMonsDiligently::
    .string "Good things can happen if you\n"
    .string "raise POKéMON diligently,\p"
    .string "even the weak ones!$"

