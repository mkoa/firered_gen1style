Text_BootedUpPC::
	.string "{PLAYER} turned on the PC.$"

Text_AccessWhichPC::
	.string "Which PC should be accessed?$"

Text_AccessedSomeonesPC::
	.string "Accessed Someone's PC.$"

Text_OpenedPkmnStorage::
	.string "POKéMON Storage System opened.$"

Text_AccessedPlayersPC::
	.string "Accessed my PC.$"

Text_AccessedItemStorage::
	.string "Accessed Item Storage System.$"

Text_AccessedBillsPC::
	.string "Accessed BILL's PC.$"
