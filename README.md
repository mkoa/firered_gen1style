# Pokémon Red & Blue Gen1Style V5

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/RedTitlePromo.png "Red Title Screen")

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BlueTitlePromo.png "Blue Title Screen")

**Why?**

This project started when I decided that I dislike the vanilla FireRed art style enough to create a sprite replacement hack.
This idea eventually grew through feature creep into a total re-de-make that aims to bring Red & Blue to the GBA, complete with widescreen and menus that don't suck!

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromo1.png "Battle, safari, trainer challenge, level up.")

**Why not play a colorization hack?**

The tiny screen of the GBC makes me sad, your view is very limited and menus are necessarily cramped. I wanted widescreen, I wanted the ability to connect to DS projects, I wanted good menus and full color.

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromo2.png "Trade, map, summary, pokedex.")

**What's changed?**

Trainer parties, move stats, item prices and availability, learnsets, TMs, world layout, scripted events, dialogue, and more all match NTSC Red & Blue as closely as I can manage. I have played through the original games and G1S side-by-side 3 times in order to match the experience as closely as possible.

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromo3.png "Bicycle, fishing, surfing, standing.")

**Why the Progressing Hacks category?**

Although the project is 99% done, there are still some very important changes to be made
and more testing to be done before declaring the project fully finished.
National Dex event and other post E4 events have not been completely erased/modified/verified.

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromo4.png "Forest, dark cave, power plant, game corner.")

**Is this yet another eternal beta?**

The current version is playable all the way through Cerulean Cave. It's 99% done.
Too many projects promised greatness then died due to disinterest, showed amazing work and then fizzled out. I decided to develop G1S in secret until it was ready for the general public to play.

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/BulkPromo5.png "Shop, bag, pokemon storage, item storage.")

![](https://gitlab.com/xyifer12/firered_gen1style/-/raw/master/ImagesAndStuff/Overworld_V1.png "Kanto Map")

**Ensure your base ROM matches one of the hashes below. Rev0 patches are for FRLG 1.0 while Rev1 patches are for FRLG 1.1.**

- _pokefirered sha1_: 41cb23d8dccc8ebd7c649cd8fbb58eeace6e2fdc
- _pokefirered_rev1_ sha1: dd5945db9b930750cb39d00c84da8571feebf417
- _pokeleafgreen sha1_: 574fa542ffebb14be69902d1d36f1ec0a4afd71e
- _pokeleafgreen_rev1_ sha1: 7862c67bdecbe21d1d69ce082ce34327e1c6ed5e

**Multiplayer seems to work, but there is the chance that you become stuck or your save is corrupted if I missed something. Be aware that ALL link play is done at your own risk.**

https://www.pokecommunity.com/threads/red-blue-g1s.525470/

I have personally played through both Red and Blue to completion, however, there is always a chance a new breaking bug can appear. Please use mGBA or a comparable emulator, do not use VBA.

Credits:
Thank you to CHAMBER, SOLOO993, BLUE EMERALD, LAKE, NESLUG, PIKACHU25 for the Pokemon party icon sprite sheet available through PRET tutorials, 
TheXaman for the wonderful Gen4-style party menu, Moylend for playing my project and reporting issues, 
Meister_anon and GriffinR for answering my questions on Discord!